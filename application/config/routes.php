<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['dashboard'] = '';



$route['login'] = 'Auth/login/';

// Tata Usaha

$route['tata-usaha/profil_sekolah'] = 'ProfilSekolah/index/';
$route['tata-usaha/list-tahun-ajaran'] = 'TahunAjaran/index/';
$route['tata-usaha/profil-sekolah'] = 'Sekolah/index/';

$route['tata-usaha/list-pegawai'] = 'Pegawai/index/';
$route['tata-usaha/list-siswa'] = 'Siswa/index/';
$route['tata-usaha/list-rombel'] = 'Rombel/index/';
$route['tata-usaha/list-mata-pelajaran'] = 'Mapel/index/';
$route['tata-usaha/list-ekstrakurikuler'] = 'Ekskul/index/';

$route['tata-usaha/list-role-pegawai'] = 'RolePegawai/index/';
$route['tata-usaha/anggota-rombel'] = 'AnggotaRombel/index/';
$route['tata-usaha/pembelajaran'] = 'Pembelajaran/index/';

$route['tata-usaha/cetak-rapor'] = 'CetakRapor/index/';

$route['guru-bk/nilai-sikap'] = 'NilaiSikap/index/';

$route['guru-mapel/nilai-pengetahuan'] = 'NilaiPengetahuan/index/';
$route['guru-mapel/nilai-keterampilan'] = 'NilaiKeterampilan/index/';

$route['kesiswaan/nilai-ekstrakurikuler'] = 'NilaiEkskul/index/';
$route['kesiswaan/prestasi'] = 'Prestasi/index/';

$route['wali-kelas/absensi'] = 'Absensi/index/';
$route['wali-kelas/catatan-wali-kelas'] = 'CatatanWalas/index/';


$route['laporan/detail/(:any)/(:any)'] = 'CetakRapor/getLaporan/$1/$2';


