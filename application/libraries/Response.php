<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response
{

	public $title;
	public $message;
	public $status;	
	public $data;
	public $info;
	public $meta;

	private $CI;

	public function __construct(){
		
		$this->CI =& get_instance();		
		$this->CI->benchmark->mark('code_start');			
	}	

	public function initialize($params = null){

		$this->title = !empty($params->title)?$params->title:"New Response";

		$this->message = !empty($params->message)?$params->message:"Create new response success";		


		$status = new StdClass();
		$status->error = !empty($params->status->error)?$params->status->error:false;
		$status->code = !empty($params->status->code)?$params->status->code:200;	

		$this->status = $status;		

		$this->data = !empty($params->data)?$params->data:array();

		$info = new StdClass();

		$pagination = new StdClass();
		$pagination->status = !empty($params->info->pagination->status)?$params->info->pagination->status:false;
		$pagination->page = !empty($params->info->pagination->page)?$params->info->pagination->page:1;
		$pagination->total = !empty($params->info->pagination->total)?$params->info->pagination->total:1;

		$info->pagination = $pagination;		

		$data = new StdClass();
		$data->shown = !empty($params->info->data->shown)?$params->info->data->shown:0;
		$data->total = !empty($params->info->data->total)?$params->info->data->total:0;

		$info->data = $data;
		
		$this->CI->benchmark->mark('code_end');
		
		$info->executionTime = $this->CI->benchmark->elapsed_time('code_start', 'code_end');		

		$this->info = $info;

		$this->meta = !empty($params->meta)?$params->meta:new StdClass();

		return $this;
	}
	
}
/* End of file Response */
/* Location: ./application/libraries/Response */
