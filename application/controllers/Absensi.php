<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdAbsensi');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isWaliKelas') === TRUE):

			redirect('page/page-404');

		endif;
		$data["page"] = 'absensi';
		$this->load->view('wali_kelas/absensi',$data);
	}

	public function getAbsensi($id_rombel, $smt){

		$where = array(
			"deleted_at"=> null,
			"id_rombel" => $id_rombel,
			"smt" => $smt
		);

		$data = $this->MdAbsensi->getAbsensi($where);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function saveAbsensi(){
		$param = array(
			"rombel" => $this->input->post("rombel"),
			"semester" => $this->input->post("semester"),
			"penilaian" => $this->input->post("penilaian"),
		);
		// var_dump($param);
		$saved = $this->MdAbsensi->saveAbsensi($param);
		if($saved){
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil disimpan!"
			]);
		} else {
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		}
	}

}

/* End of file Absensi.php */
/* Location: ./application/controllers/Absensi.php */