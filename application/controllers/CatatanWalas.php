<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatatanWalas extends CI_Controller {

	
	public function __construct(){

		parent::__construct();

		$this->load->model('MdCatatanWalas');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isWaliKelas') === TRUE):

			redirect('page/page-404');

		endif;
		$data["page"] = 'catatan-walas';
		$this->load->view('wali_kelas/catatan-walas',$data);
	}

	public function getCatatanWalas($id_rombel, $smt){

		$where = array(
			"deleted_at"=> null,
			"id_rombel" => $id_rombel,
			"smt" => $smt
		);

		$data = $this->MdCatatanWalas->getCatatanWalas($where);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function saveCatatanWalas(){
		$param = array(
			"rombel" => $this->input->post("rombel"),
			"semester" => $this->input->post("semester"),
			"penilaian" => $this->input->post("penilaian"),
		);
		$saved = $this->MdCatatanWalas->saveCatatanWalas($param);
		if($saved){
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil disimpan!"
			]);
		} else {
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		}
	}


}

/* End of file CatatanWalas.php */
/* Location: ./application/controllers/CatatanWalas.php */