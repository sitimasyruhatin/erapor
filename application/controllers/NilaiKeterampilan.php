<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiKeterampilan extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdNilaiKeterampilan');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isGuruMapel') === TRUE):

			redirect('page/page-404');

		endif;
		$data["page"] = 'nilaiketerampilan';
		$this->load->view('guru_mapel/nilai-keterampilan',$data);
	}

	public function getNilaiK($id_rombel, $smt, $id_mapel){

		$where = array(
			"deleted_at"=> null,
			"id_rombel" => $id_rombel,
			"smt" => $smt,
			"id_mapel" => $id_mapel
		);

		$data = $this->MdNilaiKeterampilan->getNilaiK($where);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function getDeskripsiK($id_pembelajaran,$smt,$kategori){

		$where = array(
			"deleted_at"=> null,
			"id_pembelajaran" => $id_pembelajaran,
			"smt"=> $smt,
			"kategori_nilai"=> $kategori
		);

		$data = $this->MdNilaiKeterampilan->getDeskripsiK($where);

		// var_dump($data);
		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function saveNilaiK(){
		$param = array(
			"rombel" => $this->input->post("rombel"),
			"semester" => $this->input->post("semester"),
			"penilaian" => $this->input->post("penilaian")
		);
		// var_dump($param);
		$saved = $this->MdNilaiKeterampilan->saveNilaiK($param);
		if($saved){
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil disimpan!",
				"data" => $param
			]);
		} else {
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		}
	}


	public function saveDeskripsiK(){
		$param = array(
			// "id_pembelajaran" => $this->input->post("pembelajaran"),
			// "semester" => $this->input->post("semester"),
			// "kategori_nilai" => $this->input->post("kategori_nilai"),
			"deskripsi" => $this->input->post("deskripsi"),
		);
		// var_dump($param);
		$saved = $this->MdNilaiKeterampilan->saveDeskripsiK($param);
		if($saved){
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil disimpan!"
			]);
		} else {
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		}
	}


}

/* End of file NilaiKeterampilan.php */
/* Location: ./application/controllers/NilaiKeterampilan.php */