<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiPengetahuan extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdNilaiPengetahuan');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isGuruMapel') === TRUE):

			redirect('page/page-404');

		endif;
		$data["page"] = 'nilaipengetahuan';
		$this->load->view('guru_mapel/nilai-pengetahuan',$data);
	}

	public function getNilaiP($id_rombel, $smt, $id_mapel){

		$where = array(
			"deleted_at"=> null,
			"id_rombel" => $id_rombel,
			"smt" => $smt,
			"id_mapel" => $id_mapel
		);

		$data = $this->MdNilaiPengetahuan->getNilaiP($where);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function getDeskripsiP($id_pembelajaran,$smt,$kategori){

		$where = array(
			"deleted_at"=> null,
			"id_pembelajaran" => $id_pembelajaran,
			"smt"=> $smt,
			"kategori_nilai"=> $kategori
		);

		$data = $this->MdNilaiPengetahuan->getDeskripsiP($where);

		// var_dump($data);
		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function saveNilaiP(){
		$param = array(
			"rombel" => $this->input->post("rombel"),
			"semester" => $this->input->post("semester"),
			"penilaian" => $this->input->post("penilaian")
		);
		// var_dump($param);
		$saved = $this->MdNilaiPengetahuan->saveNilaiP($param);
		if($saved){
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil disimpan!",
				"data" => $param
			]);
		} else {
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		}
	}


	public function saveDeskripsiP(){
		$param = array(
			// "id_pembelajaran" => $this->input->post("pembelajaran"),
			// "semester" => $this->input->post("semester"),
			// "kategori_nilai" => $this->input->post("kategori_nilai"),
			"deskripsi" => $this->input->post("deskripsi"),
		);
		// var_dump($param);
		$saved = $this->MdNilaiPengetahuan->saveDeskripsiP($param);
		if($saved){
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil disimpan!"
			]);
		} else {
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		}
	}


}

/* End of file NilaiPengetahuan.php */
/* Location: ./application/controllers/NilaiPengetahuan.php */