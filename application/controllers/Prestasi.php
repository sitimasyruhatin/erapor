<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestasi extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdPrestasi');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isKesiswaan') === TRUE):

			redirect('page/page-404');

		endif;
		$data["page"] = 'prestasi';
		$this->load->view('kesiswaan/prestasi',$data);
	}

	public function getPrestasi($id_rombel, $smt){

		$res = array();

		$where = array(
			"deleted_at"=> null,
			"id_rombel_fk" => $id_rombel,
			"semester" => $smt
		);

		if($id_rombel && $smt):
			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdPrestasi->getPrestasi($where));


		endif;

		echo json_encode($res);
	}

	// public function getSiswa($id = null){

	// 	$res = array();

	// 	if($id):

	// 		$res = array (
	// 			"success" => true,
	// 			"message" => "Data berhasil ditemukan",
	// 			"data" => $this->MdSiswa->getPrestasi($id));

	// 	else:

	// 		$res = array (
	// 			"success" => true,
	// 			"message" => "Data berhasil ditemukan",
	// 			"data" => $this->MdSiswa->getSiswa());

	// 	endif;


	// }

	public function tambahPrestasi($smt){

		$param = array (
			"id_anggota_rombel_fk"=>$this->input->post("anggota_rombel"),
			"jenis_kegiatan"=>$this->input->post("jenis_kegiatan"),
			"keterangan"=>$this->input->post("keterangan"),
			"semester"=>$smt,
		);

		$data = $this->MdPrestasi->tambahPrestasi($param);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil ditambahkan!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		endif;

	}

	public function updatePrestasi($id,$smt){

		$param = array (
			"id_anggota_rombel_fk"=>$this->input->post("anggota_rombel"),
			"jenis_kegiatan"=>$this->input->post("jenis_kegiatan"),
			"keterangan"=>$this->input->post("keterangan"),
			"semester"=>$smt,
		);

		$data = $this->MdPrestasi->updatePrestasi($param,$id);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil ditambahkan!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		endif;

	}

	public function deletePrestasi($id){

		$data = $this->MdPrestasi->deletePrestasi($id);

		if($id):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil dihapus!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data gagal dihapus!"
			]);
		endif;

	}

	// public function savePrestasi(){
	// 	$param = array(
	// 		"rombel" => $this->input->post("rombel"),
	// 		"semester" => $this->input->post("semester"),
	// 		"penilaian" => $this->input->post("penilaian"),
	// 	);

	// 	$saved = $this->MdPrestasi->savePrestasi($param);
	// 	if($saved){
	// 		echo json_encode([
	// 			"success" => true,
	// 			"message" => "Data berhasil disimpan!"
	// 		]);
	// 	} else {
	// 		echo json_encode([
	// 			"success" => false,
	// 			"message" => "Data gagal ditambahkan!"
	// 		]);
	// 	}
	// }

}

/* End of file Prestasi.php */
/* Location: ./application/controllers/Prestasi.php */