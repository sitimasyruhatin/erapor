<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CetakRapor extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdCetakRapor');

		$this->load->model('MdSiswa');

		$this->load->model('MdRombel');

		$this->load->model('MdPrestasi');

		$this->load->model('MdNilaiPengetahuan');

		$this->load->model('MdSekolah');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
        // var_dump($this->session->userdata('isTataUsaha'));
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');

		endif;
		$data["page"] = 'cetakrapor';
		// $data["pegawai"] = $this->MdPegawai->getPegawai();
		$this->load->view('tata-usaha/cetak-rapor',$data);
	}


	public function getRekapNilai($id_rombel, $smt){

		$where = array(
			"id_rombel_fk" => $id_rombel,
			"smt" => $smt
		);

		$data = $this->MdCetakRapor->getRekapNilai($where);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;


	}

	public function getLaporan($id , $smt){

		$where = array(
			"id_anggota_rombel" => $id,
			"smt" => $smt
		);

		$penilaian = $this->MdCetakRapor->getRekapNilai($where);

		$prestasi = $this->MdPrestasi->getPrestasi($where);

		$nilai_mapel = $this->MdCetakRapor->getNilaiMapel($where);

        $data_sekolah = $this->MdSekolah->getSekolah();

        // echo json_encode($nilai_mapel);
        // exit();

		$bulan = array (1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$split = explode('-', date('Y-m-d'));
		$hasil= $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
		$data = array(

			"penilaian"=>$penilaian[0],
			"prestasi"=>$prestasi,
			"sekolah"=> $data_sekolah[0],
			"tanggal"=> $hasil,
			"nilai_mapel"=>$nilai_mapel
		);

		$this->load->view('tata-usaha/laporan', $data);

	}


}

/* End of file CetakRapor.php */
/* Location: ./application/controllers/CetakRapor.php */