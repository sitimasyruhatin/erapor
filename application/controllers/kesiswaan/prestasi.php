<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestasi extends CI_Controller {

	public function index()
	{
		$data = array('title' => 'Dashboard Kesiswaan | ', );
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('kesiswaan/prestasi');
		$this->load->view('footer');
	}

	public function input()
	{
		$data = array('title' => 'Dashboard Kesiswaan | ', );
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('kesiswaan/input_prestasi');
		$this->load->view('footer');
	}

}

/* End of file prestasi.php */
/* Location: ./application/controllers/kesiswaan/prestasi.php */