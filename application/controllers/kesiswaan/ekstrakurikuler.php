<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ekstrakurikuler extends CI_Controller {

	public function index()
	{
		$data = array('title' => 'Dashboard Kesiswaan | ', );
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('kesiswaan/ekskul');
		$this->load->view('footer');
	}
	public function input()
	{
		$data = array('title' => 'Dashboard Kesiswaan | ', );
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('kesiswaan/input_ekskul');
		$this->load->view('footer');
	}


}

/* End of file ekstrakurikuler.php */
/* Location: ./application/controllers/kesiswaan/ekstrakurikuler.php */