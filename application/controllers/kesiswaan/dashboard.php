<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		$data = array('title' => 'Dashboard Kesiswaan | ', );
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('kesiswaan/dashboard');
		$this->load->view('footer');
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/kesiswaan/dashboard.php */