<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiSikap extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdNilaiSikap');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isGuruBK') === TRUE):

			redirect('page/page-404');

		endif;
		$data["page"] = 'nilai-sikap';
		$this->load->view('guru_bk/nilai-sikap',$data);
	}

	public function getNilaiSikap($id_rombel, $smt){

		$where = array(
			"deleted_at"=> null,
			"id_rombel" => $id_rombel,
			"smt" => $smt,
			"id_mapel" => 1
		);

		$data = $this->MdNilaiSikap->getNilaiSikap($where);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	

	public function saveNilaiSikap(){
		$param = array(
			"rombel" => $this->input->post("rombel"),
			"semester" => $this->input->post("semester"),
			"penilaian" => $this->input->post("penilaian"),
		);
		// var_dump($param);
		$saved = $this->MdNilaiSikap->saveNilaiSikap($param);
			if($saved){
				echo json_encode([
					"success" => true,
					"message" => "Data berhasil disimpan!"
				]);
			} else {
				echo json_encode([
					"success" => false,
					"message" => "Data gagal ditambahkan!"
				]);
			}
	}

}

/* End of file NilaiSikap.php */
/* Location: ./application/controllers/NilaiSikap.php */