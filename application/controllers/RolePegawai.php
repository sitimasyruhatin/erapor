<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RolePegawai extends CI_Controller {

	public function __construct(){

		parent::__construct();

		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;
	}

	public function index()
	{
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');

		endif;

		$data["page"] = 'rolepegawai';
		$this->load->view('tata-usaha/data-role-pegawai',$data);
	}

	public function getRolePegawai($id = null){

		$res = array();

		if($id):

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdRolePegawai->getRolePegawai($id));

		else:

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdRolePegawai->getRolePegawai());

		endif;

		echo json_encode($res);

	}

	public function getCariRolePegawai($param = null){
		$search = array(
			"nama_pegawai" => $this->input->get('q')
		);

		$where = array(
			"deleted_at"=> null,
			"id_role_fk" => $param
		);

		$data = $this->MdRolePegawai->getCariRolePegawai($where, $search);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function getCariRole(){
        $where = array(
            'deleted_at' => NULL
        );

        $search = array(
            "nama_role" => $this->input->get('q'),
        );

        $data = $this->MdRolePegawai->getCariRole($where, $search);

        if(!empty($data)):
            echo json_encode([
                "success" => true,
                "message" => " Data berhasil ditemukan!",
                "data" => $data
            ]);
        else:
            echo json_encode([
                "success" => false,
                "message" => " Data tidak ditemukan!"
            ]);
        endif;
    }

	
	public function tambahRolePegawai(){

		$param = array (
			"id_role_fk" => $this->input->post("role"),
			"id_pegawai_fk" => $this->input->post("pegawai")
		);
		$data = $this->MdRolePegawai->tambahRolePegawai($param);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil ditambahkan!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!, Role telah digunakan pada pegawai tersebut"
			]);
		endif;

	}

	public function updateRolePegawai($id){

		$param = array (
			"id_role_fk" => $this->input->post("role"),
			"id_pegawai_fk" => $this->input->post("pegawai")
		);
		$data = $this->MdRolePegawai->updateRolePegawai($param, $id);
		if($data):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil diupdate!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal diupdate!, Role telah digunakan pada pegawai tersebut"
			]);
		endif;


	}
	public function deleteRolePegawai($id){
		
		if($id){

			$data = $this->MdRolePegawai->deleteRolePegawai($id);

			if($data):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil dihapus!"
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data gagal dihapus, data role pegawai sedang digunakan!"
				]);
			endif;
		}

		else{
			echo json_encode([
				"success" => false,
				"message" => " Data gagal dihapus!"
			]);
		}

	}

}

/* End of file RolePegawai.php */
/* Location: ./application/controllers/RolePegawai.php */