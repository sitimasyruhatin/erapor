<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdAuth');

		$this->load->model('MdTahunAjaran');

	}

	public function index(){
		$sidebar['page'] = 'dashboard';
		if ($this->session->userdata('status')!=="logged") {

			$data = array('title' => 'Login | E-Rapor', );

			$this->load->view('login',$sidebar);
			
		}
		else {
			$this->load->view('dashboard',$sidebar);
		}
	}

	// public function getCariTahunAjaran(){

	// 	$search = array(
	// 		"tahun_ajaran" => $this->input->get('q')
	// 	);

	// 	$where = array(
	// 		"deleted_at"=> null
	// 	);

	// 	$data = $this->MdTahunAjaran->getCariTahunAjaran($where, $search);

	// 	if(!empty($data)):
	// 		echo json_encode([
	// 			"success" => true,
	// 			"message" => " Data berhasil ditemukan!",
	// 			"data" => $data
	// 		]);
	// 	else:
	// 		echo json_encode([
	// 			"success" => false,
	// 			"message" => " Data tidak ditemukan!"
	// 		]);
	// 	endif;
	// }


	public function login(){

		if ($this->session->userdata('status')!=="logged"):

			$param = array(
				"nik" => $this->input->post("nik"),
				"password" => md5($this->input->post("password"))
				// "id_tahun_ajaran" => $this->input->post("tahun-ajaran")
			);

			if($this->MdAuth->login($param)):
				$res = array (
					"success" => true,
					"message" => "Berhasil Login !",
					"data" => $this->MdAuth->login($param));

			else:

				$res = array (
					"success" => false,
					"message" => "NIK/Password yang anda masukkan salah",
				);

			endif;

			echo json_encode($res);

		else: 

			$this->load->view('dashboard',$data);

		endif;
	}

	// public function checkSession(){
	// 	echo json_encode($this->session->userdata());
	// }

	// FUNCTION LOGOUT ADMIN
	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url());
	}

	// public function login(){   
 //        if ($_SERVER['REQUEST_METHOD'] === 'POST'):
 //            $param = array(
 //                "nik_fk" => $this->input->post("nik"),
	// 			"password" => md5($this->input->post("password")),
	// 			"status" => 1
 //            );

 //            $res = $this->MdAuth->login($param);
 //            echo json_encode($res);
 //        else:
 //            $this->load->view('login');
 //        endif;
 //    }
 //    public function logout(){
 //        session_destroy();
 //        redirect(base_url());  
 //    }

	//hapus session nya terus login ulang.. set userdata buat TA yang baru belum bener
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */