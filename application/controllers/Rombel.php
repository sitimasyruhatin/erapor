<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rombel extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdRombel');
		$this->load->model('MdTahunAjaran');
		$this->load->model('MdPembelajaran');
		

		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');

		endif;

		$data["page"] = 'rombel';
		$this->load->view('tata-usaha/data-rombel',$data);

	}


	public function getRombel($id = null){

		$res = array();

		if($id):

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdRombel->getRombel($id));

		else:

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdRombel->getRombel());

		endif;

		echo json_encode($res);

	}

	public function getRombelWalas(){

		$id_pegawai_login = $this->session->userdata('id_pegawai');

		$where = array(
			"deleted_at"=> null,
			"id_pegawai_fk" => $id_pegawai_login,
			"id_tahun_ajaran_fk" => $this->session->userdata('tahun_ajaran')->id_tahun_ajaran
		);

		$data = $this->MdRombel->getRombelWalas($where);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;

	}

	public function getCariRombel(){
		$search = array(
			"nama_rombel" => $this->input->get('q')
		);
		$where = array(
			"deleted_at"=> null,
			"id_tahun_ajaran_fk" => $this->session->userdata('tahun_ajaran')->id_tahun_ajaran
		);
		$data = $this->MdRombel->getCariRombel($where, $search);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function getCariRombelAllYear(){
		$search = array(
			"nama_rombel" => $this->input->get('q')
		);

		$where = array(
			"deleted_at"=> null
		);

		$data = $this->MdRombel->getCariRombel($where, $search);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function getCariRombelNilai($id_mapel = null){

		$id_pegawai_login = $this->session->userdata('id_pegawai');

		$search = array(
			"nama_rombel" => $this->input->get('q')
		);

		if ($id_mapel != 1) {

			$where = array(
				"id_pegawai" => $id_pegawai_login,
				"id_tahun_ajaran_fk" => $this->session->userdata('tahun_ajaran')->id_tahun_ajaran,
				"id_mapel !=" => 1
			);

			$data = $this->MdRombel->getCariRombelNilai($where, $search);

			if(!empty($data)):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil ditemukan!",
					"data" => $data
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data tidak ditemukan!"
				]);
			endif;
		}

		else if($id_mapel == 1){
			$where = array(
				"id_pegawai" => $id_pegawai_login,
				"id_tahun_ajaran_fk" => $this->session->userdata('tahun_ajaran')->id_tahun_ajaran,
				"id_mapel" => 1
			);
			$data = $this->MdRombel->getCariRombelNilai($where, $search);

			if(!empty($data)):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil ditemukan!",
					"data" => $data
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data tidak ditemukan!"
				]);
			endif;
		}

		else{
			$where = array(
				"id_pegawai" => $id_pegawai_login,
				"id_tahun_ajaran_fk" => $this->session->userdata('tahun_ajaran')->id_tahun_ajaran,
				"id_mapel" => $id_mapel
			);
			$data = $this->MdRombel->getCariRombelNilai($where, $search);

			if(!empty($data)):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil ditemukan!",
					"data" => $data
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data tidak ditemukan!"
				]);
			endif;
		}

	}

	public function tambahRombel(){

		$param = array (
			"nama_rombel"=>$this->input->post("nama_rombel"),
			"tingkatan" => $this->input->post("tingkatan"),
			"jurusan" => $this->input->post("jurusan"),
			"id_pegawai_fk" => $this->input->post("walas"),
			"id_tahun_ajaran_fk" => $this->session->userdata('tahun_ajaran')->id_tahun_ajaran

		);

		$data = $this->MdRombel->tambahRombel($param);
		
		// var_dump($param);
		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil ditambahkan!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!, nama pegawai sudah digunakan"
			]);
		endif;

	}


	public function updateRombel($id){

		$param = array (
			"nama_rombel"=>$this->input->post("nama_rombel"),
			"tingkatan" => $this->input->post("tingkatan"),
			"jurusan" => $this->input->post("jurusan"),
			"id_pegawai_fk" => $this->input->post("walas"),
			"id_tahun_ajaran_fk" => $this->session->userdata('tahun_ajaran')->id_tahun_ajaran

		);
		
		$data = $this->MdRombel->updateRombel($param, $id);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil diupdate!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal diupdate!, nama pegawai sudah digunakan"
			]);
		endif;


	}

	public function deleteRombel($id){

		if($id){

			$data = $this->MdRombel->deleteRombel($id);

			if($data):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil dihapus!"
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data gagal dihapus, data rombel sedang digunakan!"
				]);
			endif;
		}

		else{
			echo json_encode([
				"success" => false,
				"message" => " Data gagal dihapus!"
			]);
		}

	}



}

/* End of file Rombel.php */
/* Location: ./application/controllers/Rombel.php */