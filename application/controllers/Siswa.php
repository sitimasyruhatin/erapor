<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdSiswa');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');

		endif;

		$data = array(
				'page' => 'siswa',
				'master' => 'open'

			);
		// $data["page"] = 'siswa';
		// $data["master"] = 'open';

		$this->load->view('tata-usaha/data-siswa',$data);
	}


	public function getSiswa($id = null){

		$res = array();

		if($id):

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdSiswa->getSiswa($id));

		else:

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdSiswa->getSiswa());

		endif;

		echo json_encode($res);

	}

	public function getCariSiswa(){
		$search = array(
			"nama_siswa" => $this->input->get('q')
		);

		
		$where = array(
			"deleted_at"=> null
		);
		
		$data = $this->MdSiswa->getCariSiswa($where, $search);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function tambahSiswa(){

		$param = array (
			"nis"=>$this->input->post("nis"),
			"nisn" => $this->input->post("nisn"),
			"nama_siswa" => $this->input->post("nama_siswa")

		);

		$data = $this->MdSiswa->tambahSiswa($param);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil ditambahkan!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan, nis/nisn telah digunakan!"
			]);
		endif;

	}

	public function updateSiswa($id){

		$param = array (
			"nis"=>$this->input->post("nis"),
			"nisn" => $this->input->post("nisn"),
			"nama_siswa" => $this->input->post("nama_siswa")
		);

		$data = $this->MdSiswa->updateSiswa($param, $id);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil diupdate!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data gagal diupdate, nis/nisn telah digunakan!"
			]);
		endif;


	}

	public function deleteSiswa($id){

		if($id){

			$data = $this->MdSiswa->deleteSiswa($id);

			if($data):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil dihapus!"
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data gagal dihapus, data siswa sedang digunakan!"
				]);
			endif;
		}

		else{
			echo json_encode([
				"success" => false,
				"message" => " Data gagal dihapus!"
			]);
		}

	}


}

/* End of file Siswa.php */
/* Location: ./application/controllers/Siswa.php */