<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai_lps extends CI_Controller {

	public function index()
	{
		$data = array('title' => 'Input Nilai Sikap | ', );
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('guru_mapel/nilai_lps');
		$this->load->view('footer');
	}


}

/* End of file nilai_lps.php */
/* Location: ./application/controllers/guru_mapel/nilai_lps.php */