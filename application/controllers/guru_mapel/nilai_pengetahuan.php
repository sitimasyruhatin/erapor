<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai_pengetahuan extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}
	public function index()
	{
		$data['page'] = "dashboard_guru_bk";
		$data['title'] = 'Dashboard | Guru BK';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('guru_mapel/nilai_pengetahuan');
		$this->load->view('footer');
	}

}

/* End of file nilai_pengetahuan.php */
/* Location: ./application/controllers/guru_mapel/nilai_pengetahuan.php */