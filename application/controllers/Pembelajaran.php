<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelajaran extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdRombel');

		$this->load->model('MdTahunAjaran');

		$this->load->model('MdAnggotaRombel');

		$this->load->model('MdRolePegawai');

		$this->load->model('MdPembelajaran');

		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');

		endif;

		$data["page"] = 'pembelajaran';
		$this->load->view('tata-usaha/data-pembelajaran',$data);

	}

	public function getPembelajaran($id = null){

		$res = array();

		if($id):

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdPembelajaran->getPembelajaran($id));

		else:

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdPembelajaran->getPembelajaran());

		endif;
		echo json_encode($res);

	}

	public function savePembelajaran($id_rombel){
		$data = $this->input->raw_input_stream;	

		if($data):
			$saved = $this->MdPembelajaran->savePembelajaran($id_rombel, $data);
			if($saved){
				echo json_encode([
					"success" => true,
					"message" => "Data berhasil disimpan!"
				]);
			}else{
				echo json_encode([
					"success" => false,
					"message" => "Data gagal disimpan!"
				]);
			}
		else:
			echo json_encode([
				"success" => false,
				"message" => "Tidak ada data yang disimpan"
			]);
		endif;
	}
}

/* End of file Pembelajaran.php */
/* Location: ./application/controllers/Pembelajaran.php */