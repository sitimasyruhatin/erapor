<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdPegawai');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');

		endif;

		$data = array(
				'page' => 'pegawai',
				'master' => 'open'

			);

		$this->load->view('tata-usaha/data-pegawai',$data);
	}


	public function getPegawai($id = null){

		$res = array();

		if($id):

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdPegawai->getPegawai($id));

		else:

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdPegawai->getPegawai());

		endif;

		echo json_encode($res);

	}

	public function getCariPegawai(){
		$search = array(
			"nama_pegawai" => $this->input->get('q')
		);

		$where = array(
			"deleted_at"=> null,
		);

		$data = $this->MdPegawai->getCariPegawai($where, $search);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	// public function getCariPegawaiForWalas(){

	// 	$where = $this->db->query('SELECT * FROM tb_pegawai WHERE nik NOT IN (select nik_fk from tb_rombel) AND deleted_at IS NULL ');

	// 	$search = array(
	// 		"nama_pegawai" => $this->input->get('q')
	// 	);
	// 	$data = $this->MdPegawai->getCariPegawaiForWalas($where,$search);

	// 	if(!empty($data)):
	// 		echo json_encode([
	// 			"success" => true,
	// 			"message" => " Data berhasil ditemukan!",
	// 			"data" => $data
	// 		]);
	// 	else:
	// 		echo json_encode([
	// 			"success" => false,
	// 			"message" => " Data tidak ditemukan!"
	// 		]);
	// 	endif;
	// }

	public function tambahPegawai(){

		$param = array (
			"nik"=>$this->input->post("nik"),
			"nama_pegawai" => $this->input->post("nama_pegawai"),
			"password" => md5($this->input->post("password")),

		);

		$data = $this->MdPegawai->tambahPegawai($param);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil ditambahkan!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan, nik telah digunakan!"
			]);
		endif;

	}

	public function updatePegawai($id){

		$param = array (
			"nik" => $this->input->post("nik"),
			"nama_pegawai" => $this->input->post("nama_pegawai")
		);

		if($this->input->post("password") != NULL){
			$param["password"] = md5($this->input->post("password"));
		}


		$data = $this->MdPegawai->updatePegawai($param, $id);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil diupdate!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal diupdate, nik telah digunakan!"
			]);
		endif;


	}


	public function deletePegawai($id){

		if($id){

			$data = $this->MdPegawai->deletePegawai($id);

			if($data):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil dihapus!"
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data gagal dihapus, data pegawai sedang digunakan!"
				]);
			endif;
		}

		else{
			echo json_encode([
				"success" => false,
				"message" => " Data gagal dihapus!"
			]);
		}

	}

}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */