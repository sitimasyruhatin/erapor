<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiEkskul extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdNilaiEkskul');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isKesiswaan') === TRUE):

			redirect('page/page-404');

		endif;
		$data["page"] = 'nilaiekskul';
		$this->load->view('kesiswaan/nilai-ekskul',$data);
	}

	public function getNilaiEkskul($id_rombel, $smt){

		$res = array();

		$where = array(
			"deleted_at"=> null,
			"id_rombel" => $id_rombel,
			"semester" => $smt
		);

		if($id_rombel && $smt):
			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdNilaiEkskul->getNilaiEkskul($where));


		endif;

		echo json_encode($res);
	}

	public function saveNilaiEkskul(){
		$param = array(
			"rombel" => $this->input->post("rombel"),
			"semester" => $this->input->post("semester"),
			"penilaian" => $this->input->post("penilaian"),
		);
				
		$saved = $this->MdNilaiEkskul->saveNilaiEkskul($param);
			if($saved){
				echo json_encode([
					"success" => true,
					"message" => "Data berhasil disimpan!"
				]);
			} else {
				echo json_encode([
					"success" => false,
					"message" => "Data gagal ditambahkan!"
				]);
			}
	}
}

/* End of file NilaiEkskul.php */
/* Location: ./application/controllers/NilaiEkskul.php */