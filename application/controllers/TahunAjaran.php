<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TahunAjaran extends CI_Controller {


    public function __construct(){

        parent::__construct();

        $this->load->model('MdTahunAjaran');

        if (!$this->session->userdata('status') == 'logged'):

            redirect('auth');

        endif;

    }

    public function index()
    {
        $data["page"] = "tahunajaran";
        
        if (!$this->session->userdata('isTataUsaha') === TRUE):

            redirect('page/page-404');

        endif;
        
        $data["tahun_ajaran"] = $this->MdTahunAjaran->getTahunAJaran();
        $this->load->view('tata-usaha/data-tahun-ajaran',$data);
    }

    public function getTahunAjaran($id = null){

        $res = array();

        if($id):

            $res = array (
                "success" => true,
                "message" => "Data berhasil ditemukan",
                "data" => $this->MdTahunAjaran->getTahunAJaran($id));

        else:

            $res = array (
                "success" => true,
                "message" => "Data berhasil ditemukan",
                "data" => $this->MdTahunAjaran->getTahunAJaran());

        endif;

        echo json_encode($res);

    }
    
    public function getActiveTahunAjaran(){

        $data = $this->session->userdata('tahun_ajaran');

        if(!empty($data)):
            echo json_encode([
                "success" => true,
                "message" => " Data berhasil ditemukan!",
                "data" => $data
            ]);
        else:
            echo json_encode([
                "success" => false,
                "message" => " Data tidak ditemukan!"
            ]);
        endif;      

    }


    public function getCariTahunAjaran(){
        $search = array(
            "tahun_ajaran" => $this->input->get('q')
        );

        $where = array(
            "deleted_at"=> null
        );

        $data = $this->MdTahunAjaran->getCariTahunAjaran($where, $search);
        
        if(!empty($data)):
            echo json_encode([
                "success" => true,
                "message" => " Data berhasil ditemukan!",
                "data" => $data
            ]);
        else:
            echo json_encode([
                "success" => false,
                "message" => " Data tidak ditemukan!"
            ]);
        endif;
    }

    public function getCariTahunAjaranChange($id_tahun_ajaran){

        $this->session->unset_userdata('tahun_ajaran');

        $this->session->set_userdata(array(
            "tahun_ajaran" => $this->MdTahunAjaran->getTahunAjaran($id_tahun_ajaran)[0] 
        ));

        $res = array (
            "success" => true,
            "message" => "Berhasil ubah tahun ajaran"); 

        echo json_encode($res);
    }


    public function tambahTahunAjaran(){
        $param = array (
            "tahun_ajaran" => $this->input->post("tahun_ajaran")
        );


        if(!empty($param)):
            $data =  $this->MdTahunAjaran->tambahTahunAjaran($param);
            echo json_encode([
                "success" => true,
                "message" => " Data berhasil ditambahkan!"
            ]);
        else:
            echo json_encode([
                "success" => false,
                "message" => " Data gagal ditambahkan!"
            ]);
        endif;

    }

    public function updateTahunAjaran($id){

        $param = array (
            "tahun_ajaran" => $this->input->post("tahun_ajaran"),
            "status" => $this->input->post("status")
        );


        $data = $this->MdTahunAjaran->updateTahunAjaran($param, $id);

        if($data):
            echo json_encode([
                "success" => true,
                "message" => " Data berhasil dirubah!"
            ]);
        else:
            echo json_encode([
                "success" => false,
                "message" => " Data gagal dirubah!"
            ]);
        endif;

    }
    
    public function deleteTahunAjaran($id){

        if($id){

            $data = $this->MdTahunAjaran->deleteTahunAjaran($id);

            if($data):
                echo json_encode([
                    "success" => true,
                    "message" => " Data berhasil dihapus!"
                ]);
            else:
                echo json_encode([
                    "success" => false,
                    "message" => " Data gagal dihapus, tahun ajaran sedang digunakan!"
                ]);
            endif;
        }

        else{
            echo json_encode([
                "success" => false,
                "message" => " Data gagal dirubah!"
            ]);
        }

    }

}

/* End of file TahunAjaran.php */
/* Location: ./application/controllers/tata_usaha/TahunAjaran.php */