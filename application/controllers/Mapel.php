<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdMapel');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');

		endif;

		$data = array(
				'page' => 'mapel',
				'master' => 'open'

			);

		$this->load->view('tata-usaha/data-mapel',$data);
	}


	public function getMapel($id = null){

		$res = array();

		if($id):

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdMapel->getMapel($id));

		else:

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdMapel->getMapel());

		endif;

		echo json_encode($res);

	}

	public function getCariMapelNilai($id_rombel = null){

		$id_pegawai_login = $this->session->userdata('id_pegawai');

		$search = array(
			"nama_mapel" => $this->input->get('q')
		);

		if ($id_rombel != null) {
			$where = array(
				"id_pegawai" => $id_pegawai_login,
				"id_tahun_ajaran_fk" => $this->session->userdata('tahun_ajaran')->id_tahun_ajaran,
				"id_rombel"=> $id_rombel,
				"id_mapel !=" => 1
			);

			$data = $this->MdMapel->getCariMapel($where, $search);

			if(!empty($data)):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil ditemukan!",
					"data" => $data
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data tidak ditemukan!"
				]);
			endif;
		}
		else{
			$where = array(
				"id_pegawai" => $id_pegawai_login,
				"id_tahun_ajaran_fk" => $this->session->userdata('tahun_ajaran')->id_tahun_ajaran,
				"id_mapel !=" => 1
			);

			$data = $this->MdMapel->getCariMapel($where, $search);

			if(!empty($data)):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil ditemukan!",
					"data" => $data
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data tidak ditemukan!"
				]);
			endif;
		}

		
	}

	public function tambahMapel(){
		$jenis_mapel = explode("|",$this->input->post("jenis_mapel"));
		$kelompok_mapel = $jenis_mapel[0];
		$jenis_rombel = $jenis_mapel[1];

		$param = array (
			"nama_mapel" => $this->input->post("nama_mapel"),
			"sngkt_mapel" => $this->input->post("sngkt_mapel"),
			"kelompok_mapel" => $kelompok_mapel,
			"jenis_rombel" => $jenis_rombel
		);

		$data = $this->MdMapel->tambahMapel($param);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil ditambahkan!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		endif;

	}

	public function updateMapel($id){

		$jenis_mapel = explode("|",$this->input->post("jenis_mapel"));
		$kelompok_mapel = $jenis_mapel[0];
		$jenis_rombel = $jenis_mapel[1];

		$param = array (
			"nama_mapel" => $this->input->post("nama_mapel"),
			"sngkt_mapel" => $this->input->post("sngkt_mapel"),
			"kelompok_mapel" => $kelompok_mapel,
			"jenis_rombel" => $jenis_rombel
		);

		$data = $this->MdMapel->updateMapel($param, $id);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil diupdate!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data gagal diupdate!"
			]);
		endif;


	}
	public function deleteMapel($id){

		if($id){

			$data = $this->MdMapel->deleteMapel($id);

			// if($data):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil dihapus!"
				]);
			// else:
			// 	echo json_encode([
			// 		"success" => false,
			// 		"message" => " Data gagal dihapus!, mata pelajaran sedang digunakan"
			// 	]);
			// endif;
		}

		else{
			echo json_encode([
				"success" => false,
				"message" => " Data gagal dihapus!"
			]);
		}

	}
}

/* End of file Mapel.php */
/* Location: ./application/controllers/Mapel.php */