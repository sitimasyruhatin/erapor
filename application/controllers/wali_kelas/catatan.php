<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catatan extends CI_Controller {

	public function index()
	{
		$data = array('title' => 'Dashboard Guru BK | ', );
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('wali_kelas/catatan');
		$this->load->view('footer');
	}


}

/* End of file catatan.php */
/* Location: ./application/controllers/wali_kelas/catatan.php */