<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ketidakhadiran extends CI_Controller {

	public function index()
	{
		$data = array('title' => 'Dashboard Guru BK | ', );
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('wali_kelas/ketidakhadiran');
		$this->load->view('footer');
	}

}

/* End of file ketidakhadiran.php */
/* Location: ./application/controllers/wali_kelas/ketidakhadiran.php */