<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ekskul extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdEkskul');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');

		endif;
		$data = array(
				'page' => 'ekskul',
				'master' => 'open'

			);
		$this->load->view('tata-usaha/data-ekskul',$data);
	}


	public function getEkskul($id = null){

		$res = array();

		if($id):

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdEkskul->getEkskul($id));

		else:

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdEkskul->getEkskul());

		endif;

		echo json_encode($res);

	}

	public function getCariEkskul(){

		$search = array(
			"nama_ekskul" => $this->input->get('q')
		);
		
		$where = array(
			"deleted_at"=> null
		);
		
		$data = $this->MdEkskul->getCariEkskul($where, $search);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function tambahEkskul(){

		$param = array (
			"nama_ekskul"=>$this->input->post("nama_ekskul"),
		);

		$data = $this->MdEkskul->tambahEkskul($param);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil ditambahkan!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!"
			]);
		endif;

	}
	
	public function updateEkskul($id){

		$param = array (
			"nama_ekskul"=>$this->input->post("nama_ekskul"),
		);

		$data = $this->MdEkskul->updateEkskul($param, $id);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil diupdate!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data gagal diupdate!"
			]);
		endif;


	}
	
	public function deleteEkskul($id){

		if($id){

			$data = $this->MdEkskul->deleteEkskul($id);

			if($data):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil dihapus!"
				]);
			else:
				echo json_encode([
					"success" => false,
					"message" => " Data gagal dihapus, data ekskul sedang digunakan pada peniaian!"
				]);
			endif;
			
		}

		else{
			echo json_encode([
				"success" => false,
				"message" => " Data gagal dihapus!"
			]);
		}

	}

}

/* End of file Ekskul.php */
/* Location: ./application/controllers/Ekskul.php */