<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai_sikap extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}
	public function index()
	{
		$data['page'] = 'nilai_sikap';
		$data['title'] = 'Input Nilai Sikap | ';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('guru_bk/input-sikap');
		$this->load->view('footer');
	}


}

/* End of file nilai_sikap.php */
/* Location: ./application/controllers/guru-bk/nilai_sikap.php */