<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ekstrakurikuler extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}
	public function index()
	{		
		$data['page'] = 'ekstrakurikuler';
		$data['title'] = 'Data Ekstrakurikuler | Tata Usahass';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/data-ekstrakurikuler');
		$this->load->view('footer');
	}

}

/* End of file guru.php */
/* Location: ./application/controllers/admin/guru.php */