<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leger extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}
	public function index()
	{		
		$data['page'] = 'leger';
		$data['title'] = 'Ledger | Tata Usaha';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/data-ledger-rapot',$data);
		$this->load->view('footer');
	}

	public function cetak_rapor(){
		$data['page'] = 'cetak_rapor';
		$data['title'] = 'Cetak Rapor | Tata Usaha';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/cetak-rapor',$data);
		$this->load->view('footer');
	}
	public function cetak_lps(){
		$data['page'] = 'cetak_lps';
		$data['title'] = 'Cetak LPS | Tata Usaha';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/cetak-rapot',$data);
		$this->load->view('footer');
	}


}

/* End of file ledger.php */
/* Location: ./application/controllers/admin/ledger.php */