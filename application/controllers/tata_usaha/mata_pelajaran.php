<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mata_pelajaran extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}
	public function index()
	{		
		$data['page'] = 'data_mata_pelajaran';
		$data['title'] = 'Data Mata Pelajaran | Tata Usaha';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/data-mata-pelajaran');
		$this->load->view('footer');
	}

}

/* End of file mata_pelajaran.php */
/* Location: ./application/controllers/admin/mata_pelajaran.php */