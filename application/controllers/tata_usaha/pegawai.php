<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}
	public function index()
	{		
		$data['page'] = 'data_guru';
		$data['title'] = 'Data Pegawai | Tata Usaha';
		$data['pegawai']= $this->m_pegawai->select_all();

		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/data-pegawai',$data);
		$this->load->view('footer');
	}

	// public function get_pegawai(){
	// 	$res["data"]=  $this->m_pegawai->select_all();
	// }

	public function tambah_pegawai(){

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = '100';
		$config['encrypt_name'] = TRUE;
		$config['overwrite'] = FALSE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$upload = $this->upload->do_upload('foto');
		$upload_data = $this->upload->data();
		$foto = $upload_data['file_name'];

		$username = $this->input->post('nik');
		$explode = explode("-",$this->input->post('tanggal_lahir'));
		$password = "";
		for ($i=0; $i <3 ; $i++) { 
			$password .= $explode[$i];
		}
		
		$data_pegawai = $this->m_pegawai->select_all();

		foreach ($data_pegawai as $pegawai) {
			if ($pegawai->nik == $this->input->post('nik')) {
				$this->session->set_flashdata('errorMessage', 'NIK sudah digunakan!');
				redirect('tata_usaha/pegawai','refresh');
			}
			else {
				$insert_user= array(
					'username' => $this->input->post('nik'),
					'password' => $password,
					'role'=> $this->input->post('jabatan')
				);
				if(!empty($insert_user)){
					$insert = $this->m_user->insert($insert_user);
					$this->session->set_flashdata('successMessage', 'User berhasil ditambahkan!');
					redirect('tata_usaha/pegawai','refresh');
				}
				else{
					$this->session->set_flashdata('errMessage', 'User gagal ditambahkan!');
					redirect('tata_usaha/pegawai','refresh');
				}

				$data_user = $this->m_user->select_all();

				foreach ($ambil_data as $data) {
					if ($data->role == $this->input->post('jabatan')) {
						$insert_pegawai = array(
							'nik' => $this->input->post('nik'),
							'nama_lengkap' => $this->input->post('nama_lengkap'),
							'tanggal_lahir'=> $this->input->post('tanggal_lahir'),
							'jabatan' => $this->input->post('jabatan'),
							'foto'=>$foto,
							'status' => 0,
							'id_user' => $data->id_user
						);
					}
				}
				if(!empty($insert_pegawai)){
					$insert = $this->m_pegawai->insert($insert_pegawai);
					$this->session->set_flashdata('successMessage', 'Guru/pegawai berhasil ditambahkan!');
					redirect('tata_usaha/pegawai','refresh');
				}
				else{
					$this->session->set_flashdata('errMessage', 'Guru/pegawai gagal ditambahkan!');
					redirect('tata_usaha/pegawai','refresh');
				}
				
			}
		}

		

		

		

		
	}

	public function nik_exist($username)
	{
		if ($this->m_user->check_entry('username',$username)) {
			$this->form_validation->set_message('is_username_exist','Username tidak tersedia, silahkan pilih username lain.');
			return false;
		} else {
			return true;
		}
	}
	public function hapus_pegawai($id_pegawai){
		if (!empty($id_pegawai)) {
			$this->m_pegawai->delete($id_pegawai);
			$this->session->set_flashdata('successMessage', 'Data pegawai telah dihapus!');
			redirect('admin/pegawai', 'refresh');
		}
		else{
			$this->session->set_flashdata('errMessage', 'Data pegawai gagal dihapus!');
			redirect('admin/pegawai', 'refresh');
		}
	}


}

/* End of file guru.php */
/* Location: ./application/controllers/admin/guru.php */