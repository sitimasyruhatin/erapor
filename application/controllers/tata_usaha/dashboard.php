<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}

	public function index()
	{
		$data['page'] = 'dashboard_admin';
		$data['title'] = 'Dashboard | Tata Usaha';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/dashboard',$data);
		$this->load->view('footer');
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/admin/dashboard.php */