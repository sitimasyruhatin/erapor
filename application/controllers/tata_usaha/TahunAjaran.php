<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TahunAjaran extends CI_Controller {


	public function __construct(){

        parent::__construct();

        $this->load->model('MdTahunAjaran');
       

        if (!$this->session->userdata('status') == 'logged'):

            redirect('auth/login');

        endif;

    }

	public function index()
	{
		$sidebar["page"] = "tahunajaran";
		if (!$this->session->userdata('isTataUsaha') === TRUE):

            redirect('page/page-404');

        endif;

        $this->load->view('tata-usaha/data-tahun-ajaran',$sidebar);
	}

	public function getTahunAjaran($id = null){

        if($id){

            $res = $this->MdTahunPenilaian->getTahunPenilaian($id);

        } else {

            $res = $this->MdTahunPenilaian->getTahunPenilaian();

        }


    }



}

/* End of file TahunAjaran.php */
/* Location: ./application/controllers/tata_usaha/TahunAjaran.php */