<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}
	public function index()
	{
		$data['page'] = 'data_siswa';
		$data['title']= 'Data Siswa | Tata Usaha';
		$data['siswa']= $this->m_siswa->select_all();

		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/data-siswa',$data);
		$this->load->view('footer');
	}

	public function tambah_siswa()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = '100';
		$config['encrypt_name'] = TRUE;
		$config['overwrite'] = FALSE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('foto_profil')){
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('warn', $error);
			redirect('tata_usaha/siswa','refresh');
		}
		else{
			
			$data = $this->upload->data();
			$foto_profil = $data['file_name'];

			$tanggal_lahir = date('Y-m-d',strtotime($this->input->post('tanggal_lahir')));
			$tambah_siswa = array(
				'nis' => $this->input->post('nis'),
				'nisn' => $this->input->post('nisn'), 
				'nama_lengkap' => $this->input->post('nama_lengkap'),
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'tanggal_lahir' => $tanggal_lahir,
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'agama' => $this->input->post('agama'),
				'status_keluarga' => $this->input->post('status_keluarga'),
				'anak_ke' => $this->input->post('anak_ke'),
				'alamat' => $this->input->post('alamat'),
				'telpon_rumah' => $this->input->post('telpon_rumah'),
				'sekolah_asal' => $this->input->post('sekolah_asal'),
				'kelas_diterima' => $this->input->post('kelas_diterima'),
				'tanggal_diterima' => $this->input->post('tanggal_diterima'),
				'nama_ayah' => $this->input->post('nama_ayah'),
				'nama_ibu' => $this->input->post('nama_ibu'),
				'alamat_ortu' => $this->input->post('alamat_ortu'),
				'telpon_ortu' => $this->input->post('telpon_ortu'),
				'kerja_ayah' => $this->input->post('kerja_ayah'),
				'kerja_ibu' => $this->input->post('kerja_ibu'),
				'nama_wali' => $this->input->post('nama_wali'),
				'telpon_wali' => $this->input->post('telpon_wali'),
				'alamat_wali' => $this->input->post('alamat_wali'),
				'kerja_wali' => $this->input->post('kerja_wali'),
				'foto_profil' => $foto_profil
			);
			$insert_id = $this->m_siswa->insert($tambah_siswa);
			redirect('tata_usahas/siswa/'.$insert_id,'refresh');
		}
	}

	public function edit_siswa($id_siswa)
	{
		if ($FILES['picture']['size'] == 0) {
			$foto_profil = $this->input->post('current_picture');
		}
		else{
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '100';
			$config['encrypt_name'] = TRUE;
			$config['overwrite'] = FALSE;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('foto_profil')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('warn', $error);
				redirect('tata_usaha/siswa','refresh');
			}
			else{

				$data = $this->upload->data();
				$foto_profil = $data['file_name'];


			}
		}
		$tanggal_lahir = date('Y-m-d',strtotime($this->input->post('tanggal_lahir')));
		$edit_siswa = array(
			'nis' => $this->input->post('nis'),
			'nisn' => $this->input->post('nisn'), 
			'nama_lengkap' => $this->input->post('nama_lengkap'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'tanggal_lahir' => $tanggal_lahir,
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'agama' => $this->input->post('agama'),
			'status_keluarga' => $this->input->post('status_keluarga'),
			'anak_ke' => $this->input->post('anak_ke'),
			'alamat' => $this->input->post('alamat'),
			'telpon_rumah' => $this->input->post('telpon_rumah'),
			'sekolah_asal' => $this->input->post('sekolah_asal'),
			'kelas_diterima' => $this->input->post('kelas_diterima'),
			'tanggal_diterima' => $this->input->post('tanggal_diterima'),
			'nama_ayah' => $this->input->post('nama_ayah'),
			'nama_ibu' => $this->input->post('nama_ibu'),
			'alamat_ortu' => $this->input->post('alamat_ortu'),
			'telpon_ortu' => $this->input->post('telpon_ortu'),
			'kerja_ayah' => $this->input->post('kerja_ayah'),
			'kerja_ibu' => $this->input->post('kerja_ibu'),
			'nama_wali' => $this->input->post('nama_wali'),
			'telpon_wali' => $this->input->post('telpon_wali'),
			'alamat_wali' => $this->input->post('alamat_wali'),
			'kerja_wali' => $this->input->post('kerja_wali'),
			'foto_profil' => $foto_profil
		);
		$insert_id = $this->m_siswa->insert($tambah_siswa);
		redirect('tata_usaha/siswa/'.$insert_id,'refresh');
	}

	public function hapus_siswa($id_siswa){
		if (!empty($id_siswa)) {
			$this->m_siswa->delete($id_siswa);
			$this->session->set_flashdata('successMessage', 'Data siswa telah dihapus!');
			redirect('tata_usaha/siswa', 'refresh');
		}
		else{
			$this->session->set_flashdata('errMessage', 'Data siswa gagal dihapus!');
			redirect('tata_usaha/siswa', 'refresh');
		}
	}
}

/* End of file siswa.php */
/* Location: ./application/controllers/tata_usaha/siswa.php */