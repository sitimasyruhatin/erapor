
'<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}
	public function index()
	{		
		$data['page'] = 'data_kelas';
		$data['title'] = 'Data Kelas | Tata Usaha';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/data-kelas');
		$this->load->view('footer');
	}
	public function anggota_kelas()
	{	
		$data['page'] = 'data_kelas';	
		$data['title'] = 'Anggota Kelas | Tata Usaha';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/data-anggota-kelas');
		$this->load->view('footer');
	}

	public function pembelajaran()
	{		
		$data['page'] = 'data_kelas';
		$data['title'] = 'Data Kelas | Tata Usaha';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/data-pembelajaran');
		$this->load->view('footer');
	}

}

/* End of file guru.php */
/* Location: ./application/controllers/admin/guru.php */