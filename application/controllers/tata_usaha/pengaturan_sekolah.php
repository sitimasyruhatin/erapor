<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan_sekolah extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('a_username') == null){
			redirect('main', 'refresh');
		}
	}
	public function index()
	{
		$data['page'] = 'profil_sekolah';
		$data['title'] = 'Profil Sekolah | Tata Usaha';
		$this->load->view('html_head',$data);
		$this->load->view('header',$data);
		$this->load->view('sidebar',$data);
		$this->load->view('tata_usaha/pengaturan-sekolah');
		$this->load->view('footer');
	}

}

/* End of file profil-sekolah.php */
/* Location: ./application/controllers/admin/profil-sekolah.php */