<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnggotaRombel extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdRombel');

		$this->load->model('MdTahunAjaran');

		$this->load->model('MdAnggotaRombel');

		$this->load->model('MdRolePegawai');

		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');

		endif;

		$data["page"] = 'anggotarombel';

		$this->load->view('tata-usaha/data-anggota-rombel',$data);

	}

	public function getAnggotaRombel($id = null){

		$res = array();

		if($id):

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdAnggotaRombel->getAnggotaRombel($id));

		else:

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdAnggotaRombel->getAnggotaRombel());

		endif;

		echo json_encode($res);

	}

	public function getCariAnggotaRombel($id_rombel){
		$search = array(
			"nama_siswa" => $this->input->get('q')
		);

		$where = array(
			"deleted_at"=> null,
			"id_rombel_fk"=> $id_rombel
		);

		$data = $this->MdAnggotaRombel->getCariAnggotaRombel($where, $search);

		if(!empty($data)):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil ditemukan!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data tidak ditemukan!"
			]);
		endif;
	}

	public function tambahAnggotaRombel(){

		$param = array (
			"id_rombel_fk"=>$this->input->post("rombel"),
			"id_siswa_fk" => $this->input->post("siswa")
		);

		$data = $this->MdAnggotaRombel->tambahAnggotaRombel($param);

		// var_dump($param);
		if($data):
			echo json_encode([
				"success" => true,
				"message" => "Data berhasil ditambahkan!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => "Data gagal ditambahkan!, siswa sudah ditambahkan"
			]);
		endif;

	}

	public function copyRombel($old, $new){

		$data = $this->MdAnggotaRombel->copyRombel($old, $new);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil disalin!",
				"data" => $data
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data gagal disalin, beberapa siswa sudah ditambahkan!"
			]);
		endif;
	}

	public function deleteAnggotaRombel($id){

		if($id){

			$data = $this->MdAnggotaRombel->deleteAnggotaRombel($id);

			// if($data):
				echo json_encode([
					"success" => true,
					"message" => " Data berhasil dihapus!"
				]);
			// else:
			// 	echo json_encode([
			// 		"success" => false,
			// 		"message" => " Data gagal dihapus, data siswa sedang digunakan pada penilaian!"
			// 	]);
			// endif;
		}

		else{
			echo json_encode([
				"success" => false,
				"message" => " Data gagal dihapus!"
			]);
		}

	}

}

/* End of file AnggotaRombel.php */
/* Location: ./application/controllers/AnggotaRombel.php */