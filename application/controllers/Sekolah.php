<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('MdSekolah');


		if (!$this->session->userdata('status') == 'logged'):

			redirect('auth');

		endif;

	}

	public function index()
	{
		if (!$this->session->userdata('isTataUsaha') === TRUE):

			redirect('page/page-404');
		endif;

		$sekolah = $this->MdSekolah->getSekolah();
		$data = array(
				'page' => 'profil-sekolah',
				'master' => 'open',
				'sekolah'=> $sekolah[0]
			);
		$this->load->view('tata-usaha/profil-sekolah',$data);
	}


	public function getSekolah($id = null){

		$res = array();

		if($id):

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdSekolah->getSekolah($id));

		else:

			$res = array (
				"success" => true,
				"message" => "Data berhasil ditemukan",
				"data" => $this->MdSekolah->getSekolah());

		endif;

		echo json_encode($res);
	}

	public function updateSekolah($id){

		$param = array (
			"nama_sekolah"=>$this->input->post("nama_sekolah"),
			"nama_kepsek"=>$this->input->post("nama_kepsek"),
			"nik_kepsek"=>$this->input->post("nik_kepsek"),
			"alamat_sekolah"=>$this->input->post("alamat"),
			"kkm"=>$this->input->post("kkm")
		);

		$data = $this->MdSekolah->updateSekolah($param, $id);

		if($data):
			echo json_encode([
				"success" => true,
				"message" => " Data berhasil diupdate!"
			]);
		else:
			echo json_encode([
				"success" => false,
				"message" => " Data gagal diupdate!"
			]);
		endif;


	}

}

/* End of file Sekolah.php */
/* Location: ./application/controllers/Sekolah.php */