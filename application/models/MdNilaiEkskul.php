<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdNilaiEkskul extends CI_Model {

	public function getNilaiEkskul($where = array()){

		$this->db->select('*');

		$this->db->where($where);

		return $this->db->get('vw_nilai_ekskul')->result();
	}

	public function saveNilaiEkskul($param = array()){		
        foreach ($param['penilaian'] as $key=>$value) {
			$predikat_wajib = '';
			$predikat_pilihan = '';

            if ($param['penilaian'][$key]['nilai_wajib'] >= 86) {
                $predikat_wajib = 'A';
            } elseif ($param['penilaian'][$key]['nilai_wajib'] >= 75) {
                $predikat_wajib = 'B';
            } elseif ($param['penilaian'][$key]['nilai_wajib'] >= 70) {
                $predikat_wajib = 'C';
            } else {
                $predikat_wajib = 'D';
            }
            
            if ($param['penilaian'][$key]['nilai_pilihan'] >= 86) {
                $predikat_pilihan = 'A';
            } elseif ($param['penilaian'][$key]['nilai_pilihan'] >= 75) {
                $predikat_pilihan = 'B';
            } elseif ($param['penilaian'][$key]['nilai_pilihan'] >= 70) {
                $predikat_pilihan = 'C';
            } else {
                $predikat_pilihan = 'D';
            }
            
            $nilai_ekskul = array(
                'semester' => $param['semester'],
                'id_anggota_rombel_fk' => $param['penilaian'][$key]['id_anggota_rombel'],
                'id_ekskul_wajib_fk' => $param['penilaian'][$key]['id_ekskul_wajib_fk'],
                'predikat_wajib'=> $predikat_wajib,
                'nilai_wajib' => $param['penilaian'][$key]['nilai_wajib'],
                'deskripsi_wajib' => $param['penilaian'][$key]['deskripsi_wajib'],
                'id_ekskul_pilihan_fk' => $param['penilaian'][$key]['id_ekskul_pilihan_fk'],
                'nilai_pilihan' => $param['penilaian'][$key]['nilai_pilihan'],
                'predikat_pilihan'=> $predikat_pilihan,
                'deskripsi_pilihan' => $param['penilaian'][$key]['deskripsi_pilihan'],
			);

            $check = $this->db->get_where('tb_nilai_ekskul', ['id_nilai_ekskul' => $param['penilaian'][$key]['id_nilai_ekskul']])->num_rows();						
            if ($check>0) {
                $update = $this->db->where(['id_nilai_ekskul' => $param['penilaian'][$key]['id_nilai_ekskul']])->update('tb_nilai_ekskul', $nilai_ekskul);
            } else {
                $insert = $this->db->insert("tb_nilai_ekskul", $nilai_ekskul);
            }
        }

		if($this->db->affected_rows() >= 0)	return true;		
		return false;
	}
}

/* End of file MdNilaiEkskul.php */
/* Location: ./application/models/MdNilaiEkskul.php */