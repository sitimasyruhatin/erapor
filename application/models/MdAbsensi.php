<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdAbsensi extends CI_Model {

	public function getAbsensi($where = array()){

		$this->db->select('*');

		$this->db->where($where);

		return $this->db->get('vw_absensi')->result(); 
	}

	public function saveAbsensi($param = array()){

		foreach($param['penilaian'] as $key=>$value):

			$data = array(
				'semester' => $param['semester'],
				'id_anggota_rombel_fk' => $param['penilaian'][$key]['id_anggota_rombel'],
				'sakit' => $param['penilaian'][$key]['sakit'],
				'ijin' => $param['penilaian'][$key]['ijin'],
				'alpa' => $param['penilaian'][$key]['alpa']
			);

			$check = $this->db->get_where('tb_absensi', ['id_absensi' => $param['penilaian'][$key]['id_absensi']])->num_rows();
			if($check!==0){
				$update = $this->db->where(['id_absensi' => $param['penilaian'][$key]['id_absensi']])->update('tb_absensi',$data);
			}
			else{
				$insert = $this->db->insert("tb_absensi", $data);
			}

		endforeach;

		if($this->db->affected_rows() >= 0):
			return true;
		endif;

		return false;
	}


}

/* End of file MdAbsensi.php */
/* Location: ./application/models/MdAbsensi.php */