<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdNilaiPengetahuan extends CI_Model {

	public function getNilaiP($where = array()){

		$this->db->select('*');

		$this->db->where($where);

		return $this->db->get('vw_nilai_pengetahuan')->result(); 
	}

	public function getDeskripsiP($where = array()){

		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('predikat', 'asc');
		return $this->db->get('vw_format_nilai')->result(); 
	}
	public function saveDeskripsiP($param = array()){

		foreach($param['deskripsi'] as $key=>$value):

			$data = array(
				'id_pembelajaran_fk' => $param['deskripsi'][$key]['id_pembelajaran'],
				'semester' => $param['deskripsi'][$key]['semester'],
				'predikat' => $param['deskripsi'][$key]['predikat'],
				'deskripsi_nilai' => $param['deskripsi'][$key]['deskripsi_nilai'],
				'kategori_nilai' => $param['deskripsi'][$key]['kategori_nilai'],
			);

			$check = $this->db->get_where('tb_format_nilai', ['id_format' => $param['deskripsi'][$key]['id_format']])->num_rows();

			if($check!==0){
				$update = $this->db->where(['id_format' => $param['deskripsi'][$key]['id_format']])->update('tb_format_nilai',$data);
			}

			else{
				$insert = $this->db->insert("tb_format_nilai", $data);
			}

		endforeach;

		if($this->db->affected_rows() >= 0):
			return true;
		endif;

		return false;
	}

	public function saveNilaiP($param = array()){
		$penilaian = $param['penilaian'];
		foreach($penilaian as $key=>$value):

			$data = array(
				'semester' => $param['semester'],
				'id_anggota_rombel_fk' => $penilaian[$key]['id_anggota_rombel'],
				'id_pembelajaran_fk' => $penilaian[$key]['id_pembelajaran'],
				'kd1_kuis' => $penilaian[$key]['kd1_kuis'],
				'kd1_uh' => $penilaian[$key]['kd1_uh'],
				'kd1_lisan' => $penilaian[$key]['kd1_lisan'],
				'kd1_tugas' => $penilaian[$key]['kd1_tugas'],
				'kd2_kuis' => $penilaian[$key]['kd2_kuis'],
				'kd2_uh' => $penilaian[$key]['kd2_uh'],
				'kd2_lisan' => $penilaian[$key]['kd2_lisan'],
				'kd2_tugas' => $penilaian[$key]['kd2_tugas'],
				'kd3_kuis' => $penilaian[$key]['kd3_kuis'],
				'kd3_uh' => $penilaian[$key]['kd3_uh'],
				'kd3_lisan' => $penilaian[$key]['kd3_lisan'],
				'kd3_tugas' => $penilaian[$key]['kd3_tugas'],
				'kd4_kuis' => $penilaian[$key]['kd4_kuis'],
				'kd4_uh' => $penilaian[$key]['kd4_uh'],
				'kd4_lisan' => $penilaian[$key]['kd4_lisan'],
				'kd4_tugas' => $penilaian[$key]['kd4_tugas'],
				'kd5_kuis' => $penilaian[$key]['kd5_kuis'],
				'kd5_uh' => $penilaian[$key]['kd5_uh'],
				'kd5_lisan' => $penilaian[$key]['kd5_lisan'],
				'kd5_tugas' => $penilaian[$key]['kd5_tugas'],
				'kd6_kuis' => $penilaian[$key]['kd6_kuis'],
				'kd6_uh' => $penilaian[$key]['kd6_uh'],
				'kd6_lisan' => $penilaian[$key]['kd6_lisan'],
				'kd6_tugas' => $penilaian[$key]['kd6_tugas'],
				'pts' => $penilaian[$key]['pts'],
				'pas' => $penilaian[$key]['pas']
			);

			$check = $this->db->get_where('tb_nilai_pengetahuan', ['id_nilai_p' => $penilaian[$key]['id_nilai_p']])->num_rows();

			if($check!==0){
				$update = $this->db->where(['id_nilai_p' => $penilaian[$key]['id_nilai_p']])->update('tb_nilai_pengetahuan',$data);
			}

			else{
				$insert = $this->db->insert("tb_nilai_pengetahuan", $data);
			}

		endforeach;

		if($this->db->affected_rows() >= 0):
			return true;
		endif;

		return false;
	}

}

/* End of file MdNilaiPengetahuan.php */
/* Location: ./application/models/MdNilaiPengetahuan.php */