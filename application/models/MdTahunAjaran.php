<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdTahunAjaran extends CI_Model {

	public function getTahunAjaran($id = null){

		$this->db->order_by('tahun_ajaran desc');

		if($id):

			$result = $this->db->get_where('tb_tahun_ajaran', array('id_tahun_ajaran'=> $id, 'deleted_at'=>null))->result(); 

		else:

			$result = $this->db->get_where('tb_tahun_ajaran', array('deleted_at'=>null))->result(); 

		endif;

		if (count($result)>0):

			return $result;

		else:

			return [];

		endif;	

	}

	public function getActiveTahunAjaran(){

		$this->db->select('*');

		$this->db->where('status', 1);
		
		$this->db->limit(1);

		$result = $this->db->get('tb_tahun_ajaran')->result(); 

		return $result;

	}

	public function getCariTahunAjaran($where = array(), $like = array()){

		if(!empty($where)):

			$this->db->where($where);

		endif;

		if(!empty($like)){

			$this->db->like($like);
		}

		$result = $this->db->get('tb_tahun_ajaran')->result(); 
		
		if (count($result)>0):

			return $result;

		else:

			false;

		endif;	

	}

	public function tambahTahunAjaran($param = array())
	{
		
		$this->db->insert('tb_tahun_ajaran', $param);

	}

	public function updateTahunAjaran($param = array(), $id)
	{
		$check = $this->db->get_where('tb_tahun_ajaran', ['id_tahun_ajaran'=> $id])->row();

		if($check):

			if($param['status'] == 1 ):

				$this->db->update('tb_tahun_ajaran',array('status'=>0));

			endif;

			$this->db->where('id_tahun_ajaran', $id)->update('tb_tahun_ajaran',$param);

			return true;

		endif;

		return false;

	}

	// public function updateTahunAjaran($param = array(), $id)
	// {
	// 	return $this->db->where('id_tahun_ajaran', $id)->update('tb_tahun_ajaran',$param);
	// }

	public function deleteTahunAjaran($id)
	{
		if ($id) :

			$check = $this->db->get_where('tb_rombel', ['id_tahun_ajaran_fk'=> $id])->row();

			if($check):

				return false;

			else:

				$this->db->where('id_tahun_ajaran', $id)->update('tb_tahun_ajaran',array("deleted_at" => date("Y-m-d H:i:s")));

				return true;

			endif;

		else : 

			return false;

		endif;

	}

}

/* End of file MdTahunAjaran.php */
/* Location: ./application/models/MdTahunAjaran.php */