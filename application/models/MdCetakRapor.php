<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdCetakRapor extends CI_Model {

	public function getRekapNilai($where = array()){

		$this->db->select('*');

		$this->db->where($where);

		return $this->db->get('vw_rekapnilai')->result(); 
	}

	public function getNilaiMapel($where = array()){

		$this->db->select('*');

		$this->db->where($where);
		$this->db->order_by('id_mapel', 'asc');

		return $this->db->get('vw_export')->result(); 
	}
	
}


/* End of file MdCetakRapor.php */
/* Location: ./application/models/MdCetakRapor.php */