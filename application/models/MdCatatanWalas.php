<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdCatatanWalas extends CI_Model {

	public function getCatatanWalas($where = array()){

		$this->db->select('*');

		$this->db->where($where);

		return $this->db->get('vw_catatan')->result(); 
	}

	public function saveCatatanWalas($param = array()){

		foreach($param['penilaian'] as $key=>$value):

			$data = array(
				'semester' => $param['semester'],
				'id_anggota_rombel_fk' => $param['penilaian'][$key]['id_anggota_rombel'],
				'catatan' => $param['penilaian'][$key]['catatan'],
			);
			$check = $this->db->get_where('tb_catatan', ['id_catatan' => $param['penilaian'][$key]['id_catatan']])->num_rows();

			if($check!==0){
				$update = $this->db->where(['id_catatan' => $param['penilaian'][$key]['id_catatan']])->update('tb_catatan',$data);
			}

			else{
				$insert = $this->db->insert("tb_catatan", $data);
			}

		endforeach;

		if($this->db->affected_rows() >= 0):
			return true;
		endif;

		return false;
	}



}

/* End of file MdCatatanWalas.php */
/* Location: ./application/models/MdCatatanWalas.php */