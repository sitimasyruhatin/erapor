<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdPrestasi extends CI_Model {

	public function getPrestasi($where = array()){

		$this->db->order_by('nama_siswa', 'asc');

		$this->db->select('*');

		$this->db->where($where);

		return $this->db->get('vw_prestasi')->result(); 
	}

	public function tambahPrestasi($param = array()){

		return $this->db->insert("tb_prestasi", $param);

	}


	public function updatePrestasi($param = array(), $id)
	{
		
		if($id){

			return $this->db->where('id_prestasi', $id)->update('tb_prestasi',$param);

		}
		else{
			return false;
		}
	}

	public function deletePrestasi($id){

		if($id){

			return $this->db->where('id_prestasi', $id)->update('tb_prestasi',array("deleted_at" => date("Y-m-d H:i:s")));
		}
		else{

			return false;

		}

	}

	// public function savePrestasi($param = array()){

	// 	foreach($param['penilaian'] as $key=>$value):

	// 		foreach ($value['id_prestasi'] as $key2=>$value2):
	// 			$data = array(
	// 				'semester' => $param['semester'],
	// 				'id_anggota_rombel_fk' => $value['id_anggota_rombel'],
	// 				'jenis_kegiatan' => $value['jenis_prestasi'][$key2],
	// 				'keterangan' => $value['keterangan'][$key2]
	// 			);

	// 			if($value2 == "null" || $value2 == null):
	// 				$insert = $this->db->insert("tb_prestasi", $data);
	// 			else:
	// 				$this->db->where(['id_prestasi' => $value2])->update('tb_prestasi',$data);
	// 			endif;

	// 		endforeach;

			// $data = array(
			// 	'semester' => $param['semester'],
			// 	'id_anggota_rombel_fk' => $param['penilaian'][$key]['id_anggota_rombel'],
			// 	'jenis_kegiatan' => $param['penilaian'][$key]['jenis_kegiatan'],
			// 	'keterangan' => $param['penilaian'][$key]['keterangan']
			// );

			// $check = $this->db->get_where('tb_prestasi', ['id_prestasi' => $param['penilaian'][$key]['id_prestasi']])->num_rows();
			// if($check!==0){
			// 	$update = $this->db->where(['id_prestasi' => $param['penilaian'][$key]['id_prestasi']])->update('tb_prestasi',$data);
			// }
			// else{
			// 	$insert = $this->db->insert("tb_prestasi", $data);
			// }

	// 	endforeach;

	// 	if($this->db->affected_rows() >= 0):
	// 		return true;
	// 	endif;

	// 	return false;
	// }


}

/* End of file MdPrestasi.php */
/* Location: ./application/models/MdPrestasi.php */