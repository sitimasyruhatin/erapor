<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdAuth extends CI_Model {

	// function login
	public function login($param) {

		$this->load->model("MdRolePegawai");

		$this->load->model("MdRombel");

		$this->load->model("MdTahunAjaran");

		$data = $this->db->get_where('vw_role_pegawai', array('nik' => $param['nik'], 'password' => $param['password']))->result();

		if(count($data) > 0){
			$this->session->set_userdata(array(

				"status"=> "logged",

				"id_pegawai"=> $data[0]->id_pegawai_fk,

				"nik"=> $data[0]->nik,

				"nama_pegawai"=> $data[0]->nama_pegawai,

				"nama_role" => $data[0]->nama_role,

				"tahun_ajaran"=> $this->MdTahunAjaran->getActiveTahunAjaran()[0],

				"isTataUsaha"=>$this->MdRolePegawai->checkIsTataUsaha($data[0]->id_pegawai_fk),

				"isGuruBK"=>$this->MdRolePegawai->checkIsGuruBK($data[0]->id_pegawai_fk),

				"isGuruMapel"=>$this->MdRolePegawai->checkIsGuruMapel($data[0]->id_pegawai_fk),

				"isKesiswaan"=>$this->MdRolePegawai->checkIsKesiswaan($data[0]->id_pegawai_fk),

				'isWaliKelas'=>$this->MdRolePegawai->checkIsWaliKelas($data[0]->id_pegawai_fk)
				// 'isWaliKelas'=>$this->MdRombel->checkIsWaliKelas($data[0]->id_pegawai_fk)

			));
			return $data;

			// var_dump($this->MdTahunAjaran->getLastTahunAjaran());
		} else {

			return false;

		}
	}
}

/* End of file Auth.php */
/* Location: ./application/models/MdAuth.php */