<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdPegawai extends CI_Model {

	public function getPegawai($id = null){

		$this->db->order_by('created_at desc');
		
		if($id):

			$result = $this->db->get_where("tb_pegawai", array('id_pegawai'=> $id, 'deleted_at'=>null))->result();

		else:
			
			$result = $this->db->get_where("tb_pegawai", array('deleted_at'=>null))->result();

		endif;

		if (count($result)>0):

			return $result;

		else:

			false;

		endif;	

	}


	public function getCariPegawai($where = array(), $like = array()){

		if(!empty($where)):

			$this->db->where($where);

		endif;

		if(!empty($like)):

			$this->db->like($like);

		endif;

		$result = $this->db->get('tb_pegawai')->result(); 

		if (count($result)>0):

			return $result;

		else:

			false;

		endif;	
	}

	public function tambahPegawai($param = array())	{

		$getData = $this->db->get_where("tb_pegawai", array(

			"nik"=> $param["nik"],

			"deleted_at"=> null))->result();

		if(count($getData) == 0):

			$this->db->insert("tb_pegawai", $param);

			if($this->db->affected_rows() >= 0):

				return true;

			endif;

		else:

			false;

		endif;

	}

	public function updatePegawai($param = array(), $id) {

		$getData = $this->db->get_where("tb_pegawai", array(

			"id_pegawai !="=> $id,

			"nik"=> $param["nik"],

			"deleted_at"=> null))->result();

		if(count($getData) == 0):

			$this->db->where('id_pegawai', $id)->update('tb_pegawai',$param);

			if($this->db->affected_rows() >= 0):

				return true;

			endif;

		else:

			false;

		endif;
	}

	public function deletePegawai($id) {

		if ($id) :

			$check = $this->db->get_where('tb_role_pegawai', ['tb_role_pegawai.id_pegawai_fk'=> $id])->row();
			
			if($check):

				return false;

			else:

				$this->db->where('id_pegawai', $id)->update('tb_pegawai',array("deleted_at" => date("Y-m-d H:i:s")));

				return true;

			endif;

		else : 

			return false;

		endif;
	}

}
/* End of file MdPegawai.php */
/* Location: ./application/models/MdPegawai.php */