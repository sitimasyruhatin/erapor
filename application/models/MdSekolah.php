<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdSekolah extends CI_Model {

	public function getSekolah($id=null){
		
		if($id):

			$result = $this->db->get_where("tb_sekolah", array("id_sekolah"=> $id, "deleted_at"=> null))->result(); 

		else:
			
			$result = $this->db->get_where("tb_sekolah", array("deleted_at"=> null))->result(); 


		endif;

		return $result;

	}

	public function updateSekolah($param = array(), $id)
	{
		return $this->db->where('id_sekolah', $id)->update('tb_sekolah',$param);
	}

}

/* End of file MdSekolah.php */
/* Location: ./application/models/MdSekolah.php */