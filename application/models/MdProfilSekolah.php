<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdProfilSekolah extends CI_Model {

	public function getSiswa($id = null){


		$this->db->order_by('status desc');
		$this->db->order_by('created_at desc');
		
		if($id):

			$result = $this->db->get_where("tb_siswa", array("id"=> $id))->result(); 

		else:
			
			$result = $this->db->get("tb_siswa")->result(); 


		endif;

		return $result;

	}

	public function getCariSiswa($where = array(), $like = array()){

		if(!empty($where)):

			$this->db->where($where);

		endif;

		if(!empty($like)){

			$this->db->like($like);

		}

		return $this->db->get('tb_siswa')->result(); 
	}


	public function getDetailSiswa($id) {
		return $this->db->get_where('tb_siswa', ["nis" => $id])->row();
	}

	public function updateSiswa($param = array(), $id)
	{
		return $this->db->where('nis', $id)->update('tb_siswa',$param);
	}

}

/* End of file MdProfilSekolah.php */
/* Location: ./application/models/MdProfilSekolah.php */