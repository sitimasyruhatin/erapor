<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdRombel extends CI_Model {

	public function getRombel($id = null){

		$this->db->order_by('created_at desc');

		$get_thn = $this->session->userdata('tahun_ajaran')->id_tahun_ajaran;

		if($id):

			$result = $this->db->get_where("vw_rombel", array("id_rombel"=> $id, "deleted_at"=> null, "id_tahun_ajaran_fk"=> $get_thn))->result();

		else:

			$result = $this->db->get_where("vw_rombel", array("deleted_at"=> null, "id_tahun_ajaran_fk"=> $get_thn))->result(); 

		endif;

		return $result;

	}

	public function getRombelWalas($where = array()){
		return $this->db->get_where('vw_rombel', $where)->row();
	}

	public function getCariRombel($where = array(), $like = array()){


		
		$this->db->order_by('tahun_ajaran', 'asc');
		$this->db->order_by('nama_rombel asc');

		if(!empty($where)):

			$this->db->where($where);

		endif;

		if(!empty($like)){

			$this->db->like($like);

		}

		return $this->db->get('vw_rombel')->result(); 
	}

	public function getCariRombelNilai($where = array(), $like = array()){


		$this->db->order_by('nama_rombel asc');

		if(!empty($where)):

			$this->db->where($where);

			$this->db->group_by('id_rombel');

		endif;

		if(!empty($like)){

			$this->db->like($like);

		}

		return $this->db->get('vw_pembelajaran')->result(); 
	}

	public function tambahRombel($param = array())
	{
		$getData = $this->db->get_where("tb_rombel", array(

			"id_pegawai_fk"=> $param["id_pegawai_fk"],

			"id_tahun_ajaran_fk"=> $param["id_tahun_ajaran_fk"],

			"deleted_at"=> null))->result();

		if(count($getData) == 0):

			$this->db->insert('tb_rombel',$param);

			if($this->db->affected_rows() >= 0):
				return true;

			endif;

		else:

			false;

		endif;
	}

	public function updateRombel($param = array(), $id)
	{
		$getData = $this->db->get_where("tb_rombel", array(

			"id_rombel !="=> $id,

			"id_pegawai_fk"=> $param["id_pegawai_fk"],

			"id_tahun_ajaran_fk"=> $param["id_tahun_ajaran_fk"],

			"deleted_at"=> null))->result();

		if(count($getData) == 0):

			$this->db->where('id_rombel', $id)->update('tb_rombel',$param);

			if($this->db->affected_rows() >= 0):

				return true;

			endif;

			return false;
		else:

			return false;

		endif;
	}

	public function deleteRombel($id)
	{
		if ($id) :

			$check1 = $this->db->get_where('tb_anggota_rombel', ['id_rombel_fk'=> $id])->row();
			$check2 = $this->db->get_where('tb_pembelajaran', ['id_rombel_fk'=> $id])->row();

			if($check1 || $check2):

				return false;

			else:

				$this->db->where('id_rombel', $id)->update('tb_rombel',array("deleted_at" => date("Y-m-d H:i:s")));

				return true;

			endif;

		else : 

			return false;

		endif;
	}

	// public function checkIsWaliKelas($id){

	// 	$where = array(
	// 		'id_pegawai_fk'=> $id
	// 	);

	// 	$data = $this->db->get_where('tb_rombel', $where)->result(); 

	// 	if(count($data)>0):

	// 		return TRUE;

	// 	endif;

	// 	return FALSE;

	// }



}

/* End of file MdRombel.php */
/* Location: ./application/models/MdRombel.php */