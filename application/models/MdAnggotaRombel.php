<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdAnggotaRombel extends CI_Model {

	public function getAnggotaRombel($id = null){

		$this->db->order_by('nis asc');

		if($id):

			$result = $this->db->get_where('vw_anggota_rombel', array('id_rombel_fk'=> $id, 'deleted_at'=>null))->result(); 

		else:

			$result = $this->db->get_where('vw_anggota_rombel', array('deleted_at'=>null))->result(); 

		endif;

		return $result;

	}


	public function getCariAnggotaRombel($where = array(), $like = array()){

		if(!empty($where)):

			$this->db->where($where);

		endif;

		if(!empty($like)){

			$this->db->like($like);

		}

		return $this->db->get('vw_anggota_rombel')->result(); 
	}

	public function tambahAnggotaRombel($param)
	{
		$this->db->where_in("id_siswa_fk", $param["id_siswa_fk"]);

		$this->db->where('id_rombel_fk', $param["id_rombel_fk"]);

		$this->db->where('deleted_at', null);

		$getData = $this->db->get('tb_anggota_rombel')->result();

		if(count($getData) == 0):

			foreach ($param["id_siswa_fk"] as $value) {

				$data= array(

					"id_siswa_fk" => $value,

					"id_rombel_fk" => $param["id_rombel_fk"]

				);

				$check = $this->db->get_where('tb_anggota_rombel', $data)->num_rows();

				if($check === 0) {

					$this->db->insert('tb_anggota_rombel', $data);
				}   

				else{

					return false;

				} 

			}

			if($this->db->affected_rows() > 0):

				return true;

			endif;

		else:

			false;

		endif;
	}

	public function deleteAnggotaRombel($id)
	{
		if ($id) :

			// $check1 = $this->db->get_where('tb_nilai_sikap',  array("id_anggota_rombel_fk"=> $id))->row();
			// $check2 = $this->db->get_where('tb_nilai_keterampilan',  array("id_anggota_rombel_fk"=> $id))->row();
			// $check3 = $this->db->get_where('tb_nilai_pengetahuan',  array("id_anggota_rombel_fk"=> $id))->row();
			// $check4 = $this->db->get_where('tb_nilai_ekskul',  array("id_anggota_rombel_fk"=> $id))->row();
			// $check5 = $this->db->get_where('tb_prestasi',  array("id_anggota_rombel_fk"=> $id))->row();
			// $check6 = $this->db->get_where('tb_absensi',  array("id_anggota_rombel_fk"=> $id))->row();
			// $check7 = $this->db->get_where('tb_catatan',  array("id_anggota_rombel_fk"=> $id))->row();
			

			// if($check1 || $check2 || $check3 || $check4 || $check5 || $check6 || $check7 ):

			// 	return false;

			// else:

				$this->db->where('id_anggota_rombel', $id)->update('tb_anggota_rombel',array("deleted_at" => date("Y-m-d H:i:s")));

			// 	return true;

			// endif;

		else : 

			return false;

		endif;
		
	}

	public function copyRombel($old, $new){

		$this->db->select('id_siswa_fk');

		$this->db->where('id_rombel_fk', $old);

		$this->db->where('deleted_at', null);

		$siswa = $this->db->get('tb_anggota_rombel')->result();

		if(count($siswa)>0){

			foreach ($siswa as $value) {

				$data = array(

					'id_rombel_fk' => $new,

					'id_siswa_fk' => $value->id_siswa_fk
				);

				$check = $this->db->get_where('tb_anggota_rombel', $data)->num_rows();

				if($check === 0) {
					$this->db->insert('tb_anggota_rombel', $data);
				}   
				else{
					return false;
				} 
			}

			return true;
		}
		return false;

	}



}

/* End of file MdAnggotaRombel.php */
/* Location: ./application/models/MdAnggotaRombel.php */