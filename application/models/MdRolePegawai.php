<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdRolePegawai extends CI_Model {


    public function getRolePegawai($id = null){

        $this->db->order_by('nik desc');

        $this->db->order_by('created_at desc');


        if($id):

           $result = $this->db->get_where("vw_role_pegawai", array("id_role_pegawai"=> $id, "deleted_at"=> null))->result(); 

       else:

        $result = $this->db->get_where("vw_role_pegawai", array("deleted_at"=> null))->result(); 

    endif;

    return $result;

}

public function getCariRolePegawai($where = array(), $like = array()){

    if(!empty($where)):

        $this->db->where($where);

    endif;

    if(!empty($like)):

        $this->db->like($like);

    endif;

    $result = $this->db->get('vw_role_pegawai')->result(); 

    if (count($result)>0):

        return $result;

    else:

        false;

    endif;  
}

public function getCariRole($where = array(), $like = array()){

    if(!empty($where)):

        $this->db->where($where);

    endif;

    if(!empty($like)){

        $this->db->like($like);

    }

    $result = $this->db->get('tb_role')->result(); 
    
    if (count($result)>0):
        return $result;
    else:
        false;
    endif;  
}

public function tambahRolePegawai($param = array()) {

    $getData = $this->db->get_where("tb_role_pegawai", array(

        "id_pegawai_fk"=> $param["id_pegawai_fk"],

        "id_role_fk"=> $param["id_role_fk"],
        
        "deleted_at"=> null))->result();

    if(count($getData) == 0):

        $this->db->insert("tb_role_pegawai", $param);

        if($this->db->affected_rows() >= 0):
            return true;

        endif;

    else:

        false;

    endif;

}

public function updateRolePegawai($param = array(), $id){

 $getData = $this->db->get_where("tb_role_pegawai", array(

    "id_role_pegawai !="=> $id,

    "id_pegawai_fk"=> $param["id_pegawai_fk"],

    "id_role_fk"=> $param["id_role_fk"],

    "deleted_at"=> null))->result();

 if(count($getData) == 0):

     $this->db->where('id_role_pegawai', $id)->update('tb_role_pegawai',$param);

     if($this->db->affected_rows() >= 0):

        return true;

    endif;

else:

    false;

endif;
}

public function deleteRolePegawai($id)
{
    if ($id) :

        $this->db->where('id_role_pegawai', $id)->update('tb_role_pegawai',array("deleted_at" => date("Y-m-d H:i:s")));
        return true;

    else : 

        return false;

    endif;
}

public function checkIsTataUsaha($id){

    $where = array(
        "id_pegawai_fk"=> $id,
        "id_role_fk"=> $this->config->item("role_tata_usaha"),

        "deleted_at"=> null
    );

    $data = $this->db->get_where("tb_role_pegawai", $where)->result(); 

    if(count($data)>0):
        return TRUE;
    endif;
    return FALSE;

}
public function checkIsGuruBK($id){

    $where = array(
        "id_pegawai_fk"=> $id,
        "id_role_fk"=> $this->config->item("role_guru_bk"),

        "deleted_at"=> null
    );

    $data = $this->db->get_where("tb_role_pegawai", $where)->result(); 

    if(count($data)>0):
        return TRUE;
    endif;
    return FALSE;

}

public function checkIsGuruMapel($id){

    $where = array(
        "id_pegawai_fk"=> $id,
        "id_role_fk"=> $this->config->item("role_guru_mapel"),
        "deleted_at"=> null
    );

    $data = $this->db->get_where("tb_role_pegawai", $where)->result(); 

    if(count($data)>0):
        return TRUE;
    endif;
    return FALSE;

}

public function checkIsKesiswaan($id){

    $where = array(
        "id_pegawai_fk"=> $id,
        "id_role_fk"=> $this->config->item("role_kesiswaan"),
        "deleted_at"=> null
    );

    $data = $this->db->get_where("tb_role_pegawai", $where)->result(); 

    if(count($data)>0):
        return TRUE;
    endif;
    return FALSE;

}

public function checkIsWaliKelas($id){

    $where = array(
        'id_pegawai_fk'=> $id,
        "deleted_at"=> null
    );

    $data = $this->db->get_where('tb_rombel', $where)->result(); 

    if(count($data)>0):
        return TRUE;
    endif;
    return FALSE;
// return true;
}
}

/* End of file MdRolePegawai.php */
/* Location: ./application/models/MdRolePegawai.php */