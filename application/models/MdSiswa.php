<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdSiswa extends CI_Model {

	public function getSiswa($id = null){

		$this->db->order_by('nis asc');
		
		if($id):

			$result = $this->db->get_where("tb_siswa", array("id_siswa"=> $id, "deleted_at"=> null))->result(); 

		else:
			
			$result = $this->db->get_where("tb_siswa", array("deleted_at"=> null))->result(); 

		endif;

		if (count($result)>0):

			return $result;

		else:

			return [];
			
		endif;	

	}

	public function getCariSiswa($where = array(), $like = array()){

		if(!empty($where)):

			$this->db->where($where);

		endif;

		if(!empty($like)){

			$this->db->like($like);

		}

		$result= $this->db->get('tb_siswa')->result();
		
		if (count($result)>0):
			return $result;
		else:
			false;
		endif;	
	}


	public function tambahSiswa($param)	{

		$getData1 = $this->db->get_where("tb_siswa", array(

			"nis"=> $param["nis"],

			"deleted_at"=> null))->result();

		$getData2 = $this->db->get_where("tb_siswa", array(

			"nisn"=> $param["nisn"],

			"deleted_at"=> null))->result();

		if(count($getData1) == 0 && count($getData2) == 0){

			$this->db->insert("tb_siswa", $param);

			if($this->db->affected_rows() >= 0){

				return true;
			}

			else{

				false;

			}
		}
	}

	public function updateSiswa($param = array(), $id){

		$getData1 = $this->db->get_where("tb_siswa", array(

			"id_siswa !="=> $id,

			"nis"=> $param["nis"],

			"deleted_at"=> null))->result();

		$getData2 = $this->db->get_where("tb_siswa", array(

			"id_siswa !="=> $id,

			"nisn"=> $param["nisn"],

			"deleted_at"=> null))->result();

		if(count($getData1) == 0 && count($getData2) == 0){

			$this->db->where('id_siswa', $id)->update('tb_siswa',$param);

			if($this->db->affected_rows() >= 0){

				return true;

			}

			else{

				return false;

			}
		}
	}

	public function deleteSiswa($id){

		if ($id) :

			$check = $this->db->get_where('tb_anggota_rombel', ['id_siswa_fk'=> $id, 'deleted_at'=> null])->row();

			if($check):

				return false;

			else:

				$this->db->where('id_siswa', $id)->update('tb_siswa',array("deleted_at" => date("Y-m-d H:i:s")));

				return true;

			endif;

		else : 

			return false;

		endif;
	}

}

/* End of file MdSiswa.php */
/* Location: ./application/models/MdSiswa.php */