<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdNilaiSikap extends CI_Model {


	public function getNilaiSikap($where = array()){

		$this->db->select('*');

		$this->db->where($where);

		return $this->db->get('vw_nilai_sikap')->result(); 
	}

	// public function getRombel($where = array(), $like = array()){


	// 	$this->db->order_by('nama_rombel asc');

	// 	if(!empty($where)):

	// 		$this->db->where($where);

	// 		$this->db->group_by('id_rombel');

	// 	endif;

	// 	if(!empty($like)){

	// 		$this->db->like($like);

	// 	}

	// 	return $this->db->get('vw_pembelajaran')->result(); 
	// }

	public function saveNilaiSikap($param = array()){

		foreach($param['penilaian'] as $key=>$value):

			if ([$param['penilaian'][$key]['desc_spiritual']] == 'null') {
				$desc_spiritual = NULL;
			}
			if ([$param['penilaian'][$key]['desc_spiritual']] == 'null') {
				return '-';
			}

			$data = array(
				'semester' => $param['semester'],
				'id_anggota_rombel_fk' => $param['penilaian'][$key]['id_anggota_rombel'],
				'id_pembelajaran_fk' => $param['penilaian'][$key]['id_pembelajaran'],
				'nilai_spiritual' => $param['penilaian'][$key]['nilai_spiritual'],
				'deskripsi_spiritual' => $param['penilaian'][$key]['desc_spiritual'],
				'nilai_sosial' => $param['penilaian'][$key]['nilai_sosial'],
				'deskripsi_sosial' => $param['penilaian'][$key]['desc_sosial'],
			);

			$check = $this->db->get_where('tb_nilai_sikap', ['id_nilai_sikap' => $param['penilaian'][$key]['id_nilai_sikap']])->num_rows();


// var_dump($param['penilaian'][$key]['id_nilai_sikap']);
				if($check!==0){
					// var_dump("tes");
					// $check;
					$update = $this->db->where(['id_nilai_sikap' => $param['penilaian'][$key]['id_nilai_sikap']])->update('tb_nilai_sikap',$data);
					// var_dump($update);
					// $update = $this->db->update('tb_nilai_sikap', $data)->where(['id_nilai_sikap' => $param['penilaian'][$key]['id_nilai_sikap']]);
				}

				else{
					// var_dump("insert");
					$insert = $this->db->insert("tb_nilai_sikap", $data);
					// var_dump($insert);
				}

			endforeach;

			if($this->db->affected_rows() >= 0):
				return true;
			endif;

			return false;
		}


	}

	/* End of file MdNilaiSikap.php */
/* Location: ./application/models/MdNilaiSikap.php */