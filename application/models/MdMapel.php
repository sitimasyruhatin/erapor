<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdMapel extends CI_Model {

	public function getMapel($id = null){

		$this->db->order_by('id_mapel asc');
		
		if($id):

			$result = $this->db->get_where("tb_mapel", array("id_mapel"=> $id, "deleted_at"=> null))->result(); 

		else:
			
			$result = $this->db->get_where("tb_mapel", array("deleted_at"=> null))->result(); 


		endif;

		if (count($result)>0):

			return $result;

		else:

			false;
			
		endif;	

	}

	public function getCariMapel($where = array(), $like = array()){


		$this->db->order_by('nama_rombel asc');

		if(!empty($where)):

			$this->db->where($where);

		endif;

		if(!empty($like)){

			$this->db->like($like);

		}

		return $this->db->get('vw_pembelajaran')->result(); 
	}

	public function tambahMapel($param)
	{
		return $this->db->insert("tb_mapel", $param);
	}

	public function updateMapel($param = array(), $id)
	{
		return $this->db->where('id_mapel', $id)->update('tb_mapel',$param);
	}

	public function deleteMapel($id)
	{
		if ($id) :

			// $check = $this->db->get_where('tb_pembelajaran', ['id_mapel_fk'=> $id,])->row();

			// if($check):

			// 	return false;

			// else:

				$this->db->where('id_mapel', $id)->update('tb_mapel',array("deleted_at" => date("Y-m-d H:i:s")));

			// 	return true;

			// endif;

		else : 

			return false;

		endif;
	}
}

/* End of file MdMapel.php */
/* Location: ./application/models/MdMapel.php */