<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdPembelajaran extends CI_Model {

	public function getPembelajaran($id = null){

		$this->db->select('*');

		$this->db->from('vw_pembelajaran');

		if($id):

			$this->db->where('id_rombel', $id);

			$result = $this->db->get()->result(); 

		else:

			$result = $this->db->get()->result(); 

		endif;

		return $result;
	}

	// public function getCariRolePegawai($where = array(), $like = array()){

	// 	if(!empty($where)):

	// 		$this->db->where($where);

	// 	endif;

	// 	if(!empty($like)):

	// 		$this->db->like($like);

	// 	endif;

	// 	$result = $this->db->get('tb_pegawai')->result(); 

	// 	if (count($result)>0):

	// 		return $result;

	// 	else:

	// 		false;

	// 	endif;	
	// }

	public function savePembelajaran($id = null, $data = null){
		
        if ($id && $data) {

			$new_data = json_decode($data);
			foreach ($new_data as $value) {
				$this->db->select('*');            				
				$check = $this->db->get_where('tb_pembelajaran', array('id_rombel_fk' => $id, 'id_mapel_fk' => $value->id_mapel_fk))->result();
				if ($check) {
					$this->db->where(array('id_rombel_fk' => $id, 'id_mapel_fk' => $value->id_mapel_fk));
					$this->db->update('tb_pembelajaran', $value);	

				}else{
					$this->db->insert('tb_pembelajaran', $value);
				}
			}
            // $this->db->select('*');            
            // $this->db->where('id_rombel_fk', $id);
            // $check = $this->db->get('tb_pembelajaran')->result();
            // if ($check) {
			// 	$this->db->delete('tb_pembelajaran', array('id_rombel_fk' => $id));
			// }

			// $this->db->insert_batch('tb_pembelajaran', json_decode($data));
			
			return true;
        }else{
			return false;
		}		
	}

}

/* End of file MdPembelajaran.php */
/* Location: ./application/models/MdPembelajaran.php */