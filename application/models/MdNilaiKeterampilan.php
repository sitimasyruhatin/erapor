<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdNilaiKeterampilan extends CI_Model {

	public function getNilaiK($where = array()){

		$this->db->select('*');

		$this->db->where($where);

		return $this->db->get('vw_nilai_keterampilan')->result(); 
	}

	public function getDeskripsiK($where = array()){

		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('predikat', 'asc');
		return $this->db->get('vw_format_nilai')->result(); 
	}
    
    public function saveDeskripsiK($param = array()){

		foreach($param['deskripsi'] as $key=>$value):

			$data = array(
				'id_pembelajaran_fk' => $param['deskripsi'][$key]['id_pembelajaran'],
				'semester' => $param['deskripsi'][$key]['semester'],
				'predikat' => $param['deskripsi'][$key]['predikat'],
				'deskripsi_nilai' => $param['deskripsi'][$key]['deskripsi_nilai'],
				'kategori_nilai' => $param['deskripsi'][$key]['kategori_nilai'],
			);

			$check = $this->db->get_where('tb_format_nilai', ['id_format' => $param['deskripsi'][$key]['id_format']])->num_rows();

			if($check!==0){
				$update = $this->db->where(['id_format' => $param['deskripsi'][$key]['id_format']])->update('tb_format_nilai',$data);
			}

			else{
				$insert = $this->db->insert("tb_format_nilai", $data);
			}

		endforeach;

		if($this->db->affected_rows() >= 0):
			return true;
		endif;

		return false;
	}

	public function saveNilaiK($param = array()){
		$penilaian = $param['penilaian'];
		foreach($penilaian as $key=>$value):

			$data = array(
				'semester' => $param['semester'],
				'id_anggota_rombel_fk' => $penilaian[$key]['id_anggota_rombel'],
				'id_pembelajaran_fk' => $penilaian[$key]['id_pembelajaran'],
				'kd1_praktik' => $penilaian[$key]['kd1_praktik'],
				'kd1_produk' => $penilaian[$key]['kd1_produk'],
                'kd1_porto' => $penilaian[$key]['kd1_porto'],
                'kd2_praktik' => $penilaian[$key]['kd2_praktik'],
				'kd2_produk' => $penilaian[$key]['kd2_produk'],
                'kd2_porto' => $penilaian[$key]['kd2_porto'],				
                'kd3_praktik' => $penilaian[$key]['kd3_praktik'],
				'kd3_produk' => $penilaian[$key]['kd3_produk'],
                'kd3_porto' => $penilaian[$key]['kd3_porto'],
                'kd4_praktik' => $penilaian[$key]['kd4_praktik'],
				'kd4_produk' => $penilaian[$key]['kd4_produk'],
                'kd4_porto' => $penilaian[$key]['kd4_porto'],
                'kd5_praktik' => $penilaian[$key]['kd5_praktik'],
				'kd5_produk' => $penilaian[$key]['kd5_produk'],
                'kd5_porto' => $penilaian[$key]['kd5_porto'],
                'kd6_praktik' => $penilaian[$key]['kd6_praktik'],
				'kd6_produk' => $penilaian[$key]['kd6_produk'],
				'kd6_porto' => $penilaian[$key]['kd6_porto']
			);

			$check = $this->db->get_where('tb_nilai_keterampilan', ['id_nilai_ket' => $penilaian[$key]['id_nilai_ket']])->num_rows();

			if($check!==0){
				$update = $this->db->where(['id_nilai_ket' => $penilaian[$key]['id_nilai_ket']])->update('tb_nilai_keterampilan',$data);
			}

			else{
				$insert = $this->db->insert("tb_nilai_keterampilan", $data);
			}

		endforeach;

		if($this->db->affected_rows() >= 0):
			return true;
		endif;

		return false;
	}

}

/* End of file MdNilaiKeterampilan.php */
/* Location: ./application/models/MdNilaiKeterampilan.php */