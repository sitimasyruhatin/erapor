<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdEkskul extends CI_Model {

	public function getEkskul($id = null){

		$this->db->order_by('created_at desc');
		
		if($id):

			$result = $this->db->get_where("tb_ekskul", array("id_ekskul"=> $id, "deleted_at"=> null))->result(); 

		else:
			
			$result = $this->db->get_where("tb_ekskul", array("deleted_at"=> null))->result(); 


		endif;

		return $result;

	}

	
	public function getCariEkskul($where = array(), $like = array()){

		if(!empty($where)):

			$this->db->where($where);

		endif;

		if(!empty($like)){

			$this->db->like($like);

		}

		return $this->db->get('tb_ekskul')->result(); 
	}

	public function tambahEkskul($param)
	{
		return $this->db->insert("tb_ekskul", $param);
	}

	public function updateEkskul($param = array(), $id)
	{
		return $this->db->where('id_ekskul', $id)->update('tb_ekskul',$param);
	}

	public function deleteEkskul($id){

		if ($id) :

			$check1 = $this->db->get_where('tb_nilai_ekskul',  array("id_ekskul_wajib_fk"=> $id))->row();
			$check2 = $this->db->get_where('tb_nilai_ekskul',  array("id_ekskul_pilihan_fk"=> $id))->row();

			// echo json_encode($check1);
			// echo json_encode($check2);

			if($check1 || $check2):

				return false;

			else:

				$this->db->where('id_ekskul', $id)->update('tb_ekskul',array("deleted_at" => date("Y-m-d H:i:s")));
				return true;

			// return $this->db->last_query();
			endif;

		else : 

			return false;

		endif;
	}
}

/* End of file MdEkskul.php */
/* Location: ./application/models/MdEkskul.php */