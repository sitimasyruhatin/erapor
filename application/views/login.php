<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-Rapor | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Select2 -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/select2/css/select2.min.css"?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/plugins/fontawesome-free/css/all.min.css"?>">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css"?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/dist/css/adminlte.min.css"?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="#"><b>E-Rapor</b> SMA BSS</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Login to your account</p>

        <form id="form-login">
          <div class="input-group mb-3">
            <input type="text"name="nik" id="nik" class="form-control" placeholder="Masukkan NIK" required="">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" name="password" id="password" class="form-control" data-toggle="password" placeholder="Masukkan Password" required="">
            <div class="input-group-append">
              <div class="input-group-text"><i class="fa fa-eye"></i>
              </div>
            </div>
          </div>
<!--           <div class="input-group mb-3">
            <select name="tahun-ajaran" id="tahun-ajaran" class="form-control select2 select2-tahun-ajaran" style="width: 100%;" required="">
              <option value="" selected="selected">Pilih Tahun Ajaran</option>
            </select>
          </div> -->
          <div class="row">
            <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block ">Login</button>
            </div>
          </div>
        </form>
        <!-- /.col -->
      </div>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js"?>"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url()."assets/plugins/sweetalert2/sweetalert2.min.js"?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.bundle.min.js"?>"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url()."assets/dist/js/adminlte.min.js"?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url()."assets/plugins/select2/js/select2.full.min.js"?>"></script>

<script src="<?php echo base_url()."assets/plugins/bootstrap-show-password-master/bootstrap-show-password.min.js"?>"></script>

</body>
</html>

<script>
  $(document).ready(function () {
    // $('#form-login').submit(function (e) {
    //   e.preventDefault();
    // }).validate({
    //   submitHandler: function (form) {
    //     var form = $('#form-login');
    //     var data = form.serialize();
    //     axios.post('/Auth/login', data)
    //     .then(function (response) {
    //       var res = response.data;
    //       if (!res.error) {
    //         toastr.success(res.messages);
    //         setTimeout(function () {
    //           window.location = "/";
    //         }, 1500);
    //       } else {
    //         toastr.error(res.messages);
    //       }
    //     })
    //     .catch(function (error) {
    //       console.log(error);
    //     });
    //   }
    // });

    $('.select2').select2({
      theme: 'bootstrap4'
    });

    const Toast = Swal.mixin({
      toast: false,
      position: 'center',
      showConfirmButton: false,
      timer: 2000
    });

    // $(".select2-tahun-ajaran").select2({
    //   ajax: {
    //     url: "/Auth/getCariTahunAjaran/",
    //     dataType: 'json',
    //     type: "GET",
    //     quietMillis: 50,
    //     processResults: function (res) {
    //       return {
    //         results: $.map(res.data, function (item) {
    //           return {
    //             text: item.tahun_ajaran,
    //             id: item.id_tahun_ajaran
    //           }
    //         })
    //       };
    //     }
    //   }
    // });

    

    $('#form-login').submit(function (e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: `/Auth/login`,
        data: $('#form-login').serialize(),
        dataType: 'json'
      })
      .done(function (res) {
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          setTimeout(function () {
            window.location = "/";
          }, 1500);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
          setTimeout(() => {
            location.reload();
          }, 1000);
        }
      });
    });
  });

</script>