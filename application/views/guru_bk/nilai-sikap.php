 <?php $this->load->view("header", ["page" => $page]);?>
 <div class="content-wrapper">
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Nilai Sikap</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Nilai Sikap</li>
          </ol>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="container-fluid">
      <form id="form-simpan-nilai-sikap">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="form-group col-sm-3">
                    <label>Rombel</label>
                    <select name="data[rombel]" class="form-control select2 select2-rombel" id="select2-rombel">
                      <option value="" selected>Pilih rombel</option>
                    </select>
                  </div>
                  <div class="form-group col-sm-3">
                    <label>Semester</label>
                    <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt">
                      <option selected="selected" disabled>Pilih semester</option>
                      <option value = "1">1 (Gasal)</option>
                      <option value = "2">2 (Genap)</option>
                    </select>
                  </div>
                  <div class="form-group col-sm-6">
                    <button type="submit" class="btn btn-flat btn-success float-right"><i class="fa fa-save "></i> Simpan</button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-12" id="show-nilai-sikap" hidden>
            <div class="card">
              <div class="card-body">
                <div class="bg-warning callout callout-warning">
                 <span><b>Nilai Sikap Spiritual dan Sosial diisi dengan huruf (A,B,C atau D)</b></span>
               </div>
               <div class="table-responsive">
                 <table class="table table-bordered table-striped col-12 display responsive nowrap" data-plugin="nilaisikap" style="width:100%; margin-bottom: 1rem"> 
                  <thead>
                    <tr>
                      <th rowspan="2" style="vertical-align: middle;">No</th>
                      <th rowspan="2" style="vertical-align: middle; width: 20%">Nama Siswa</th>
                      <th colspan="2" style="text-align: center;">Sikap Spiritual</th>
                      <th colspan="2" style="text-align: center;">Sikap Sosial</th>
                    </tr>
                    <tr>
                      <th style="width: 7%">Nilai</th>
                      <th>Deskripsi</th>
                      <th style="width: 7%">Nilai </th>
                      <th>Deskripsi</th>
                    </tr>
                  </thead>
                  <tbody  id="table-nilai-sikap">
                  </tbody>
                </table>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-flat btn-success float-right"><i class="fa fa-save "></i> Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>

<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
  <i class="fas fa-chevron-up"></i>
</a>
</div>


<?php
$this->load->view("footer");
?>

<script>

  $(document).ready(function () {

    var datatableNilaiSikap;

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });


    $(".select2-rombel").select2({
      ajax: {
        url: "/Rombel/getCariRombelNilai/"+1,
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_rombel,
                id: item.id_rombel
              }
            })
          };
        }
      }
    });

    $('.select2-rombel').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var id_rombel = data.id;

      var smt = $('#select2-smt').val();
      if(smt) set_nilai(id_rombel, smt);      
    });

    $('.select2-smt').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var smt = data.id;

      var id_rombel = $('#select2-rombel').val();
      if(id_rombel) set_nilai(id_rombel, smt);      
    });

    $('#form-simpan-nilai-sikap').submit(function (e) {
      e.preventDefault();

      let form = $('#form-simpan-nilai-sikap').serializeObject();
      let penilaian = [];

      form.data.id_anggota_rombel.map((item, index) => {
        penilaian.push({
          id_nilai_sikap: form.data.id_nilai_sikap[index],
          id_anggota_rombel: form.data.id_anggota_rombel[index],
          id_pembelajaran: form.data.id_pembelajaran[index],
          nilai_spiritual: form.data.nilai_spiritual[index],
          desc_spiritual: form.data.desc_spiritual[index],
          nilai_sosial: form.data.nilai_sosial[index],
          desc_sosial: form.data.desc_sosial[index],
        });
      });
      let data = {
        rombel: form.data.rombel,
        semester: form.data.semester,
        penilaian
      }

      $.ajax({
        type: 'POST',
        url: `${base_url}/NilaiSikap/saveNilaiSikap/`,
        data: data,
        dataType: 'json'
      })
      .done(function (res) {
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      });
    });

  });
  function set_nilai(rombel, smt) {
    $.ajax({
      type: 'GET',
      url: "/NilaiSikap/getNilaiSikap/"+rombel+"/"+smt,
      dataType: 'json'
    })
    .done(function (res) {
      $('#table-nilai-sikap').html("");

      if (res.success) {

        let number = 1;

        res.data.forEach(element => {
          let nilai_spiritual = '';
          let nilai_sosial = '';
          let desc_spiritual = '';
          let desc_sosial = '';

          if (element.nilai_spiritual != null) {
            nilai_spiritual = element.nilai_spiritual;
          }
          if (element.nilai_sosial != null) {
            nilai_sosial = element.nilai_sosial;
          }
          if(element.deskripsi_spiritual != null){
            desc_spiritual = element.deskripsi_spiritual;
          }
          if(element.deskripsi_sosial != null){
            desc_sosial = element.deskripsi_sosial;
          }

          $('#table-nilai-sikap').append('<tr><td width="30"><input class="form-control" hidden type="text" name="data[id_nilai_sikap][]" value="'+element.id_nilai_sikap+'"><input class="form-control" hidden type="text" name="data[id_anggota_rombel][]" value="'+element.id_anggota_rombel+'"><input class="form-control" hidden type="text" name="data[id_pembelajaran][]" value="'+element.id_pembelajaran+'">' + number++ + '</td><td>' + element.nama_siswa + '</td><td><input class="form-control" style="min-width: 50px;" type="text" name="data[nilai_spiritual][]" value="'+ nilai_spiritual + '"></td><td><textarea class="form-control"  style="min-width: 200px;"  name="data[desc_spiritual][]" rows="2" value="'+ desc_spiritual +'" >'+ desc_spiritual +'</textarea></td><td><input class="form-control"  style="min-width: 50px;"  type="text" name="data[nilai_sosial][]" value="'+ nilai_sosial +'" ></td><td><textarea class="form-control"  style="min-width: 200px;"  name="data[desc_sosial][]" rows="2"  value="'+ desc_sosial +'">'+desc_sosial+'</textarea></td></tr>');
        });

        $('#show-nilai-sikap').attr('hidden',false);
      } else {
        Toast.fire({
          type: 'error',
          title: res.message,
        });
        $('#table-nilai-sikap').append(
          '<tr><td colspan="6" align="middle">No data available in table</td></tr>')
      }
    });
  }
</script>