 <?php $this->load->view("header", ["page" => $page]);?>

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Absensi <span id="nama-rombel"></span></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active">Absensi</li>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <form id="form-simpan-absensi">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <input type="hidden" name="id_rombel" id="id-rombel" class="form-control">
                <div class="form-group col-3">
                  <label>Pilih Semester</label>
                  <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt" style="width: 100%;">
                    <option selected="selected" disabled>Pilih semester</option>
                    <option value = "1">1 (Gasal)</option>
                    <option value = "2">2 (Genap)</option>
                  </select>
                </div>
                <div class="form-group col-9">
                  <button type="submit " class="btn btn-flat btn-success float-right " data-toggle="modal" data-target="#tambah-kelas"><i class="fa fa-save "></i> Simpan</button>
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.card-header -->
          </div>
        </div>

        <div class="col-12" id="show-absensi" hidden >
          <div class="card">
            <div class="card-body">

             <table class="table table-bordered table-striped" data-plugin="absensi" style="width:100%"> 
              <thead>
                <tr>
                  <th rowspan="2" colspan="1" width="1%">No</th>
                  <th rowspan="2" colspan="1">Nama Siswa</th>
                  <th colspan="3" rowspan="1" style="text-align: center;">Ketidakhadiran</th>
                </tr>
                <tr>
                 <th style="width: 7%">Sakit</th>
                 <th style="width: 7%">Ijin</th>
                 <th style="width: 7%">Alpa</th>
               </tr>
             </thead>
             <tbody  id="table-absensi">

             </tbody>

           </table>
         </div>
       </div>
     </div>
   </form>
   <!-- /.card -->
 </div>
</section>
<!-- /.content -->
<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
  <i class="fas fa-chevron-up"></i>
</a>
</div>


<?php
$this->load->view("footer");
?>

<script>

  $(document).ready(function () {

    var datatableNilaiSikap;

    var dataRombel;

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $.ajax({
      type: 'POST',
      url: `${base_url}/Rombel/getRombelWalas/`,
      dataType: 'json'
    })
    .done(function (res) {
      if(res.success){
       $('#id-rombel').val(res.data.id_rombel);
       $('#nama-rombel').text(res.data.nama_rombel);
     }
   });


    $('.select2-smt').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var smt = data.id;

      var id_rombel = $('#id-rombel').val();

      if(id_rombel) set_data(id_rombel, smt);  

    });

    $('#form-simpan-absensi').submit(function (e) {
      e.preventDefault();

      let form = $('#form-simpan-absensi').serializeObject();
      let penilaian = [];

      form.data.id_anggota_rombel.map((item, index) => {
        penilaian.push({
          id_absensi: form.data.id_absensi[index],
          id_anggota_rombel: form.data.id_anggota_rombel[index],
          sakit: form.data.sakit[index],
          ijin: form.data.ijin[index],
          alpa: form.data.alpa[index]
        });
      });
      let data = {
        rombel: form.data.rombel,
        semester: form.data.semester,
        penilaian
      }

      $.ajax({
        type: 'POST',
        url: `${base_url}/Absensi/saveAbsensi/`,
        data: data,
        dataType: 'json'
      })
      .done(function (res) {
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
        $('#show-absensi').attr('hidden',false);
      });
    });

    function set_data(rombel, smt) {
      $.ajax({
        type: 'GET',
        url: "/Absensi/getAbsensi/"+rombel+"/"+smt,
        dataType: 'json'
      })
      .done(function (res) {
        $('#table-absensi').html("");
        if (res.success) {

          let number = 1;

          res.data.forEach(element => {
            let sakit = '';
            let ijin = '';
            let alpa = '';

            if (element.sakit != null && element.sakit != 0) {
              sakit = element.sakit;
            }
            else{
              sakit = '';
            }
            if (element.ijin != null && element.sakit != 0) {
              ijin = element.ijin;
            }
            else{
              ijin = '';
            }
            if (element.alpa != null && element.sakit != 0) {
              alpa = element.alpa;
            }
            else{
              alpa = '';
            }
            
            $('#table-absensi').append(
              '<tr><td width="30"><input class="form-control" hidden type="text" name="data[id_absensi][]" value="'+element.id_absensi+'"><input class="form-control" hidden type="text" name="data[id_anggota_rombel][]" value="'+element.id_anggota_rombel+'">' + number++ + '</td><td>' + element.nama_siswa + '</td><td><input class="form-control" type="number" name="data[sakit][]" value="'+ sakit + '" style="width: 100%" autocomplete="off" ></td><td><input class="form-control" type="number" name="data[ijin][]" value="'+ ijin + '" style="width: 100%" autocomplete="off" ></td><td><input class="form-control" type="number" name="data[alpa][]" value="'+ alpa + '" style="width: 100%" autocomplete="off" ></td></tr>')

            $('#show-absensi').attr('hidden',false);
          }); 
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      }); 
    }
  });

</script>