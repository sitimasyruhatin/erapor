 <?php $this->load->view("header", ["page" => $page]);?>

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Catatan Wali Kelas <span id="nama-rombel"></span></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active">Catatan Wali Kelas</li>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <form id="form-simpan-catatan-walas">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <div class="row">
<!--                 <div class="form-group col-3">
                  <label>Pilih Rombel</label>
                  <span class="text-danger">*</span>
                  <select name="data[rombel]" id="id-rombel" class="form-control select2 select2-rombel" style="width: 100%;" required="">
                    <option value="" selected>Pilih item</option>
                  </select>
                </div> -->
                <input type="hidden" name="id_rombel" id="id-rombel" class="form-control">
                <div class="form-group col-3">
                  <label>Pilih Semester</label>
                  <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt" style="width: 100%;">
                    <option selected="selected" disabled>Pilih semester</option>
                    <option value = "1">1 (Gasal)</option>
                    <option value = "2">2 (Genap)</option>
                  </select>
                </div>
                <div class="form-group col-9">
                  <button type="submit " class="btn btn-flat btn-success float-right " data-toggle="modal" data-target="#tambah-kelas"><i class="fa fa-save "></i> Simpan</button>
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.card-header -->
          </div>
        </div>

        <div class="col-12" id="show-catatan-walas" hidden >
          <div class="card">
            <div class="card-body">

             <table class="table table-bordered table-striped" data-plugin="catatanwalas" style="width:100%"> 
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align: middle;">No</th>
                  <th rowspan="2" style="vertical-align: middle; width: 20%">Nama Siswa</th>
                  <th colspan="2" style="text-align: center;">Catatan Wali Kelas</th>
                </tr>
              </thead>
              <tbody  id="table-catatan-walas">

              </tbody>

            </table>
          </div>
        </div>
      </div>
    </form>
    <!-- /.card -->
  </div>
</section>
<!-- /.content -->
<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
  <i class="fas fa-chevron-up"></i>
</a>
</div>


<?php
$this->load->view("footer");
?>

<script>

  $(document).ready(function () {

    var datatableNilaiSikap;

    var dataRombel;

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $.ajax({
      type: 'POST',
      url: `${base_url}/Rombel/getRombelWalas/`,
      dataType: 'json'
    })
    .done(function (res) {
      if(res.success){
       $('#id-rombel').val(res.data.id_rombel);
       $('#nama-rombel').text(res.data.nama_rombel);
       console.log(res.data);
     }
   });


    $('.select2-smt').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var smt = data.id;

      var id_rombel = $('#id-rombel').val();

      if(id_rombel) set_nilai(id_rombel, smt);  

    });

    $('#form-simpan-catatan-walas').submit(function (e) {
      e.preventDefault();

      let form = $('#form-simpan-catatan-walas').serializeObject();
      let penilaian = [];

      form.data.id_anggota_rombel.map((item, index) => {
        penilaian.push({
          id_catatan: form.data.id_catatan[index],
          id_anggota_rombel: form.data.id_anggota_rombel[index],
          catatan: form.data.catatan[index]
        });
      });
      let data = {
        rombel: form.data.rombel,
        semester: form.data.semester,
        penilaian
      }

      $.ajax({
        type: 'POST',
        url: `${base_url}/CatatanWalas/saveCatatanWalas/`,
        data: data,
        dataType: 'json'
      })
      .done(function (res) {
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          // setTimeout(() => {
          //   location.reload();
          // }, 1000);

          // $('#show-catatan-walas').html('');
          // $('.select2-smt').val(data.semester).trigger("change");
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
          // setTimeout(() => {
          //   location.reload();
          // }, 1000);
        }
        $('#show-catatan-walas').attr('hidden',false);
      });
    });

    function set_nilai(rombel, smt) {
      $.ajax({
        type: 'GET',
        url: "/CatatanWalas/getCatatanWalas/"+rombel+"/"+smt,
        dataType: 'json'
      })
      .done(function (res) {
        $('#table-catatan-walas').html("");
        if (res.success) {

          let number = 1;

          res.data.forEach(element => {
            let catatan = '';

            if (element.catatan != null) {
              catatan = element.catatan;
            }
            $('#table-catatan-walas').append(
              '<tr><td width="30"><input class="form-control" hidden type="text" name="data[id_catatan][]" value="'+element.id_catatan+'"><input class="form-control" hidden type="text" name="data[id_anggota_rombel][]" value="'+element.id_anggota_rombel+'">' + number++ + '</td><td>' + element.nama_siswa + '</td><td><textarea class="form-control" name="data[catatan][]" rows="2" value="'+ catatan +'" >'+ catatan +'</textarea></td></tr>')

            $('#show-catatan-walas').attr('hidden',false);
          }); 
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      }); 
    }
  });


</script>