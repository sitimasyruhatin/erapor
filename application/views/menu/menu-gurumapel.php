<li class="nav-header">MENU GURU MAPEL</li>
<li class="nav-item">
	<a href="<?php echo base_url()."guru-mapel/nilai-pengetahuan/"?>" class="nav-link <?php if ($page == 'nilaipengetahuan') {echo 'active';} ?>">
		<i class="nav-icon fas fa-book-open"></i>
		<p>
			Nilai Pengetahuan
		</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url()."guru-mapel/nilai-keterampilan/"?>" class="nav-link <?php if ($page == 'nilaiketerampilan') {echo 'active';} ?>">
		<i class="nav-icon fas fa-microscope"></i>
		<p>
			Nilai Keterampilan
		</p>
	</a>
</li>