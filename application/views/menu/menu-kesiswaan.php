<li class="nav-header">MENU KESISWAAN</li>
<li class="nav-item">
	<a href="<?php echo base_url()."kesiswaan/nilai-ekstrakurikuler/"?>" class="nav-link <?php if ($page == 'nilaiekskul') {echo 'active';} ?>">
		<i class="nav-icon fas fa-running"></i>
		<p>
			Ekstrakurikuler
		</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url()."kesiswaan/prestasi/"?>" class="nav-link <?php if ($page == 'prestasi') {echo 'active';} ?>">
		<i class="nav-icon fas fa-award"></i>
		<p>
			Prestasi
		</p>
	</a>
</li>