<li class="nav-header">MENU WALI KELAS</li>
<li class="nav-item">
	<a href="<?php echo base_url()."wali-kelas/absensi/" ?>" class="nav-link <?php if ($page == 'absensi') {echo 'active';} ?>">
		<i class="nav-icon far fa-address-card"></i>
		<p>
			Absensi
		</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url()."wali-kelas/catatan-wali-kelas/" ?>" class="nav-link <?php if ($page == 'catatan-walas') {echo 'active';} ?>">
		<i class="nav-icon far fa-sticky-note"></i>
		<p>
			Catatan Wali Kelas
		</p>
	</a>
</li>