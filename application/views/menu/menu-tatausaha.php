<li class="nav-header">MENU TATA USAHA</li>
<!-- <li class="nav-item">
	<a href="<?php echo base_url()."tata-usaha/profil-sekolah/" ?>" class="nav-link <?php if ($page == 'profil-sekolah') {echo 'active';} ?>">
		<i class="nav-icon fas fa-chalkboard-teacher"></i>
		<p>
			Profil Sekolah
		</p>
	</a>
</li> -->
<li class="nav-item">
	<a href="<?php echo base_url()."tata-usaha/list-tahun-ajaran/" ?>" class="nav-link <?php if ($page == 'tahunajaran') {echo 'active';} ?>">
		<i class="nav-icon far fa-calendar-alt"></i>
		<p>
			Data Tahun Ajaran
		</p>
	</a>
</li>
<li class="nav-item has-treeview <?php if (isset($master) && $master == 'open') {echo 'menu-open';}   ?>">

	<a href="#" class="nav-link">
		<i class="nav-icon fas fa-database"></i>
		<p>
			Data Master
			<i class="fas fa-angle-left right"></i>
		</p>
	</a>

	<ul class="nav nav-treeview">
		<!-- <li class="nav-item">
			<a href="<?php echo base_url()."tata-usaha/list-role/"?>" class="nav-link <?php if ($page == 'role') {echo 'active';} ?>" class="nav-link <?php if ($page == 'role') {echo 'active';} ?>">
				<i class="nav-icon far fa-dot-circle"></i>
				<p>
					Data Role
				</p>
			</a>
		</li> -->
		<li class="nav-item">
			<a href="<?php echo base_url()."tata-usaha/profil-sekolah/"?>" class="nav-link <?php if ($page == 'profil-sekolah') {echo 'active';} ?>" class="nav-link">
				<i class="nav-icon far fa-dot-circle"></i>
				<p>
					Profil Sekolah
				</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url()."tata-usaha/list-pegawai/"?>" class="nav-link <?php if ($page == 'pegawai') {echo 'active';} ?>" class="nav-link">
				<i class="nav-icon far fa-dot-circle"></i>
				<p>
					Data Pegawai
				</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url()."tata-usaha/list-siswa/"?>" class="nav-link <?php if ($page == 'siswa') {echo 'active';} ?>" class="nav-link">
				<i class="nav-icon far fa-dot-circle"></i>
				<p>
					Data Siswa
				</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url()."tata-usaha/list-mata-pelajaran/"?>" class="nav-link <?php if ($page == 'mapel') {echo 'active';} ?>">
				<i class="nav-icon far fa-dot-circle"></i>
				<p>
					Data Mata Pelajaran
				</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?php echo base_url()."tata-usaha/list-ekstrakurikuler/"?>" class="nav-link <?php if ($page == 'ekskul') {echo 'active';} ?>">
				<i class="nav-icon far fa-dot-circle"></i>
				<p>
					Data Ekstrakurikuler
				</p>
			</a>
		</li>
	</ul>
</li>



<li class="nav-item">
	<a href="<?php echo base_url()."tata-usaha/list-role-pegawai/"?>" class="nav-link <?php if ($page == 'rolepegawai') {echo 'active';} ?>" class="nav-link" class="nav-link">
		<i class="nav-icon fas fa-users"></i>
		<p>
			Data Role Pegawai
		</p>
	</a>
</li>

<li class="nav-item">
	<a href="<?php echo base_url()."tata-usaha/list-rombel/"?>" class="nav-link <?php if ($page == 'rombel') {echo 'active';} ?>" class="nav-link">
		<i class="nav-icon fas fa-store-alt"></i>
		<p>
			Data Rombel
		</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo base_url()."tata-usaha/anggota-rombel/"?>" class="nav-link <?php if ($page == 'anggotarombel') {echo 'active';} ?>" class="nav-link">
		<i class="nav-icon fas fa-user-graduate"></i>
		<p>
			Data Anggota Rombel
		</p>
	</a>
</li>

<li class="nav-item">
	<a href="<?php echo base_url()."tata-usaha/pembelajaran/"?>" class="nav-link <?php if ($page == 'pembelajaran') {echo 'active';} ?>" class="nav-link">
		<i class="nav-icon fas fa-chalkboard-teacher"></i>
		<p>
			Data Pembelajaran
		</p>
	</a>
</li>
<!-- <li class="nav-item">
	<a href="<?php echo base_url()."tata-usaha/leger/"?>" class="nav-link <?php if ($page == 'leger') {echo 'active';} ?>" class="nav-link">
		<i class="nav-icon fas fa-copy"></i>
		<p>
			Leger
		</p>
	</a>
</li> -->
<li class="nav-item">
	<a href="<?php echo base_url()."tata-usaha/cetak-rapor/"?>" class="nav-link <?php if ($page == 'cetak-rapor') {echo 'active';} ?>" class="nav-link">
		<i class="nav-icon fas fa-print"></i>
		<p>
			Cetak Rapor
		</p>
	</a>
</li>