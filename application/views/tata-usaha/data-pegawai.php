 <?php $this->load->view("header", ["page" => $page]);?>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Pegawai</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Data Pegawai</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-tambah-pegawai"><i class="fas fa-plus"></i> Tambah</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              <table class="table table-bordered table-striped" data-plugin="datatable">
                <thead>
                  <tr>
                    <th width="10" class="text-center">No</th>
                    <th>NIK</th>
                    <th>Nama Pegawai</th>
                    <th width="60" class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>
</div>

<div class="modal fade" id="modal-tambah-pegawai" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-tambah-pegawai">
        <div class="modal-body">
          <!-- text input -->
          <div class="form-group">
            <label>NIK</label>
            <span class="text-danger">*</span>
            <input type="text" name="nik" data-number-only="true" class="form-control set-null" placeholder="Masukkan nik" required>
          </div>
          <div class="form-group">
            <label>Nama Pegawai</label>
            <span class="text-danger">*</span>
            <input type="text" name="nama_pegawai" class="form-control set-null" placeholder="Masukkan nama pegawai" required>
          </div>
          <div class="form-group">
            <label>Password</label>
            <span class="text-danger">*</span>
            <div class="input-group">
              <div class="input-group-append">
                <div class="input-group-text"><i class="fa fa-eye"></i>
                </div>
              </div>
              <input type="password" name="password" class="form-control set-null" placeholder="Masukkan password" required data-toggle="password">
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-update-pegawai" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-update-pegawai">
        <div class="modal-body">
          <div class="form-group">
            <label>NIK</label>
            <span class="text-danger">*</span>
            <input type="text" name="nik" data-number-only="true" class="form-control" placeholder="Masukkan NIK" required>
          </div>
          <div class="form-group">
            <label>Nama Pegawai</label>
            <span class="text-danger">*</span>
            <input type="text" name="nama_pegawai" class="form-control" placeholder="Masukkan nama pegawai" required>
          </div>
          <div class="form-group">
            <label>Ubah Password</label>
            <div class="input-group">
              <div class="input-group-append">
                <div class="input-group-text"><i class="fa fa-eye"></i>
                </div>
              </div>
              <input type="password" name="password" class="form-control set-null" placeholder="Masukkan password" data-toggle="password">
            </div>
          </div>

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-delete-pegawai" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-delete-pegawai">
        <div class="modal-body">
          <input type="hidden" name="id" class="form-control">
          Apakah Anda yakin akan menghapus pegawai?
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>

<?php
$this->load->view("footer");
?>

<script>
  var datatable;

  $(document).ready(function () {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    datatable = $('[data-plugin=datatable]').DataTable({
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
        emptyTable: "No data available in table"
      },
      "ajax": "/Pegawai/getPegawai",
      "columns": [{
        className: "text-center"
      },
      {
        "data": "nik"
      },
      {
        "data": "nama_pegawai"
      },
      {
        className: "text-center"
      }
      ],
      "columnDefs": [{
        "targets": 0,
        "render": function (data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      },
      {
        "targets": 3,
        "render": function (data, type, row, meta) {
         return '<button type="button" class="btn btn-primary btn-sm mr-1" onclick="update(' +
         row.id_pegawai + ')"><i class="fas fa-pencil-alt"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="softdelete(' +
         row.id_pegawai + ')"><i class="fas fa-trash-alt"></i></button>';
        }
      }]

   });

    $('#form-tambah-pegawai').validate({
      rules: {
        nik : {
          digits: true
        }
      },
      messages: {
        nik: {
          required:"Kolom ini wajib diisi.",
          digits:"Kolom nik harus berupa angka."
        },
        nama_pegawai: {
          required: "Kolom ini wajib diisi."
        },
        password: {
          required: "Kolom ini wajib diisi."
        }
      },
      submitHandler: function (form) {
        var form = $('#form-tambah-pegawai');
        $.ajax({
          type: 'POST',
          url: `${base_url}/Pegawai/tambahPegawai`,
          data: $('#form-tambah-pegawai').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          // $('.set-null').val(null);
          $('#modal-tambah-pegawai').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });          
            datatable.ajax.reload(null, false);
          }
        });
      }
    });

    $('#form-update-pegawai').validate({
      rules: {
        nik : {
          digits: true
        }
      },
      messages: {
        nik: {
          required:"Kolom ini wajib diisi.",
          digits:"Kolom nik harus berupa angka."
        },
        nama_pegawai: {
          required: "Kolom ini wajib diisi."
        }
      },
      submitHandler: function (form) {
        var form = $('#form-update-pegawai');
        $.ajax({
          type: 'POST',
          url: `${base_url}/Pegawai/updatePegawai/${$('#form-update-pegawai').attr('data-form-id')}`,
          data: $('#form-update-pegawai').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('.set-null').val(null);
          $('#modal-update-pegawai').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          }
        });
      }
    });


    $('#form-delete-pegawai').submit(function (e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: `${base_url}/Pegawai/deletePegawai/${$('#form-delete-pegawai').attr('data-form-id')}`,
        data: $('#form-delete-pegawai').serialize(),
        dataType: 'json'
      })
      .done(function (res) {
        $('#modal-delete-pegawai').modal('hide');
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          datatable.ajax.reload(null, false);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
          datatable.ajax.reload(null, false);
        }
      });
    });
  });

 function update(id) {
  $.ajax({
    type: 'POST',
    url: `${base_url}/Pegawai/getPegawai/${id}`,
    data: $('#form-tambah-pegawai').serialize(),
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-update-pegawai');

      var data = res.data[0];

      form.attr('data-form-id', data.id_pegawai)
      form.find('[name=nik]').val(data.nik)
      form.find('[name=nama_pegawai]').val(data.nama_pegawai)
      // form.find('[name=password]').val(res.data.password)
      $('#modal-update-pegawai').modal();
    }
  });
}

function softdelete(id) {
  $.ajax({
    type: 'GET',
    url: `${base_url}/Pegawai/getPegawai/${id}`,
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-delete-pegawai');
      var data = res.data[0];
      form.attr('data-form-id', data.id_pegawai)
      $('#modal-delete-pegawai').modal();
    }
  });
}

</script>