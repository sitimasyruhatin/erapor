 <?php $this->load->view("header", ["page" => $page]);?>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Mata Pelajaran</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Data Mata Pelajaran</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-tambah-mapel"><i class="fas fa-plus"></i> Tambah</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              <table class="table table-bordered table-striped" data-plugin="datatable">
                <thead>
                  <tr>
                    <th width="10" class="text-center">No</th>
                    <th>Nama Mapel</th>
                    <th>Singkatan Mapel</th>
                    <th>Jenis Mapel</th>
                    <th width="60" class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>

</div>

<div class="modal fade" id="modal-tambah-mapel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Mapel</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-tambah-mapel">
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-8">
              <label>Nama Mapel</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_mapel" class="form-control mapel" placeholder="Masukkan Nama Mapel" required>
            </div>
            <div class="form-group col-4">
              <label>Singkatan Mapel</label>
              <span class="text-danger">*</span>
              <input type="text" name="sngkt_mapel" class="form-control mapel" placeholder="cth: PKN, BI dsb.." required>
            </div>
            <div class="form-group col-12">
              <label>Jenis Mapel</label>
              <span class="text-danger">*</span>
              <select class="form-control select2 mapel" id="set-null" style="width: 100%;" name="jenis_mapel" required>
                <option value="" disabled selected>Pilih Jenis Mapel</option>
                <option value="5|1,2">Bimbingan Konseling</option>
                <option value="1|1,2">Kelompok A (Umum)</option>
                <option value="2|1,2">Kelompok B (Umum)</option>
                <option value="3|1">Kelompok C (Peminatan) MIPA</option>
                <option value="3|2">Kelompok C (Peminatan) IPS</option>
                <option value="4|1,2">Kelompok C (Peminatan) Lintas Minat</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>


<div class="modal fade" id="modal-update-mapel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Mapel</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-update-mapel">
        <input type="hidden" name="id" class="form-control">
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-8">
              <label>Nama Mapel</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_mapel" class="form-control" placeholder="Masukkan Nama Mapel" required>
            </div>
            <div class="form-group col-4">
              <label>Singkatan Mapel</label>
              <span class="text-danger">*</span>
              <input type="text" name="sngkt_mapel" class="form-control" placeholder="cth: PKN, BI dsb.." required>
            </div>
            <div class="form-group col-12">
              <label>Jenis Mapel</label>
              <span class="text-danger">*</span>
              <select class="form-control select2" style="width: 100%;" name="jenis_mapel" id="select2-jenis-mapel">
                <option disabled selected>Pilih Jenis Mapel</option>
                <option value="5|1,2">Bimbingan Konseling</option>
                <option value="1|1,2">Kelompok A (Umum)</option>
                <option value="2|1,2">Kelompok B (Umum)</option>
                <option value="3|1">Kelompok C (Peminatan) MIPA</option>
                <option value="3|2">Kelompok C (Peminatan) IPS</option>
                <option value="4|1,2">Kelompok C (Peminatan) Lintas Minat</option>
              </select>
            </div>

          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-delete-mapel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Mapel</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-delete-mapel">
        <div class="modal-body">
          <input type="hidden" name="id" class="form-control">
          Apakah Anda yakin akan menghapus mapel ?
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
$this->load->view("footer");
?>

<script>
  var datatable;

  $(function(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    datatable = $('[data-plugin=datatable]').DataTable({
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
        emptyTable: "No data available in table"
      },
      "ajax": "/Mapel/getMapel",
      "columns": [{
        className: "text-center"
      },
      {
        "data": "nama_mapel"
      },
      {
        "data": "sngkt_mapel"
      },
      {},
      {
        className: "text-center"
      }
      ],
      "columnDefs": [{
        "targets": 0,
        "render": function (data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      },
      {
        "targets": 3,
        "render": function (data, type, row, meta) {
          var html = '';

          switch (row.kelompok_mapel) {
            case '1':
            html += '<span>Kelompok A (Umum)</span><br>';    
            break;
            case '2':
            html += '<span>Kelompok B (Umum)</span><br>';    
            break;
            case '3':
            html += '<span>Kelompok C (Peminatan)</span><br>';    
            break;
            case '4':
            html += '<span>Kelompok C (Lintas Minat)</span><br>';    
            break;
            case '5':
            html += '<span>Bimbingan Konseling</span><br>';    
            break;
            default:
            html += '-'; 
            break;
          }

          var jenis_rombel = row.jenis_rombel.split(',');
          jenis_rombel.forEach(jns => {
            switch (jns) {
              case '1':
              html += '<span class="badge bg-light-blue">MIPA</span>';    
              break;
              case '2':
              html += '<span class="badge bg-light-blue">IPS</span>';    
              break;
              default:              
              break;
            }
          });    
          return html;
        }
      },
      {
        "targets": 4,
        "render": function (data, type, row, meta) {
         return '<button type="button" class="btn btn-primary btn-sm mr-1" onclick="update(' +
         row.id_mapel + ')"><i class="fas fa-pencil-alt"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="softdelete(' +
         row.id_mapel + ')"><i class="fas fa-trash-alt"></i></button>';
       }
     }
     ]

   });

    $('#form-tambah-mapel').validate({
      messages: {
        nama_mapel: {
          required:"Kolom ini wajib diisi."
        },
        sngkt_mapel: {
          required:"Kolom ini wajib diisi."
        },
        jenis_mapel: {
          required:"Kolom ini wajib diisi."
        }

      },
      submitHandler: function (form) {
        var form = $('#form-tambah-mapel');
        $.ajax({
          type: 'POST',
          url: `${base_url}/Mapel/tambahMapel`,
          data: $('#form-tambah-mapel').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('#set-null').val('').trigger('change');
          $('#form-tambah-mapel')[0].reset();
          $('#modal-tambah-mapel').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });          

          }
        });
      }
    });

    $('#form-update-mapel').validate({
      messages: {
        nama_mapel: {
          required:"Kolom ini wajib diisi."
        },
        sngkt_mapel: {
          required:"Kolom ini wajib diisi."
        },
        jenis_mapel: {
          required:"Kolom ini wajib diisi."
        }

      },
      submitHandler: function (form) {
        var form = $('#form-update-mapel');
        $.ajax({
          type: 'POST',
          url: `${base_url}/Mapel/updateMapel/${$('#form-update-mapel').attr('data-form-id')}`,
          data: $('#form-update-mapel').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('#modal-update-mapel').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });
          }
        });
      }
    });

    $('#form-delete-mapel').submit(function (e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: `${base_url}/Mapel/deleteMapel/${$('#form-delete-mapel').attr('data-form-id')}`,
        data: $('#form-delete-mapel').serialize(),
        dataType: 'json'
      })
      .done(function (res) {
        $('#modal-delete-mapel').modal('hide');
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          datatable.ajax.reload(null, false);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      });
    });
  });

 function update(id) {
  $.ajax({
    type: 'POST',
    url: `${base_url}/Mapel/getMapel/${id}`,
    data: $('#form-tambah-mapel').serialize(),
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-update-mapel');

      var data = res.data[0];
      form.attr('data-form-id', data.id_mapel);
      form.find('[name=nama_mapel]').val(data.nama_mapel);
      form.find('[name=sngkt_mapel]').val(data.sngkt_mapel);

      var pre = data.kelompok_mapel + '|' + data.jenis_rombel;
      var kel = '';
      switch (pre) {
        case '1|1,2':
        kel = 'Kelompok A (Umum)';
        break;
        case '2|1,2':
        kel = 'Kelompok B (Umum)';
        break;
        case '3|1':
        kel = 'Kelompok C (Peminatan) MIPA';
        break;
        case '3|2':
        kel = 'Kelompok C (Peminatan) IPS';
        break;
        case '4|1,2':
        kel = 'Kelompok C (Peminatan) Lintas Minat';
        break;
        case '5|1,2':
        kel = 'Bimbingan Konseling';
        break;
        default:
        break;
      }

      form.find('[name=jenis_mapel]').val(pre).trigger('change');
      $('#modal-update-mapel').modal();
    }
  });
}

function softdelete(id) {
  $.ajax({
    type: 'GET',
    url: `${base_url}/Mapel/getMapel/${id}`,
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-delete-mapel');

      var data = res.data[0];
      form.attr('data-form-id', data.id_mapel)
      $('#modal-delete-mapel').modal();
    }
  });
}
</script>