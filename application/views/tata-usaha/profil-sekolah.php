 <?php $this->load->view("header", ["page" => $page]);?>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Profil Sekolah</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Profil Sekolah</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#modal-profil-sekolah" onclick="update(<?=$sekolah->id_sekolah;?>)"><i class="fas fa-edit"></i> Edit</button>
            </div>
            <div class="card-body table-responsive">
             <table class="table">
              <tbody>
                <tr>
                  <th scope="row" style="background-color: rgba(0,0,0,.05);">Nama Sekolah</th>
                  <td><?=$sekolah->nama_sekolah;?></td>
                </tr>
                <tr>
                  <th scope="row" style="background-color: rgba(0,0,0,.05);">Nama Kepala Sekolah</th>
                  <td><?=$sekolah->nama_kepsek;?></td>
                </tr>
                <tr>
                  <th scope="row" style="background-color: rgba(0,0,0,.05);">NIK Kepala Sekolah</th>
                  <td><?=$sekolah->nik_kepsek;?></td>
                </tr>
                <tr>
                  <th scope="row" style="background-color: rgba(0,0,0,.05);">Alamat Sekolah</th>
                  <td><?=$sekolah->alamat_sekolah;?></td>
                </tr>
                <tr>
                  <th scope="row" style="background-color: rgba(0,0,0,.05);">KKM Sekolah</th>
                  <td><?=$sekolah->kkm;?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
  <i class="fas fa-chevron-up"></i>
</a>

</div>

<div class="modal fade" id="modal-update-profil" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Profil Sekolah</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-update-profil">
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-12">
              <label>Nama Sekolah</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_sekolah" class="form-control ekskul" placeholder="Masukkan nama sekolah" required>
            </div>
            <div class="form-group col-12">
              <label>Nama Kepala Sekolah</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_kepsek" class="form-control ekskul" placeholder="Masukkan nama kepala sekolah" required>
            </div>
            <div class="form-group col-12">
              <label>NIK Kepala Sekolah</label>
              <span class="text-danger">*</span>
              <input type="text" name="nik_kepsek" class="form-control ekskul" placeholder="Masukkan nik kepala sekolah" required>
            </div>
            <div class="form-group col-12">
              <label>Alamat Sekolah</label>
              <span class="text-danger">*</span>
              <textarea class="form-control" name="alamat" placeholder="Masukkan alamat sekolah"></textarea>      
            </div>
            <div class="form-group col-12">
              <label>KKM Sekolah</label>
              <span class="text-danger">*</span>
              <input type="number" name="kkm" class="form-control ekskul" placeholder="Masukkan kkm sekolah" required>     
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
$this->load->view("footer");
?>

<script>
  var datatable;

  $(function(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $('#form-update-profil').validate({
      messages: {
        nama_ekskul: {
          required:"Kolom ini wajib diisi."
        },
        nik_kepsek: {
          required:"Kolom ini wajib diisi."
        },
        nama_kepsek: {
          required:"Kolom ini wajib diisi."
        },
        alamat: {
          alamat:"Kolom ini wajib diisi."
        }
      },
      submitHandler: function (form) {
        var form = $('#form-update-profil');
        $.ajax({
          type: 'POST',
          url: `${base_url}/Sekolah/updateSekolah/${$('#form-update-profil').attr('data-form-id')}`,
          data: $('#form-update-profil').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('#modal-update-profil').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            setTimeout(() => {
              location.reload();
            }, 1000);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            }); 
            setTimeout(() => {
              location.reload();
            }, 1000);       
          }
        });
      }
    });
  });

  function update(id) {
    $.ajax({
      type: 'POST',
      url: `${base_url}/Sekolah/getSekolah/${id}`,
      data: $('#form-update-profil').serialize(),
      dataType: 'json'
    })
    .done(function (res) {
      if(res.success){
        var form = $('#form-update-profil');
        var data = res.data[0];
        form.attr('data-form-id', data.id_sekolah)
        form.find('[name=nama_sekolah]').val(data.nama_sekolah)
        form.find('[name=nama_kepsek]').val(data.nama_kepsek)
        form.find('[name=nik_kepsek]').val(data.nik_kepsek)
        form.find('[name=alamat]').val(data.alamat_sekolah)

        form.find('[name=kkm]').val(data.kkm)
        $('#modal-update-profil').modal();
      }
    });
  }
</script>