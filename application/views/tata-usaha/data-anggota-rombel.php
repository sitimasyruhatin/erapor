 <?php $this->load->view("header", ["page" => $page]);?>
 <style type="text/css">
 table.dataTable thead th, table.dataTable tbody td {
  padding: 7px 8px;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Data Anggota Rombel</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active">Data Anggota Rombel</li>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="form-group col-3">
            <label>Pilih Rombel</label>
            <span class="text-danger">*</span>
            <select name="rombel" class="form-control select2 select2-rombel1" style="width: 100%;" required="">
              <option value="" selected="selected">Pilih</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="card" hidden id="show-anggota">
      <div class="card-body">
        <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#tambah-anggota" role="tab" aria-controls="custom-content-below-home" aria-selected="true">Tambah Anggota</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#copy-rombel" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Advance</a>
          </li>
        </ul>
        <div class="tab-content" id="custom-content-below-tabContent">
          <div class="tab-pane fade show active" id="tambah-anggota" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
            <div class="row">
              <div class="col-6">
               <div class="card">
                <div class="card-header">
                 <h3 class="card-title"><b>Anggota Rombel <span id="judul-anggota"></span></b></h3>
               </div>
               <div class="card-body table-responsive">
                <table class="table table-bordered table-striped" data-plugin="rombelsiswa">
                  <thead>
                    <tr>
                      <th width="1">No</th>
                      <th width="30">NIS</th>
                      <th width="60">Nama Siswa</th>
                      <th width="9">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-6">
            <div class="card">
              <form id="add-siswa">
                <div class="card-header">
                  <h3 class="card-title"><b>Tambah Anggota Rombel</b></h3>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-bordered bg-light" data-plugin="allsiswa" style="width:100%">
                    <thead>
                      <tr>
                        <th></th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                  <div class="form-group mt-2">
                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="copy-rombel" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">

        <div class="row">
          <div class="col-4">
            <p class="lead mb-0" style="font-weight: bold">Copy dari rombel sebelumnya</b>
              <form id="copy-rombel">
                <div class="form-group">
                  <select name="rombel" class="form-control select2 select2-rombel2" required="">
                    <option value="" selected="selected">Pilih Rombel</option>
                  </select>
                  <br>
                  <div class="form-group">
                   <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Copy Kelas</button>
                 </div>
               </div>
             </form>
           </div>
           <div class="col-8" hidden id="show-copy-rombel">
            <div class="card">
              <form id="add-siswa">
                <div class="card-header">
                  <h3 class="card-title"><b>Anggota Rombel <span id="daftar-copy-rombel"></span></b></h3>
                </div>
                <div class="card-body table-responsive">
                 <table class="table table-bordered table-striped" data-plugin="copyrombel" style="width: 100%;" >
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NIS</th>
                      <th>Nama Siswa</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div> 
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- /.content -->
<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
  <i class="fas fa-chevron-up"></i>
</a>
</div>

<?php
$this->load->view("footer");
?>

<script>

    // var datatableAddSiswa;
    $(document).ready(function () {

      var datatableAddSiswa = $('[data-plugin=allsiswa]').DataTable();

      var datatableAnggotaRombel =  $('[data-plugin=rombelsiswa]').DataTable();

      var datatableCopyKelas =  $('[data-plugin=copyrombel]').DataTable();

      var selectedRombel = null;

      var selectedRombel2 = null;

      var siswaCopyKelas = [];

      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

      datatableAddSiswa.clear().destroy();

      datatableAddSiswa = $('[data-plugin=allsiswa]').DataTable({
        paging:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
          emptyTable: "No data available in table"
        },
        "ajax": "/Siswa/getSiswa",
        "columns": [{
          "data":"id_siswa",
          className: "text-center"
        },
        {
          "data": "nis"
        },
        {
          "data": "nama_siswa"
        }
        ],
        "columnDefs": [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'render': function (data, type, full, meta){
           return '<input type="checkbox" class="form" name="id[]" value="' 
           + $('<div/>').text(data).html() + '">';
         }
       }
       ],
     // 'order': [1, 'asc']
   });


      $('#add-siswa').on('submit', function(e){
      // Prevent actual form submission
      e.preventDefault();

      // Serialize form data
      var data =  $('#add-siswa').serializeArray();
      
      var siswaTambahan = [];

      data.shift();
      
      $(data).each(function(index,element){

        siswaTambahan.push(element.value);

      });

      if (siswaTambahan.length == 0){
        Toast.fire({
          type: 'error',
          title: 'Tidak ada siswa yang dipilih',
        });
      }
      else {
        var object = {
          rombel:selectedRombel,
          siswa:siswaTambahan,
        }

        $.ajax({
          type: 'POST',
          url: `${base_url}/AnggotaRombel/tambahAnggotaRombel`,
          data: object,
          dataType: 'json'
        })
        .done(function (res) {
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatableAnggotaRombel.ajax.reload(null, false);

            // $('input[type=checkbox]').removeAttr('checked');
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });
          }
          $('#add-siswa')[0].reset();
        });
      }
    });

      $(".select2-rombel1").select2({
        ajax: {
          url: "/Rombel/getCariRombel/",
          dataType: 'json',
          type: "GET",
          quietMillis: 50,
          processResults: function (res) {
            return {
              results: $.map(res.data, function (item) {
                return {
                  text: item.nama_rombel,
                  id: item.id_rombel
                }
              })
            };
          }
        }
      });

      $('.select2-rombel1').on('select2:selecting', function (e) {
        var data = e.params.args.data;
        selectedRombel = data.id;
        $('#judul-anggota').text(data.text);

        datatableAnggotaRombel.clear().destroy();

        datatableAnggotaRombel = $('[data-plugin=rombelsiswa]').DataTable({
          paging:true,
          ordering:true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
            emptyTable: "No data available in table"
          },
          "ajax": "/AnggotaRombel/getAnggotaRombel/"+data.id,
          "columns": [{
            className: "text-center"
          },
          {
            "data": "nis"
          },
          {
            "data": "nama_siswa"
          },
          {
            className: "text-center"
          }
          ],
          "columnDefs": [{
            "targets": 0,
            "render": function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            "targets":3,
            "render": function (data, type, row, meta) {
              return '<button type="button" class="btn btn-danger btn-xs" onclick="deleteAnggotaRombel(' +
              row.id_anggota_rombel + ')"><i class="fas fa-trash-alt"></i></button>';
            }
          }
          ]
        });
        $('#show-anggota').attr('hidden',false);
      });

      $(".select2-rombel2").select2({
        ajax: {
          url: "/Rombel/getCariRombelAllYear/",
          dataType: 'json',
          type: "GET",
          quietMillis: 50,
          processResults: function (res) {
            return {
              results: $.map(res.data, function (item) {
                return {
                  text: item.nama_rombel + " TA " + item.tahun_ajaran,
                  id: item.id_rombel
                }
              })
            };
          }
        }
      });

      $('.select2-rombel2').on('select2:selecting', function (e) {

        var data = e.params.args.data;

        selectedRombel2 = data.id;

        datatableCopyKelas.clear().destroy();

        $('#daftar-copy-rombel').text(data.text);

        $.ajax({
          type: 'GET',
          url: "/AnggotaRombel/getAnggotaRombel/"+data.id,
          dataType: 'json'
        })
        .done(function (res) {
          if (res.success) {

           var siswa = [];

           $(res.data).each(function(index,item){

            siswa.push([item.nis,item.nama_siswa]);

          });

           datatableCopyKelas = $('[data-plugin=copyrombel]').DataTable({
            paging:true,
            searching:true,
            language: {
              searchPlaceholder: 'Search...',
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
              emptyTable: "No data available in table"
            },

            data: siswa,
            "columns": [{
              className: "text-center"
            }, 
            {
              data: 0
            },
            {
              data: 1
            },
            ],
            "columnDefs": [{
              "targets": 0,
              "render": function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
              }
            }
            ]
          });

           $('#show-copy-rombel').attr('hidden',false);
         } 
         else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      });
      });
      $('#copy-rombel').on('submit', function(e){
      // Prevent actual form submission
      e.preventDefault();
      $.ajax({
        type: 'GET',
        url: `${base_url}/AnggotaRombel/copyRombel/`+selectedRombel2+'/'+selectedRombel,
        dataType: 'json'
      })
      .done(function (res) {
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          datatableAnggotaRombel.ajax.reload(null, false);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      });
    });
    });

 function deleteAnggotaRombel(params) {
  $.ajax({
    type: 'GET',
    url: "/AnggotaRombel/deleteAnggotaRombel/"+params,
    dataType: 'json'
  })
  .done(function (res) {

    Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    datatableAnggotaRombel = $('[data-plugin=rombelsiswa]').DataTable();

    if (res.success) {
      Toast.fire({
        type: 'success',
        title: res.message,
      });
      datatableAnggotaRombel.ajax.reload(null, false);
    } else {
      Toast.fire({
        type: 'error',
        title: res.message,
      });
    }
  });
}

</script>