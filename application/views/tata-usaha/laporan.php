<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/print.css'); ?>">
</head>

<!-- <body onload="window.print()"> -->
  <body>
    <div class="page-header">
      <div class="d-flex">
        <div class="left">
          <div>
            <label>Nama Sekolah</label>
            <span><?=$sekolah->nama_sekolah;?></span>
          </div>
          <div>
            <label>Alamat</label>
            <span><?=$sekolah->alamat_sekolah;?></span>
          </div>
          <div>
            <label>Nama</label>
            <span class="text-bold"><?=$penilaian->nama_siswa;?></span>
          </div>
          <div>
            <label>NIS/NISN</label>
            <span><?=$penilaian->nis;?>/<?=$penilaian->nisn;?></span>
          </div>
        </div>
        <div class="right">
          <div>
            <label>Kelas</label>
            <span><?=$penilaian->nama_rombel;?></span>
          </div>
          <div>
            <label>Semester</label>
            <span><?php if($penilaian->smt == 1){echo "1 (Gasal)";} else{ echo "2 (Genap)";}?></span>
          </div>
          <div>
            <label>Tahun Pelajaran</label>
            <span><?=$penilaian->tahun_ajaran;?></span>
          </div>
        </div>
      </div>
    </div>
    <table>
      <thead>
        <tr>
          <td>
            <!--place holder for the fixed-position header-->
            <div class="page-header-space"></div>
          </td>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>
            <!--*** CONTENT GOES HERE ***-->
            <div class="page">
              <h5 class="print-top-title">Capaian Hasil Belajar</h5>
              <div class="print-section">
                <h5 class="print-title">
                  <label class="print-title__number">A.</label>
                  Sikap
                </h5>
                <div class="print-section__body print-section__body--sikap">
                  <div class="print-section__row">
                    <h5 class="print-section__title">
                      <label class="print-title__number">1.</label>
                      Sikap Spiritual
                    </h5>
                    <table class="print-section__table table-pd-lg">
                      <thead>
                        <tr>
                          <th class="text-center">Predikat</th>
                          <th class="text-center">Deskripsi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="text-center"><?=$penilaian->nilai_spiritual;?></td>
                          <td class="text-center">
                            <?=$penilaian->deskripsi_spiritual;?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="print-section__row">
                    <h5 class="print-section__title">
                      <label class="print-title__number">2.</label>
                      Sikap Sosial
                    </h5>
                    <table class="print-section__table table-pd-lg">
                      <thead>
                        <tr>
                          <th class="text-center">Predikat</th>
                          <th class="text-center">Deskripsi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="text-center"><?=$penilaian->nilai_sosial;?></td>
                          <td class="text-center">
                           <?=$penilaian->deskripsi_sosial;?>
                         </td>
                       </tr>
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
           <div class="page">
            <div class="print-section">
              <h5 class="print-title">
                <label class="print-title__number">B.</label>
                Pengetahuan dan Ketrampilan
              </h5>
              <div class="print-section__body print-section__body--nilai">
                <div class="print-section__row">
                  <table class="print-section__table">
                    <thead>
                      <tr>
                        <th class="text-center" rowspan="2" width="30">NO</th>
                        <th class="text-center" rowspan="2">MATA PELAJARAN</th>
                        <th class="text-center" rowspan="2">KKM</th>
                        <th class="text-center" colspan="2">Pengetahuan</th>
                        <th class="text-center" colspan="2">Ketrampilan</th>
                      </tr>
                      <tr>
                        <td>Nilai</td>
                        <td>Huruf</td>
                        <td>Nilai</td>
                        <td>Huruf</td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="text-bold">
                        <td colspan="7">Kelompok A (Umum)</td>
                      </tr>
                      <?php $i=1;
                      foreach ($nilai_mapel as $nilai): 
                        if($nilai->kelompok_mapel == 1){?>

                          <tr>
                            <td class="text-center"><?=$i;?></td>
                            <td><?=$nilai->nama_mapel;?></td>
                            <td class="text-center"><?=$sekolah->kkm;?></td>
                            <td class="text-center"><?=$nilai->nilai_akhir_p;?></td>
                            <td class="text-center">
                              <?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_p < 70 ){
                                echo 'D';
                              }
                              elseif($nilai->nilai_akhir_p >= 70 && $nilai->nilai_akhir_p <= 74){
                                echo 'C';
                              }
                              elseif($nilai->nilai_akhir_p >= 75 && $nilai->nilai_akhir_p <= 85){
                                echo 'B';
                              }
                              elseif($nilai->nilai_akhir_p >= 86 && $nilai->nilai_akhir_p <= 100){
                                echo 'A';
                              }
                              else{
                                echo '';
                              }
                              ?>
                            </td>
                            <td class="text-center"><?=$nilai->nilai_akhir_k;?></td>
                            <td class="text-center"><?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_k < 70){
                              echo 'D';
                            }
                            elseif($nilai->nilai_akhir_k >= 70 && $nilai->nilai_akhir_k <= 74){
                              echo 'C';
                            }
                            elseif($nilai->nilai_akhir_k >= 75 && $nilai->nilai_akhir_k <= 85){
                              echo 'B';
                            }
                            elseif($nilai->nilai_akhir_k >= 86 && $nilai->nilai_akhir_k <= 100){
                              echo 'A';
                            }
                            else{
                              echo '';
                            }
                            ?></td>
                          </tr>
                          <?php $i++;}  endforeach;  ?>

                          <tr class="text-bold">
                            <td colspan="7">Kelompok B (Umum)</td>
                          </tr>
                          <?php $j=1;
                          foreach ($nilai_mapel as $nilai): 
                            if($nilai->kelompok_mapel == 2){?>

                              <tr>
                                <td class="text-center"><?=$j;?></td>
                                <td><?=$nilai->nama_mapel;?></td>
                                <td class="text-center"><?=$sekolah->kkm;?></td>
                                <td class="text-center"><?=$nilai->nilai_akhir_p;?></td>
                                <td class="text-center">
                                  <?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_p < 70 ){
                                    echo 'D';
                                  }
                                  elseif($nilai->nilai_akhir_p >= 70 && $nilai->nilai_akhir_p <= 74){
                                    echo 'C';
                                  }
                                  elseif($nilai->nilai_akhir_p >= 75 && $nilai->nilai_akhir_p <= 85){
                                    echo 'B';
                                  }
                                  elseif($nilai->nilai_akhir_p >= 86 && $nilai->nilai_akhir_p <= 100){
                                    echo 'A';
                                  }
                                  else{
                                    echo '';
                                  }
                                  ?>
                                </td>
                                <td class="text-center"><?=$nilai->nilai_akhir_k;?></td>
                                <td class="text-center"><?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_k < 70){
                                  echo 'D';
                                }
                                elseif($nilai->nilai_akhir_k >= 70 && $nilai->nilai_akhir_k <= 74){
                                  echo 'C';
                                }
                                elseif($nilai->nilai_akhir_k >= 75 && $nilai->nilai_akhir_k <= 85){
                                  echo 'B';
                                }
                                elseif($nilai->nilai_akhir_k >= 86 && $nilai->nilai_akhir_k <= 100){
                                  echo 'A';
                                }
                                else{
                                  echo '';
                                }
                                ?></td>
                              </tr>
                              <?php $j++;}  endforeach;  ?>
                              <tr class="text-bold">
                                <td colspan="7">Kelompok C (Peminatan)</td>
                              </tr>
                              <tr class="text-bold">
                                <td class="text-center">I</td>
                                <td colspan="6">Peminatan Matematika dan Ilmu Pengetahuan Alam / Peminatan Ilmu Pengetahuan Sosial</td>
                              </tr>
                              <?php $k=1; $l=1;
                              foreach ($nilai_mapel as $nilai): 
                                if($nilai->kelompok_mapel == 3 && $nilai->jurusan == 1){?>

                                  <tr>
                                    <td class="text-center"><?=$k;?></td>
                                    <td><?=$nilai->nama_mapel;?></td>
                                    <td class="text-center"><?=$sekolah->kkm;?></td>
                                    <td class="text-center"><?=$nilai->nilai_akhir_p;?></td>
                                    <td class="text-center">
                                      <?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_p < 70 ){
                                        echo 'D';
                                      }
                                      elseif($nilai->nilai_akhir_p >= 70 && $nilai->nilai_akhir_p <= 74){
                                        echo 'C';
                                      }
                                      elseif($nilai->nilai_akhir_p >= 75 && $nilai->nilai_akhir_p <= 85){
                                        echo 'B';
                                      }
                                      elseif($nilai->nilai_akhir_p >= 86 && $nilai->nilai_akhir_p <= 100){
                                        echo 'A';
                                      }
                                      else{
                                        echo '';
                                      }
                                      ?>
                                    </td>
                                    <td class="text-center"><?=$nilai->nilai_akhir_k;?></td>
                                    <td class="text-center"><?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_k < 70){
                                      echo 'D';
                                    }
                                    elseif($nilai->nilai_akhir_k >= 70 && $nilai->nilai_akhir_k <= 74){
                                      echo 'C';
                                    }
                                    elseif($nilai->nilai_akhir_k >= 75 && $nilai->nilai_akhir_k <= 85){
                                      echo 'B';
                                    }
                                    elseif($nilai->nilai_akhir_k >= 86 && $nilai->nilai_akhir_k <= 100){
                                      echo 'A';
                                    }
                                    else{
                                      echo '';
                                    }
                                    ?></td>
                                  </tr>
                                  <?php $k++;} elseif ($nilai->kelompok_mapel == 3 && $nilai->jurusan == 2) { ?>

                                    <tr>
                                      <td class="text-center"><?=$l;?></td>
                                      <td><?=$nilai->nama_mapel;?></td>
                                      <td class="text-center"><?=$sekolah->kkm;?></td>
                                      <td class="text-center"><?=$nilai->nilai_akhir_p;?></td>
                                      <td class="text-center">
                                        <?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_p < 70 ){
                                          echo 'D';
                                        }
                                        elseif($nilai->nilai_akhir_p >= 70 && $nilai->nilai_akhir_p <= 74){
                                          echo 'C';
                                        }
                                        elseif($nilai->nilai_akhir_p >= 75 && $nilai->nilai_akhir_p <= 85){
                                          echo 'B';
                                        }
                                        elseif($nilai->nilai_akhir_p >= 86 && $nilai->nilai_akhir_p <= 100){
                                          echo 'A';
                                        }
                                        else{
                                          echo '';
                                        }
                                        ?>
                                      </td>
                                      <td class="text-center"><?=$nilai->nilai_akhir_k;?></td>
                                      <td class="text-center"><?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_k < 70){
                                        echo 'D';
                                      }
                                      elseif($nilai->nilai_akhir_k >= 70 && $nilai->nilai_akhir_k <= 74){
                                        echo 'C';
                                      }
                                      elseif($nilai->nilai_akhir_k >= 75 && $nilai->nilai_akhir_k <= 85){
                                        echo 'B';
                                      }
                                      elseif($nilai->nilai_akhir_k >= 86 && $nilai->nilai_akhir_k <= 100){
                                        echo 'A';
                                      }
                                      else{
                                        echo '';
                                      }
                                      ?></td>
                                    </tr>
                                    <?php $l++;}  endforeach;  ?>

                                    <tr class="text-bold">
                                      <td class="text-center">II</td>
                                      <td colspan="6">Lintas Minat</td>
                                    </tr>
                                    <?php $j=1;
                                    foreach ($nilai_mapel as $nilai): 
                                      if($nilai->kelompok_mapel == 4){?>

                                        <tr>
                                          <td class="text-center"><?=$j;?></td>
                                          <td><?=$nilai->nama_mapel;?></td>
                                          <td class="text-center"><?=$sekolah->kkm;?></td>
                                          <td class="text-center"><?=$nilai->nilai_akhir_p;?></td>
                                          <td class="text-center">
                                            <?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_p < 70 ){
                                              echo 'D';
                                            }
                                            elseif($nilai->nilai_akhir_p >= 70 && $nilai->nilai_akhir_p <= 74){
                                              echo 'C';
                                            }
                                            elseif($nilai->nilai_akhir_p >= 75 && $nilai->nilai_akhir_p <= 85){
                                              echo 'B';
                                            }
                                            elseif($nilai->nilai_akhir_p >= 86 && $nilai->nilai_akhir_p <= 100){
                                              echo 'A';
                                            }
                                            else{
                                              echo '';
                                            }
                                            ?>
                                          </td>
                                          <td class="text-center"><?=$nilai->nilai_akhir_k;?></td>
                                          <td class="text-center"><?php if($nilai->nilai_akhir_k > 0 && $nilai->nilai_akhir_k < 70){
                                            echo 'D';
                                          }
                                          elseif($nilai->nilai_akhir_k >= 70 && $nilai->nilai_akhir_k <= 74){
                                            echo 'C';
                                          }
                                          elseif($nilai->nilai_akhir_k >= 75 && $nilai->nilai_akhir_k <= 85){
                                            echo 'B';
                                          }
                                          elseif($nilai->nilai_akhir_k >= 86 && $nilai->nilai_akhir_k <= 100){
                                            echo 'A';
                                          }
                                          else{
                                            echo '';
                                          }
                                          ?></td>
                                        </tr>
                                        <?php $j++;}  endforeach;  ?>
                                      </tbody>
                                    </table>
                                    <h5 class="print-title" style="font-size: 9pt;">
                                      Keterangan
                                    </h5>
                                    <table class="print-section__table" style="width: 300px; font-size: 9pt;">
                                      <thead>
                                        <tr>
                                          <th class="text-center">Skala</th>
                                          <th class="text-center">Predikat</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td class="text-center">86 – 100</td>
                                          <td class="text-center">Sangat Baik (A)</td>
                                        </tr>
                                        <tr>
                                          <td class="text-center">75 – 85</td>
                                          <td class="text-center">Baik (B)</td>
                                        </tr>
                                        <tr>
                                          <td class="text-center">70 – 74</td>
                                          <td class="text-center">Cukup (C)</td>
                                        </tr>
                                        <tr>
                                          <td class="text-center">< 70</td>
                                          <td class="text-center">Kurang (D)</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="page">
                              <div class="print-section">
                                <h5 class="print-title">
                                  <label class="print-title__number">B.</label>
                                  Deskripsi Pengetahuan dan Keterampilan
                                </h5>
                                <div class="print-section__body print-section__body--deskripsi">
                                  <div class="print-section__row">
                                    <table class="print-section__table">
                                      <thead>
                                        <tr>
                                          <th class="text-center" rowspan="2" width="30">No</th>
                                          <th class="text-center" rowspan="2">Mata Pelajaran</th>
                                          <th class="text-center" rowspan="2">Aspek</th>
                                          <th class="text-center" colspan="2">Deskripsi</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr class="text-bold">
                                          <td colspan="4">Kelompok A (Umum)</td>
                                        </tr>
                                        <?php $i=1;
                                        foreach ($nilai_mapel as $nilai): 
                                          if($nilai->kelompok_mapel == 1){?>

                                           <tr>
                                            <td rowspan="2" class="text-center"><?=$i;?></td>
                                            <td rowspan="2"><?=$nilai->nama_mapel;?></td>
                                            <td class="text-center">Pengetahuan</td>
                                            <td class="text-justify"><?=$nilai->deskripsi_nilai_p;?></td>
                                          </tr>
                                          <tr>
                                            <td class="text-center">Keterampilan</td>
                                            <td class="text-justify"><?=$nilai->deskripsi_nilai_k;?></td>
                                          </tr>
                                          <?php $i++;}  endforeach;  ?>

                                          <tr class="text-bold">
                                            <td colspan="7">Kelompok B (Umum)</td>
                                          </tr>
                                          <?php $j=1;
                                          foreach ($nilai_mapel as $nilai): 
                                            if($nilai->kelompok_mapel == 2){?>

                                             <tr>
                                              <td rowspan="2" class="text-center"><?=$j;?></td>
                                              <td rowspan="2"><?=$nilai->nama_mapel;?></td>
                                              <td class="text-center">Pengetahuan</td>
                                              <td class="text-justify"><?=$nilai->deskripsi_nilai_p;?></td>
                                            </tr>
                                            <tr>
                                              <td class="text-center">Keterampilan</td>
                                              <td class="text-justify"><?=$nilai->deskripsi_nilai_k;?></td>
                                            </tr>
                                            <?php $j++;}  endforeach;  ?>

                                            <tr class="text-bold">
                                              <td colspan="7">Kelompok C (Peminatan)</td>
                                            </tr>
                                            <tr class="text-bold">
                                              <td class="text-center">I</td>
                                              <td colspan="6">Peminatan Matematika dan Ilmu Pengetahuan Alam / Peminatan Ilmu Pengetahuan Sosial</td>
                                            </tr>
                                            <?php $k=1;
                                            foreach ($nilai_mapel as $nilai): 
                                              if($nilai->kelompok_mapel == 3 && $nilai->jurusan == 1){?>

                                               <tr>
                                                <td rowspan="2" class="text-center"><?=$k;?></td>
                                                <td rowspan="2"><?=$nilai->nama_mapel;?></td>
                                                <td class="text-center">Pengetahuan</td>
                                                <td class="text-justify"><?=$nilai->deskripsi_nilai_p;?></td>
                                              </tr>
                                              <tr>
                                                <td class="text-center">Keterampilan</td>
                                                <td class="text-justify"><?=$nilai->deskripsi_nilai_k;?></td>
                                              </tr>
                                              <?php $k++;}  endforeach;  ?>
                                              <?php $l=1;
                                              foreach ($nilai_mapel as $nilai): 
                                                if($nilai->kelompok_mapel == 3 && $nilai->jurusan == 2){?>

                                                 <tr>
                                                  <td rowspan="2" class="text-center"><?=$l;?></td>
                                                  <td rowspan="2"><?=$nilai->nama_mapel;?></td>
                                                  <td class="text-center">Pengetahuan</td>
                                                  <td class="text-justify"><?=$nilai->deskripsi_nilai_p;?></td>
                                                </tr>
                                                <tr>
                                                  <td class="text-center">Keterampilan</td>
                                                  <td class="text-justify"><?=$nilai->deskripsi_nilai_k;?></td>
                                                </tr>
                                                <?php $l++;}  endforeach;  ?>

                                                <tr class="text-bold">
                                                  <td class="text-center">II</td>
                                                  <td colspan="6">Lintas Minat</td>
                                                </tr>
                                                <?php $m=1;
                                                foreach ($nilai_mapel as $nilai): 
                                                  if($nilai->kelompok_mapel == 4){?>

                                                   <tr>
                                                    <td rowspan="2" class="text-center"><?=$m;?></td>
                                                    <td rowspan="2"><?=$nilai->nama_mapel;?></td>
                                                    <td class="text-center">Pengetahuan</td>
                                                    <td class="text-justify"><?=$nilai->deskripsi_nilai_p;?></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-center">Keterampilan</td>
                                                    <td class="text-justify"><?=$nilai->deskripsi_nilai_k;?></td>
                                                  </tr>
                                                  <?php $m++;}  endforeach;  ?>

                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="page">
                                        <div class="print-section">
                                          <h5 class="print-title">
                                            <label class="print-title__number">D.</label>
                                            Ekstrakurikuler
                                          </h5>
                                          <div class="print-section__body print-section__body--deskripsi">
                                            <div class="print-section__rowlast">
                                              <table class="print-section__table">
                                                <thead>
                                                  <tr>
                                                    <th class="text-center" rowspan="2" width="30">No</th>
                                                    <th class="text-center" style="width: 100px;" rowspan="2">Kegiatan Ekstrakurikuler</th>
                                                    <th class="text-center" style="width: 70px;" rowspan="2">Nilai</th>
                                                    <th class="text-center" colspan="2">Deskripsi</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td class="text-center">1</td>
                                                    <td><?=$penilaian->ewajib?></td>
                                                    <td class="text-center"><?=$penilaian->predikat_wajib .' ('.$penilaian->nilai_wajib.')'; ?></td>
                                                    <td class="text-justify"><?=$penilaian->deskripsi_wajib?></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-center">2</td>
                                                    <td><?=$penilaian->epilihan?></td>
                                                    <td class="text-center"><?=$penilaian->predikat_pilihan .' ('.$penilaian->nilai_pilihan.')'; ?></td>
                                                    <td class="text-justify"><?=$penilaian->deskripsi_pilihan?></td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="print-section">
                                          <h5 class="print-title">
                                            <label class="print-title__number">E.</label>
                                            Prestasi
                                          </h5>
                                          <div class="print-section__body print-section__body--prestasi">
                                            <div class="print-section__rowlast">
                                              <table class="print-section__table">
                                                <thead>
                                                  <tr>
                                                    <th class="text-center" width="30">No</th>
                                                    <th class="text-center" style="width: 100px;" >Jenis Kegiatan</th>
                                                    <th class="text-center">Keterangan</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <?php $i = 1;
                                                  if($prestasi):
                                                    foreach ($prestasi as $prestasi):
                                                      ?>
                                                      <tr>
                                                        <td class="text-center"><?=$i?></td>
                                                        <td><?=$prestasi->jenis_kegiatan?></td>
                                                        <td class="text-justify"><?=$prestasi->keterangan?></td>
                                                      </tr>
                                                      <?php $i++; endforeach;
                                                      else:?>
                                                        <tr>
                                                          <td class="text-center">1</td>
                                                          <td></td>
                                                          <td></td>
                                                        </tr>
                                                        <tr>
                                                          <td class="text-center">2</td>
                                                          <td></td>
                                                          <td></td>
                                                        </tr>
                                                        <?php
                                                      endif; ?>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="print-section">
                                              <h5 class="print-title">
                                                <label class="print-title__number">F.</label>
                                                Ketidakhadiran
                                              </h5>
                                              <div class="print-section__body print-section__body--absensi">
                                                <div class="print-section__rowlast">
                                                  <table class="print-section__table" id="absensi">
                                                    <thead>
                                                      <tr>
                                                        <th class="text-center" colspan="2">KETIDAKHADIRAN</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td>Sakit</td>
                                                        <td><?=$penilaian->sakit.' hari'?></td>
                                                      </tr>
                                                      <tr>
                                                        <td>Ijin</td>
                                                        <td><?=$penilaian->ijin.' hari'?></td>
                                                      </tr>
                                                      <tr>
                                                        <td>Tanpa Keterangan</td>
                                                        <td><?=$penilaian->alpa.' hari'?></td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="print-section">
                                              <h5 class="print-title">
                                                <label class="print-title__number">G.</label>
                                                Catatan Wali Kelas
                                              </h5>
                                              <div class="print-section__body print-section__body--prestasi">
                                                <div class="print-section__rowlast">
                                                  <table class="print-section__table">
                                                    <tbody>
                                                      <tr>
                                                        <td class="text-center text-bold"><?=$penilaian->catatan?></td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="print-section">
                                              <h5 class="print-title">
                                                <label class="print-title__number">H.</label>
                                                Tanggapan Orang Tua
                                              </h5>
                                              <div class="print-section__body print-section__body--prestasi">
                                                <div class="print-section__rowlast">
                                                  <table class="print-section__table">
                                                    <tbody>
                                                      <tr>
                                                        <td class="tanggapan"></td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="print-footer">
                                              <div class="d-flex">
                                                <div class="left">
                                                  <div>
                                                    <label>Mengetahui:</label>
                                                  </div>
                                                  <div class="ttd">
                                                    <label>Orang Tua / Wali,</label>
                                                  </div>
                                                  <div>
                                                    <label>(...................................)</label>
                                                  </div>
                                                </div>
                                                <div class="right">
                                                  <div>
                                                    <label>Malang, <?=$tanggal?></label>
                                                  </div>
                                                  <div class="ttd">
                                                    <label>Walikelas,</label>
                                                  </div>
                                                  <div>
                                                    <label><?=$penilaian->wali_kelas?></label>
                                                  </div>
                                                  <div>
                                                    <label>NIK. <?=$penilaian->nik?></label>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="bottom">
                                                <div>
                                                  <label>Mengetahui,</label>
                                                </div>
                                                <div class="ttd">
                                                  <label>Kepala <?=$sekolah->nama_sekolah;?></label>
                                                </div>
                                                <div>
                                                  <label style="text-decoration: underline; font-weight: bold;"><?=$sekolah->nama_kepsek;?></label>
                                                </div>
                                                <div>
                                                  <label>NIK. <?=$sekolah->nik_kepsek;?></label>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>

                                </body>

                                </html>