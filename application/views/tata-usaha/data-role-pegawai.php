<?php $this->load->view("header", ["page" => $page]); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Role Pegawai</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Data Role Pegawai</li>
          </ol>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-tambah-role-pegawai"><i class="fas fa-plus"></i> Tambah</button>
            </div>
            <div class="card-body table-responsive">
              <div class="callout callout-success">
                <table class="table table-sm table-borderless">
                  <tbody>
                    <tr style="border-bottom: 1px solid rgba(0,0,0,.125);">
                      <th width="150">Tata Usaha</th>
                      <td>  Role untuk seorang pegawai yang menjadi bagian tata usaha (TU) yang bertugas mengelola seluruh data utama dan pencetakan rapor dalam sistem penilaian hasil belajar siswa.</td>
                    </tr>
                    <tr style="border-bottom: 1px solid rgba(0,0,0,.125);">
                      <th width="100">Guru BK</th>
                      <td> Role untuk seorang pegawai yang menjadi guru bimbingan konseling (BK) yang bertugas mengelola seluruh nilai sikap yang didalamnya termasuk nilai sikap spiritual dan sikap sosial siswa</td>
                    </tr>
                    <tr style="border-bottom: 1px solid rgba(0,0,0,.125);">
                      <th width="100">Guru Mata Pelajaran</th>
                      <td>Role untuk seorang pegawai yang mengajar mata pelajaran yang bertugas mengelola seluruh nilai mata pelajaran yang didalamnya termasuk nilai pengetahuan dan keterampilan siswa</td>
                    </tr>
                    <tr>
                      <th width="100">Kesiswaan</th>
                      <td>Role untuk seorang pegawai yang menjadi bagian kesiswaan yang bertugas mengelola data kegiatan kesiswaan seperti nilai ekstrakurikuler dan rekap prestasi siswa</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <table class="table table-bordered table-striped" data-plugin="datatable">
                <thead>
                  <tr>
                    <th width="10">No</th>
                    <th>NIK</th>
                    <th>Nama Pegawai</th>
                    <th>Role Pegawai</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>
</div>

<div class="modal fade" id="modal-tambah-role-pegawai" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Role Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-tambah-role-pegawai">
        <div class="modal-body">
          <!-- text input -->
          <div class="form-group">
            <label>Nama Pegawai</label>
            <span class="text-danger">*</span>
            <select name="pegawai" class="form-control select2 select2-pegawai set-null" style="width: 100%;" required="">
              <option value="" selected="selected">Pilih item</option>

            </select>
          </div>
          <div class="form-group">
            <label>Role</label>
            <span class="text-danger">*</span>
            <select name="role" class="form-control select2 select2-role set-null" style="width: 100%;" required="">
              <option value="" selected="selected">Pilih item</option>

            </select>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-update-role-pegawai" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Role Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-update-role-pegawai">
        <div class="modal-body">
          <div class="form-group">
            <label>Nama Pegawai</label>
            <span class="text-danger">*</span>
            <select name="pegawai" class="form-control select2 select2-pegawai" id="select2-pegawai" style="width: 100%;" required>
              <option value="">Pilih item</option>

            </select>
          </div>
          <div class="form-group">
            <label>Role</label>
            <span class="text-danger">*</span>
            <select name="role" class="form-control select2 select2-role" id="select2-role" style="width: 100%;" required>
              <option value="">Pilih item</option>

            </select>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-delete-role-pegawai" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Role Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-delete-role-pegawai">
        <div class="modal-body">
          <input type="hidden" name="id" class="form-control">
          Apakah Anda yakin akan menghapus role pegawai?
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
$this->load->view("footer");
?>

<script>
 var datatable;

 $(function(){
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

  datatable = $('[data-plugin=datatable]').DataTable({
    language: {
      searchPlaceholder: 'Search...',
      sSearch: '',
      lengthMenu: '_MENU_ items/page',
      emptyTable: "No data available in table"
    },
    // "rowsGroup": [  
    // 1,2
    // ],
    "ajax": "/RolePegawai/getRolePegawai",
    "columns": [{
      className: "text-center"
    },
    {
      "data": "nik"
    },
    {
      "data": "nama_pegawai"
    },
    {
      "data": "nama_role"
    },
    {
      className: "text-center"
    }
    ],
    "columnDefs": [{
      "searchable": false,
      "orderable": false,
      "targets": 0,
      "render": function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
      }
    },
    {
      "targets": 4,
      "render": function (data, type, row, meta) {
        return '<button type="button" class="btn btn-primary btn-sm mr-1" onclick="update(' +
        row.id_role_pegawai + ')"><i class="fas fa-pencil-alt"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="softdelete(' + row.id_role_pegawai + ')"><i class="fas fa-trash-alt"></i></button>';
      }
    }
    ],
    "order": [[ 0, 'asc' ]]
  });

  // datatable.on( 'order.dt search.dt', function () {
  //   datatable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
  //     cell.innerHTML = i+1;
  //   } );
  // } ).draw();

  $(".select2-pegawai").select2({
    ajax: {
      url: "/Pegawai/getCariPegawai",
      dataType: 'json',
      type: "GET",
      quietMillis: 50,
      processResults: function (res) {
        return {
          results: $.map(res.data, function (item) {
            return {
              text: item.nama_pegawai,
              id: item.id_pegawai
            }
          })
        };
      }
    }
  });


  $(".select2-role").select2({
    ajax: {
      url: "/RolePegawai/getCariRole",
      dataType: 'json',
      type: "GET",
      quietMillis: 50,
      processResults: function (res) {
        return {
          results: $.map(res.data, function (item) {
            return {
              text: item.nama_role,
              id: item.id_role
            }
          })
        };
      }
    }
  });

  $('#form-tambah-role-pegawai').submit(function (e) {
    e.preventDefault();

    $.ajax({
      type: 'POST',
      url: `${base_url}/RolePegawai/TambahRolePegawai`,
      data: $('#form-tambah-role-pegawai').serialize(),
      dataType: 'json'
    })
    .done(function (res) {
      $('.set-null').val('').trigger('change');
      $('#modal-tambah-role-pegawai').modal('hide');
      if (res.success) {
        Toast.fire({
          type: 'success',
          title: res.message,
        });
        datatable.ajax.reload(null, false);
      } else {
        Toast.fire({
          type: 'error',
          title: res.message,
        });
      }
    });
  });

  $('#form-update-role-pegawai').submit(function (e) {
    e.preventDefault();

    $.ajax({
      type: 'POST',
      url: `${base_url}/RolePegawai/UpdateRolePegawai/${$('#form-update-role-pegawai').attr('data-form-id')}`,
      data: $('#form-update-role-pegawai').serialize(),
      dataType: 'json'
    })
    .done(function (res) {
      $('#modal-update-role-pegawai').modal('hide');
      if (res.success) {
        Toast.fire({
          type: 'success',
          title: res.message,
        });
        datatable.ajax.reload(null, false);
      } else {
        Toast.fire({
          type: 'error',
          title: res.message,
        });
      }
    });
  });

  $('#form-delete-role-pegawai').submit(function (e) {
    e.preventDefault();

    $.ajax({
      type: 'POST',
      url: `${base_url}/RolePegawai/deleteRolePegawai/${$('#form-delete-role-pegawai').attr('data-form-id')}`,
      data: $('#form-delete-role-pegawai').serialize(),
      dataType: 'json'
    })
    .done(function (res) {
      $('#modal-delete-role-pegawai').modal('hide');
      if (res.success) {
        Toast.fire({
          type: 'success',
          title: res.message,
        });
        datatable.ajax.reload(null, false);
      } else {
        Toast.fire({
          type: 'error',
          title: res.message,
        });
      }
    });
  });
});

function update(id) {
  $.ajax({
    type: 'POST',
    url: `${base_url}/RolePegawai/getRolePegawai/${id}`,
    data: $('#form-tambah-role-pegawai').serialize(),
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-update-role-pegawai');

      var data = res.data[0];

      form.attr('data-form-id', data.id_role_pegawai)

      var pegawai = new Option(data.nama_pegawai, data.id_pegawai_fk, true, true);
      $('#select2-pegawai').append(pegawai).trigger('change');
      $('#select2-pegawai').trigger({
        type: 'select2:select',
        params: {
          data: {
            text: res.data.nama_pegawai,
            id: res.data.id_pegawai_fk
          }
        }
      });
      
      var role = new Option(data.nama_role, data.id_role_fk, true, true);
      $('#select2-role').append(role).trigger('change');
      $('#select2-role').trigger({
        type: 'select2:select',
        params: {
          data: {
            text: res.data.nama_role,
            id: res.data.id_role_fk
          }
        }
      });

      $('#modal-update-role-pegawai').modal();
    }
  });
}

function softdelete(id) {
  $.ajax({
    type: 'GET',
    url: `${base_url}/RolePegawai/getRolePegawai/${id}`,
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var data = res.data[0];
      var form = $('#form-delete-role-pegawai');
      form.attr('data-form-id', data.id_role_pegawai)
      $('#modal-delete-role-pegawai').modal();
    }
  });
}
</script>