 <?php $this->load->view("header", ["page" => $page]);?>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Siswa</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Data Siswa</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-tambah-siswa"><i class="fas fa-plus"></i> Tambah</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              <table class="table table-bordered table-striped" data-plugin="datatable">
                <thead>
                  <tr>
                    <th width="10" class="text-center">No</th>
                    <th>NIS</th>
                    <th>NISN</th>
                    <th>Nama Siswa</th>
                    <th width="60" class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>

</div>

<div class="modal fade" id="modal-tambah-siswa" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-tambah-siswa">
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-6">
              <label>NIS</label>
              <span class="text-danger">*</span>
              <input type="text" name="nis" data-number-only="true" class="form-control set-null" placeholder="Masukkan NIS" required>
            </div>
            <div class="form-group col-6">
              <label>NISN</label>
              <span class="text-danger">*</span>
              <input type="text" name="nisn" data-number-only="true" class="form-control set-null" placeholder="Masukkan NISN" required>
            </div>
            <div class="form-group col-12">
              <label>Nama Siswa</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_siswa" class="form-control set-null" placeholder="Masukkan Nama Siswa" required>
            </div>
            <!-- <div class="form-group col-6">
              <label>Tempat Lahir</label>
              <span class="text-danger">*</span>
              <input type="text" name="tempat_lahir" class="form-control" placeholder="Masukkan Tempat Lahir" required>
            </div> -->
           <!--  <div class="form-group col-6">
              <label>Tanggal Lahir</label>
              <span class="text-danger">*</span>
              <div class="input-group date" data-target-input="nearest">
                <input type="text" name="tanggal_lahir" class="form-control datetimepicker-input" placeholder="Tanggal Lahir" required />
                <div class="input-group-append">
                  <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                </div>
              </div>
            </div> -->
           <!--  <div class="form-group clearfix col-12">
              <label>Jenis Kelamin</label>
              <span class="text-danger">*</span>
              <br>
              <div class="row">
                <div class="col-5">
                  <div class="icheck-primary d-inline" id="gender" name="gender">
                    <input type="radio" id="addfemale" name="gender" value="0" required>
                    <label for="addfemale">Perempuan
                    </label>
                  </div>
                </div>
                <div class="col-5">
                  <div class="icheck-primary d-inline">
                    <input type="radio" id="addmale" name="gender" value="1">
                    <label for="addmale">Laki-Laki
                    </label>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>


<div class="modal fade" id="modal-update-siswa" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-update-siswa">
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-6">
              <label>NIS</label>
              <span class="text-danger">*</span>
              <input type="text" name="nis" data-number-only="true" class="form-control" placeholder="Masukkan NIS" required>
            </div>
            <div class="form-group col-6">
              <label>NISN</label>
              <span class="text-danger">*</span>
              <input type="text" name="nisn" data-number-only="true" class="form-control" placeholder="Masukkan NISN" required>
            </div>
            <div class="form-group col-12">
              <label>Nama Siswa</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_siswa" class="form-control" placeholder="Masukkan Nama Siswa" required>
            </div>
            <!-- <div class="form-group col-6">
              <label>Tempat Lahir</label>
              <span class="text-danger">*</span>
              <input type="text" name="tempat_lahir" class="form-control" placeholder="Masukkan Tempat Lahir" required>
            </div>
            <div class="form-group col-6">
              <label>Tanggal Lahir</label>
              <span class="text-danger">*</span>
              <div class="input-group date" data-target-input="nearest">
                <input type="text" name="tanggal_lahir" class="form-control datetimepicker-input" placeholder="Tanggal Lahir" required />
                <div class="input-group-append">
                  <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                </div>
              </div>
            </div>
            <div class="form-group clearfix col-12">
              <label>Jenis Kelamin</label>
              <span class="text-danger">*</span>
              <br>
              <div class="row">
                <div class="col-4">
                  <div class="icheck-primary d-inline" id="gender" name="gender">
                    <input type="radio" id="updatefemale" name="gender" value="0" required>
                    <label for="updatefemale">Perempuan
                    </label>
                  </div>
                </div>
                <div class="col-4">
                  <div class="icheck-primary d-inline">
                    <input type="radio" id="updatemale" name="gender" value="1">
                    <label for="updatemale">Laki-Laki
                    </label>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>
<div class="modal fade" id="modal-delete-siswa" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-delete-siswa">
        <div class="modal-body">
          <input type="hidden" name="id" class="form-control">
          Apakah Anda yakin akan menghapus siswa?
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>

<?php
$this->load->view("footer");
?>

<script>
  var datatable;

  $(function(){

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    datatable = $('[data-plugin=datatable]').DataTable({
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
        emptyTable: "No data available in table"
      },
      "ajax": "/Siswa/getSiswa",
      "columns": [{
        className: "text-center"
      },
      {
        "data": "nis"
      },
      {
        "data": "nisn"
      },
      {
        "data": "nama_siswa"
      },
      {
        className: "text-center"
      }
      ],
      "columnDefs": [{
        "targets": 0,
        "render": function (data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      },
      {
        "targets": 4,
        "render": function (data, type, row, meta) {
          return '<button type="button" class="btn btn-primary btn-sm mr-1" onclick="update(' +
          row.id_siswa + ')"><i class="fas fa-pencil-alt"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="softdelete(' +
          row.id_siswa + ')"><i class="fas fa-trash-alt"></i></button>';
        }
      }
      ]

    });

    
    $('#form-tambah-siswa').validate({
      rules: {
        nis : {
          digits: true
        },
        nisn : {
          digits: true
        }
      },
      messages: {
        nis: {
          required:"Kolom ini wajib diisi.",
          digits:"Kolom nis harus berupa angka."
        },
        nisn: {
          required:"Kolom ini wajib diisi.",
          digits:"Kolom nisn harus berupa angka."
        },
        nama_siswa: {
          required: "Kolom ini wajib diisi."
        }
      },
      submitHandler: function (form) {
        var form = $('#form-tambah-siswa');
        $.ajax({
          type: 'POST',
          url: `${base_url}/Siswa/tambahSiswa`,
          data: $('#form-tambah-siswa').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('.set-null').val(null);
          $('#modal-tambah-siswa').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });
          }
        });
      }
    });

    $('#form-update-siswa').validate({
      rules: {
        nis : {
          digits: true
        },
        nisn : {
          digits: true
        }
      },
      messages: {
        nis: {
          required:"Kolom ini wajib diisi.",
          digits:"Kolom nis harus berupa angka."
        },
        nisn: {
          required:"Kolom ini wajib diisi.",
          digits:"Kolom nisn harus berupa angka."
        },
        nama_siswa: {
          required: "Kolom ini wajib diisi."
        }
      },
      submitHandler: function (form) {
        var form = $('#form-update-siswa');
        $.ajax({
          type: 'POST',
          url: `${base_url}/Siswa/updateSiswa/${$('#form-update-siswa').attr('data-form-id')}`,
          data: $('#form-update-siswa').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('#modal-update-siswa').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });
          }
        });
      }
    });

    $('#form-delete-siswa').submit(function (e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: `${base_url}/Siswa/deleteSiswa/${$('#form-delete-siswa').attr('data-form-id')}`,
        data: $('#form-delete-siswa').serialize(),
        dataType: 'json'
      })
      .done(function (res) {
        $('#modal-delete-siswa').modal('hide');
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          datatable.ajax.reload(null, false);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      });
    });
  });
 function update(id) {
  $.ajax({
    type: 'POST',
    url: `${base_url}/Siswa/getSiswa/${id}`,
    data: $('#form-tambah-siswa').serialize(),
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-update-siswa');

      var data = res.data[0];

      form.attr('data-form-id', data.id_siswa)
      form.find('[name=nis]').val(data.nis)
      form.find('[name=nisn]').val(data.nisn)
      form.find('[name=nama_siswa]').val(data.nama_siswa)
        // form.find('[name=tempat_lahir]').val(res.data.tempat_lahir)
        // form.find('[name=tanggal_lahir]').val(res.data.tanggal_lahir)
        // form.find('[name=gender][value='+res.data.gender+']').prop("checked", true)
        $('#modal-update-siswa').modal();
      }
    });
}

function softdelete(id) {
  $.ajax({
    type: 'GET',
    url: `${base_url}/Siswa/getSiswa/${id}`,
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-delete-siswa');

      var data= res.data[0];
      
      form.attr('data-form-id', data.id_siswa)
      $('#modal-delete-siswa').modal();
    }
  });
}
</script>