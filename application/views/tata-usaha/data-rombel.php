 <?php $this->load->view("header", ["page" => $page]);?>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Rombel</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Data Rombel</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-tambah-rombel"><i class="fas fa-plus"></i> Tambah</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              <table class="table table-bordered table-striped" data-plugin="datatable">
                <thead>
                  <tr>
                    <th width="10" class="text-center">No</th>
                    <th>Nama Rombel</th>
                    <th>Tingkatan</th>
                    <th>Jurusan</th>
                    <th>Wali Kelas</th>
                    <th>Tahun Ajaran</th>
                    <th width="60" class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>
</div>

<div class="modal fade" id="modal-tambah-rombel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Rombel</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-tambah-rombel">
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-12">
              <label>Nama Rombel</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_rombel" class="form-control" placeholder="Masukkan Nama Rombel" required>
            </div>
            <div class="form-group clearfix col-7">
              <label>Tingkatan</label>
              <span class="text-danger">*</span>
              <br>
              <div class="row">
                <div class="col-3">
                  <div class="icheck-primary d-inline" id="tingkatan" name="gender">
                    <input type="radio" id="addx" name="tingkatan" value="10" required>
                    <label for="addx">10
                    </label>
                  </div>
                </div>
                <div class="col-3">
                  <div class="icheck-primary d-inline">
                    <input type="radio" id="addxi" name="tingkatan" value="11" required>
                    <label for="addxi">11
                    </label>
                  </div>
                </div>
                <div class="col-3">
                  <div class="icheck-primary d-inline">
                    <input type="radio" id="addxii" name="tingkatan" value="12" required>
                    <label for="addxii">12
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group clearfix col-5">
              <label>Jurusan</label>
              <span class="text-danger">*</span>
              <br>
              <div class="row">
                <div class="col-4">
                  <div class="icheck-primary d-inline" id="jurusan" name="gender">
                    <input type="radio" id="addipa" name="jurusan" value="1" required>
                    <label for="addipa">IPA
                    </label>
                  </div>
                </div>
                <div class="col-4">
                  <div class="icheck-primary d-inline">
                    <input type="radio" id="addips" name="jurusan" value="2">
                    <label for="addips">IPS
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group col-12">
              <label>Wali Kelas</label>
              <span class="text-danger">*</span>
              <select name="walas" class="form-control select2 select2-walas set-null" style="width: 100%;" required="">
                <option value="" selected="selected">Pilih</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>


<div class="modal fade" id="modal-update-rombel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Rombel</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-update-rombel">
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-12">
              <label>Nama Rombel</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_rombel" class="form-control" placeholder="Masukkan Nama Rombel" required>
            </div>
            <div class="form-group clearfix col-7">
              <label>Tingkatan</label>
              <span class="text-danger">*</span>
              <br>
              <div class="row">
                <div class="col-3">
                  <div class="icheck-primary d-inline" id="tingkatan" name="gender">
                    <input type="radio" id="updatex" name="tingkatan" value="10" required>
                    <label for="updatex">10
                    </label>
                  </div>
                </div>
                <div class="col-3">
                  <div class="icheck-primary d-inline">
                    <input type="radio" id="updatexi" name="tingkatan" value="11" required>
                    <label for="updatexi">11
                    </label>
                  </div>
                </div>
                <div class="col-3">
                  <div class="icheck-primary d-inline">
                    <input type="radio" id="updatexii" name="tingkatan" value="12" required>
                    <label for="updatexii">12
                    </label>
                  </div>
                </div>
              </div>
            </div>            
            <div class="form-group clearfix col-5">
              <label>Jurusan</label>
              <span class="text-danger">*</span>
              <br>
              <div class="row">
                <div class="col-4">
                  <div class="icheck-primary d-inline" id="jurusan" name="gender">
                    <input type="radio" id="updateipa" name="jurusan" value="1" required>
                    <label for="updateipa">IPA
                    </label>
                  </div>
                </div>
                <div class="col-4">
                  <div class="icheck-primary d-inline">
                    <input type="radio" id="updateips" name="jurusan" value="2">
                    <label for="updateips">IPS
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group col-12">
              <label>Wali Kelas</label>
              <span class="text-danger">*</span>
              <select name="walas" class="form-control select2 select2-walas" id="select2-walas" style="width: 100%;" required="">
                <option value="" selected="selected">Pilih</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>
<div class="modal fade" id="modal-delete-rombel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Rombel</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-delete-rombel">
        <div class="modal-body">
          <input type="hidden" name="id" class="form-control">
          Apakah Anda yakin akan menghapus rombel?
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>

<?php
$this->load->view("footer");
?>

<script>
  var datatable;

  $(function(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    datatable = $('[data-plugin=datatable]').DataTable({
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
        emptyTable: "No data available in table"
      },
      "ajax": "/Rombel/getRombel",
      "columns": [{
        className: "text-center"
      },
      {
        "data": "nama_rombel"
      },
      {
        "data": "tingkatan"
      },
      {},
      {
        "data": "nama_pegawai"
      },
      {
        "data": "tahun_ajaran"
      },
      {
        className: "text-center"
      }
      ],
      "columnDefs": [{
        "targets": 0,
        "render": function (data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      },
      {
        "targets": 3,
        "render": function (data, type, row, meta) {
          if (row.jurusan == 1) {
            return 'IPA' ;
          }
          else {
            return 'IPS';
          }
        }
      },
      {
        "targets": 6,
        "render": function (data, type, row, meta) {
          return '<button type="button" class="btn btn-primary btn-sm mr-1" onclick="update(' +
          row.id_rombel + ')"><i class="fas fa-pencil-alt"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="softdelete(' +
          row.id_rombel + ')"><i class="fas fa-trash-alt"></i></button>';
        }
      }
      ]

    });

    $(".select2-walas").select2({
      ajax: {
        url: "/Pegawai/getCariPegawai/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_pegawai,
                id: item.id_pegawai
              }
            })
          };
        }
      }
    });

    $('#form-tambah-rombel').submit(function (e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: `${base_url}/Rombel/tambahRombel`,
        data: $('#form-tambah-rombel').serialize(),
        dataType: 'json'
      })
      .done(function (res) {
        $('#form-tambah-rombel')[0].reset();
        $('.set-null').val('').trigger('change');
        $('#modal-tambah-rombel').modal('hide');
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          datatable.ajax.reload(null, false);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      });
    });

    $('#form-update-rombel').submit(function (e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: `${base_url}/Rombel/updateRombel/${$('#form-update-rombel').attr('data-form-id')}`,
        data: $('#form-update-rombel').serialize(),
        dataType: 'json'
      })
      .done(function (res) {
        $('#modal-update-rombel').modal('hide');
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          datatable.ajax.reload(null, false);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      });
    });
    $('#form-delete-rombel').submit(function (e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: `${base_url}/Rombel/deleteRombel/${$('#form-delete-rombel').attr('data-form-id')}`,
        data: $('#form-delete-rombel').serialize(),
        dataType: 'json'
      })
      .done(function (res) {
        $('#modal-delete-rombel').modal('hide');
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          datatable.ajax.reload(null, false);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      });
    });
  });

 function update(id) {
  $.ajax({
    type: 'POST',
    url: `${base_url}/Rombel/getRombel/${id}`,
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-update-rombel');
      var data  = res.data[0];

      form.attr('data-form-id', data.id_rombel)
      form.find('[name=nama_rombel]').val(data.nama_rombel)
      form.find('[name=tingkatan][value='+data.tingkatan+']').prop("checked", true)
      form.find('[name=jurusan][value='+data.jurusan+']').prop("checked", true)

      var pegawai = new Option(data.nama_pegawai, data.id_pegawai_fk, true, true);
      $('#select2-walas').append(pegawai).trigger('change');
      $('#select2-walas').trigger({
        type: 'select2:select',
        params: {
          data: {
            text: data.nama_pegawai,
            id: data.id_pegawai_fk
          }
        }
      });
      $('#modal-update-rombel').modal();
    }
  });
}
function softdelete(id) {
  $.ajax({
    type: 'GET',
    url: `${base_url}/Rombel/getRombel/${id}`,
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-delete-rombel');
      var data = res.data[0];
      form.attr('data-form-id', data.id_rombel)
      $('#modal-delete-rombel').modal();
    }
  });
}
</script>