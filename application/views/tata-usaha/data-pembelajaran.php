 <?php $this->load->view("header", ["page" => $page]);?>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Pembelajaran</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Data Pembelajaran</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <form id="form-simpan-pembelajaran">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="form-group col-3">
                    <label>Pilih Rombel</label>
                    <span class="text-danger">*</span>
                    <select name="rombel"  class="form-control select2 select2-rombel" style="width: 100%;" required="">
                      <option value="" selected="selected">Pilih</option>
                    </select>
                  </div>
                  <div class="form-group col-9">
                    <button type="submit" class="btn btn-success btn-flat float-right"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card" id="show-pembelajaran" hidden>
              <div class="card-body table-responsive">
                <table class="table table-bordered" style="table-layout: fixed;">
                  <thead>
                    <tr>
                      <th style="width: 5%">No</th>
                      <th>Nama Mata Pelajaran</th>
                      <th>Nama Guru Mapel</th>
                    </tr>
                  </thead>
                  <tbody id="mapel">                                              
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </form>
  </section>
  <!-- /.content -->
  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>
</div>

<?php
$this->load->view("footer");
?>

<script>
  $(document).ready(function () {

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $(".select2-rombel").select2({
      ajax: {
        url: "/Rombel/getCariRombel/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                tingkatan: item.tingkatan,
                text: item.nama_rombel,
                id: item.id_rombel
              }
            })
          };
        }
      },
      templateSelection: function (data, container) {          
        $(data.element).attr('data-tingkatan', data.tingkatan);
        return data.text;
      }
    });

    $('.select2-rombel').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var tingkatan = parseInt(data.tingkatan); 
      $.ajax({
        type: 'GET',
        url: "/Pembelajaran/getPembelajaran/"+data.id,
        dataType: 'json'
      })
      .done(function (res) {
        $('#mapel').html("");

        if (res.success) {

          let number = 1;

        // grouping data by kelompok_mapel
        var satu = res.data.filter(function(dt){
          return dt.kelompok_mapel == 1;
        });

        var dua = res.data.filter(function(dt){
          return dt.kelompok_mapel == 2;
        });

        var tiga = res.data.filter(function(dt){
          return dt.kelompok_mapel == 3;
        });

        var empat = res.data.filter(function(dt){
          return dt.kelompok_mapel == 4;
        });
        var lima = res.data.filter(function(dt){
          return dt.kelompok_mapel == 5;
        });

        // creating data array for mapel lintas minat
        var lintas_minat = [{id:'', text:'Pilih salah satu'}];
        empat.forEach(element => {
          lintas_minat.push({
            id: element.id_mapel,
            text: element.nama_mapel
          });
        });
        
        // set data kelompok A to table
        $('#mapel').append('<tr class="table-grey"><td colspan="3">Bimbingan Konseling </td></tr>');
        
        lima.forEach(element => {
          $('#mapel').append('<tr><td>'+ number +'</td><td>'+ element.nama_mapel +'</td><td><select id="' + element.sngkt_mapel.toLowerCase() + '" name="mapel[' + element.id_mapel  + ']" class="form-control select2 select2-guru-bk" style="width: 100%;"></select></td></tr>');
          
          var pegawai = new Option("Pilih salah satu", null, true, true);
          if(element.nama_pegawai && element.id_pegawai){
            pegawai = new Option(element.nama_pegawai, element.id_pegawai, true, true);
          }

          $('#' + element.sngkt_mapel.toLowerCase()).append(pegawai).trigger('change');          
          number++;
        });

        // set data kelompok A to table
        $('#mapel').append('<tr class="table-grey"><td colspan="3">Kelompok A (Umum) </td></tr>');
        
        satu.forEach(element => {
          $('#mapel').append('<tr><td>'+ number +'</td><td>'+ element.nama_mapel +'</td><td><select id="' + element.sngkt_mapel.toLowerCase() + '" name="mapel[' + element.id_mapel  + ']" class="form-control select2 select2-guru-mapel" style="width: 100%;"></select></td></tr>');
          
          var pegawai = new Option("Pilih salah satu", null, true, true);
          if(element.nama_pegawai && element.id_pegawai){
            pegawai = new Option(element.nama_pegawai, element.id_pegawai, true, true);
          }

          $('#' + element.sngkt_mapel.toLowerCase()).append(pegawai).trigger('change');          
          number++;
        });

        // set data kelompok B to table
        $('#mapel').append('<tr class="table-grey"><td colspan="3">Kelompok B (Umum) </td></tr>');
        
        dua.forEach(element => {
          $('#mapel').append('<tr><td>'+ number +'</td><td>'+ element.nama_mapel +'</td><td><select id="' + element.sngkt_mapel.toLowerCase() + '" name="mapel[' + element.id_mapel + ']" class="form-control select2 select2-guru-mapel" style="width: 100%;"></select></td></tr>');

          var pegawai = new Option("Pilih salah satu", null, true, true);
          if(element.nama_pegawai && element.id_pegawai){
            pegawai = new Option(element.nama_pegawai, element.id_pegawai, true, true);
          }

          $('#' + element.sngkt_mapel.toLowerCase()).append(pegawai).trigger('change');
          number++;
        });

        // set data kelompok C peminatan to table
        $('#mapel').append('<tr class="table-grey"><td colspan="3">Kelompok C (Peminatan) </td></tr>');
        
        tiga.forEach(element => {
          $('#mapel').append('<tr><td>'+ number +'</td><td>'+ element.nama_mapel +'</td><td><select id="' + element.sngkt_mapel.toLowerCase() + '" name="mapel[' + element.id_mapel + ']" class="form-control select2 select2-guru-mapel" style="width: 100%;"></select></td></tr>');

          var pegawai = new Option("Pilih salah satu", null, true, true);
          if(element.nama_pegawai && element.id_pegawai){
            pegawai = new Option(element.nama_pegawai, element.id_pegawai, true, true);
          }

          $('#' + element.sngkt_mapel.toLowerCase()).append(pegawai).trigger('change');
          number++;
        });

        // set data kelompok C lintas minat to table
        $('#mapel').append('<tr class="table-grey"><td colspan="3">Kelompok C (Lintas Minat) </td></tr>');  

        var saved_lm = empat.filter(function(dt){
          return dt.nama_pegawai != null && dt.id_pegawai != null;
        });    

        if(saved_lm.length > 0){
          saved_lm.forEach(dt => {
            
            let temp = number;
            
            $('#mapel').append('<tr><td>'+ number +'</td><td><select id="mapel-lm-' + number + '" class="form-control select2 select2-mapel lintas-minat"></select></td><td><select id="guru-lm-' + number + '" name="mapel[' + dt.id_mapel + ']" class="form-control select2 select2-guru-mapel" style="width: 100%;"></select></td></tr>');            
            var mapel = new Option(dt.nama_mapel, dt.id_mapel, true, true);          
            $('#mapel-lm-' + number).append(mapel).trigger('change');           
            
            var pegawai = new Option("Pilih salah satu", null, true, true);
            if(dt.nama_pegawai && dt.id_pegawai){
              pegawai = new Option(dt.nama_pegawai, dt.id_pegawai, true, true);
            }

            $('#guru-lm-' + number).append(pegawai).trigger('change');     
            // $('#guru-lm-' + number).prop('disabled', true);
            $('#mapel-lm-' + temp).prop('disabled', true);  

            $('#mapel-lm-' + temp).on('select2:selecting', function (e) {            
              var data = e.params.args.data;            
              $('#guru-lm-' + temp).attr('name', 'mapel[' + data.id + ']');
            });

            number++;        
          });

          if(saved_lm.length == 1 && tingkatan == 10){
            $('#mapel').append('<tr><td>'+ number +'</td><td><select id="mapel-lm-' + number + '" class="form-control select2 select2-mapel lintas-minat"></select></td><td><select id="guru-lm-' + number + '" name="mapel[]" class="form-control select2 select2-guru-mapel" style="width: 100%;"></select></td></tr>');            
            
            $('#mapel-lm-' + number).on('select2:selecting', function (e) {            
              var data = e.params.args.data;                
              $('#guru-lm-' + number).attr('name', 'mapel[' + data.id + ']');
            });
            
            var pegawai = new Option("Pilih salah satu", null, true, true);          
            $('#guru-lm-' + number).append(pegawai).trigger('change'); 
          }

        }else{
          let temp = number;
          $('#mapel').append('<tr><td>'+ number +'</td><td><select id="mapel-lm-' + number + '" class="form-control select2 select2-mapel lintas-minat"></select></td><td><select id="guru-lm-' + number + '" name="mapel[]" class="form-control select2 select2-guru-mapel" style="width: 100%;"></select></td></tr>');            
          
          $('#mapel-lm-' + temp).on('select2:selecting', function (e) {            
            var data = e.params.args.data;            
            $('#guru-lm-' + temp).attr('name', 'mapel[' + data.id + ']');
          });

          
          var pegawai = new Option("Pilih salah satu", null, true, true);          
          $('#guru-lm-' + temp).append(pegawai).trigger('change'); 
          number++;

          if(tingkatan == 10){
            $('#mapel').append('<tr><td>'+ number +'</td><td><select id="mapel-lm-' + number + '" class="form-control select2 select2-mapel lintas-minat"></select></td><td><select id="guru-lm-' + number + '" name="mapel[]" class="form-control select2 select2-guru-mapel" style="width: 100%;"></select></td></tr>');            
            
            $('#mapel-lm-' + number).on('select2:selecting', function (e) {            
              var data = e.params.args.data;                
              $('#guru-lm-' + number).attr('name', 'mapel[' + data.id + ']');
            });
            
            var pegawai = new Option("Pilih salah satu", null, true, true);          
            $('#guru-lm-' + number).append(pegawai).trigger('change'); 
          }
        }

        $(".select2-guru-bk").select2({
          ajax: {
            url: "/RolePegawai/getCariRolePegawai/2",
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            processResults: function (res) {
              return {
                results: $.map(res.data, function (item) {
                  return {
                    text: item.nama_pegawai,
                    id: item.id_pegawai_fk
                  }
                })
              };
            }
          }
        });

        $(".select2-guru-mapel").select2({
          ajax: {
            url: "/RolePegawai/getCariRolePegawai/3",
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            processResults: function (res) {
              return {
                results: $.map(res.data, function (item) {
                  return {
                    text: item.nama_pegawai,
                    id: item.id_pegawai_fk
                  }
                })
              };
            }
          }
        });

        $(".select2-mapel").select2({
          data: lintas_minat
        });

        $('#show-pembelajaran').attr('hidden',false);
      } else {
        Toast.fire({
          type: 'error',
          title: res.message,
        });
      }
    });
});

 $('#form-simpan-pembelajaran').submit(function (e) {
  e.preventDefault();
  let data = $('#form-simpan-pembelajaran').serializeArray();

  let rombel = data.shift();

  var filled = data.filter(function(dt){
    return dt.value != 'null';
  });

  var pre = filled.reduce(function(acc, ele) {
    var key = ele.name.match(/\[(.*)\]$/)[1];
    acc[key] = ele.value;
    return acc;
  }, {});

  var arr = Object.entries(pre);
  var post_data = [];
  arr.forEach(dt => {
    post_data.push({
      id_rombel_fk: rombel.value,        
      id_mapel_fk: dt[0],
      id_pegawai_fk: dt[1]
    });
  });
    $.ajax({
      type: 'POST',
      url: `${base_url}/Pembelajaran/savePembelajaran/`+rombel.value,
      data: JSON.stringify(post_data),
      dataType: 'json'
    })
    .done(function (res) {      
      if (res.success) {
        Toast.fire({
          type: 'success',
          title: res.message,
        });
        setTimeout(() => {
          location.reload();
        }, 1000);
      } else {
        Toast.fire({
          type: 'error',
          title: res.message,
        });
        setTimeout(() => {
          location.reload();
        }, 1000);
      }
    });
  });
});

</script>