 <?php $this->load->view("header", ["page" => $page]);?>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Ekstrakurikuler</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Data Ekstrakurikuler</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-tambah-ekskul"><i class="fas fa-plus"></i> Tambah</button>
            </div>
            <div class="card-body table-responsive">
              <table class="table table-bordered table-striped" data-plugin="datatable">
                <thead>
                  <tr>
                    <th width="10" class="text-center">No</th>
                    <th>Nama Ekskul</th>
                    <th width="60" class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>

</div>

<div class="modal fade" id="modal-tambah-ekskul" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Ekskul</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-tambah-ekskul">
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-12">
              <label>Nama Ekskul</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_ekskul" class="form-control ekskul" placeholder="Masukkan nama ekskul" required>
            </div>
            
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-update-ekskul" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Ekskul</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-update-ekskul">
        <div class="modal-body">
          <input type="hidden" name="id" class="form-control">
          <div class="row">
            <div class="form-group col-12">
              <label>Nama Ekskul</label>
              <span class="text-danger">*</span>
              <input type="text" name="nama_ekskul" class="form-control" placeholder="Masukkan nama ekskul" required>
            </div>
            
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>
<div class="modal fade" id="modal-delete-ekskul" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Ekskul</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-delete-ekskul">
        <div class="modal-body">
          <input type="hidden" name="id" class="form-control">
          Apakah Anda yakin akan menghapus ekskul ?
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>
<?php
$this->load->view("footer");
?>

<script>
  var datatable;

  $(function(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    datatable = $('[data-plugin=datatable]').DataTable({
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
        emptyTable: "No data available in table"
      },
      "ajax": "/Ekskul/getEkskul",
      "columns": [{
        className: "text-center"
      },
      {
        "data": "nama_ekskul"
      },
      {
        className: "text-center"
      }
      ],
      "columnDefs": [{
        "targets": 0,
        "render": function (data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      },
      {
        "targets": 2,
        "render": function (data, type, row, meta) {
          return '<button type="button" class="btn btn-primary btn-sm mr-1" onclick="update(' +
          row.id_ekskul + ')"><i class="fas fa-pencil-alt"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="softdelete(' +
          row.id_ekskul + ')"><i class="fas fa-trash-alt"></i></button>';
        }
      }
      ]

    });

    $('#form-tambah-ekskul').validate({
      messages: {
        nama_ekskul: {
          required:"Kolom ini wajib diisi."
        }
      },
      submitHandler: function (form) {
        var form = $('#form-tambah-ekskul');
        $.ajax({
          type: 'POST',
          url: `${base_url}/Ekskul/tambahEkskul`,
          data: $('#form-tambah-ekskul').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('.ekskul').val(null);
          $('#modal-tambah-ekskul').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });
          }
        });
      }
    });

    $('#form-update-ekskul').validate({
      messages: {
        nama_ekskul: {
          required:"Kolom ini wajib diisi."
        }
      },
      submitHandler: function (form) {
        var form = $('#form-update-ekskul');
        $.ajax({
          type: 'POST',
          url: `${base_url}/Ekskul/updateEkskul/${$('#form-update-ekskul').attr('data-form-id')}`,
          data: $('#form-update-ekskul').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('#modal-update-ekskul').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });        
          }
        });
      }
    });

    $('#form-delete-ekskul').submit(function (e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: `${base_url}/Ekskul/deleteEkskul/${$('#form-delete-ekskul').attr('data-form-id')}`,
        data: $('#form-delete-ekskul').serialize(),
        dataType: 'json'
      })
      .done(function (res) {
        $('#modal-delete-ekskul').modal('hide');
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          datatable.ajax.reload(null, false);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
      });
    });
  });

  function update(id) {
    $.ajax({
      type: 'POST',
      url: `${base_url}/Ekskul/getEkskul/${id}`,
      data: $('#form-tambah-ekskul').serialize(),
      dataType: 'json'
    })
    .done(function (res) {
      if(res.success){
        var form = $('#form-update-ekskul');
        var data = res.data[0];
        form.attr('data-form-id', data.id_ekskul)
        form.find('[name=nama_ekskul]').val(data.nama_ekskul)
        $('#modal-update-ekskul').modal();
      }
    });
  }

  function softdelete(id) {
    $.ajax({
      type: 'GET',
      url: `${base_url}/Ekskul/getEkskul/${id}`,
      dataType: 'json'
    })
    .done(function (res) {
      if(res.success){
        var form = $('#form-delete-ekskul');
        var data = res.data[0];
        form.attr('data-form-id', data.id_ekskul)
        $('#modal-delete-ekskul').modal();
      }
    });
  }
</script>