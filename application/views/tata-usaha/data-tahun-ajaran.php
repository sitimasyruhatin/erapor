<?php $this->load->view("header", ["page" => $page]); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Tahun Ajaran</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Dashboard</a>
						</li>
						<li class="breadcrumb-item active">Data Tahun Ajaran</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-tambah-tahun-ajaran"><i class="fas fa-plus"></i> Tambah</button>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive">
							<table class="table table-bordered table-striped" data-plugin="datatable">
								<thead>
									<tr>
										<th width="10" class="text-center">No</th>
										<th>Tahun Ajaran</th>

										<th width="60" class="text-center">Status</th>
										<th width="60" class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
		<i class="fas fa-chevron-up"></i>
	</a>

</div>

<div class="modal fade" id="modal-tambah-tahun-ajaran" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Tahun Ajaran</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<form id="form-tambah-tahun-ajaran">
				<div class="modal-body">
					<div class="form-group">
						<label>Tahun Ajaran</label>
						<span class="text-danger">*</span>
						<small class="text-dark">Format: tahun/tahun. Contoh: 2019/2020</small>
						<input type="text" name="tahun_ajaran" class="form-control set-null" placeholder="Masukkan tahun ajaran" required>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-update-tahun-ajaran" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Update Tahun Ajaran</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<form id="form-update-tahun-ajaran">
				<div class="modal-body">
					<input type="hidden" name="id" class="form-control">
					<div class="form-group">
						<label>Tahun Ajaran</label>
						<span class="text-danger">*</span>
						<small class="text-dark">Format: tahun/tahun. Contoh: 2019/2020</small>
						<input type="text" name="tahun_ajaran" id="tahun_ajaran" class="form-control" placeholder="Masukkan tahun ajaran" required>
					</div>
					<div class="form-group clearfix">
						<label>Status</label>
						<span class="text-danger">*</span>
						<br>
						<div class="row">
							<div class="col-3">
								<div class="icheck-primary d-inline">
									<input type="radio" id="active" name="status" value="1" required>
									<label for="active">Aktif
									</label>
								</div>
							</div>
							<div class="col-3">
								<div class="icheck-primary d-inline">
									<input type="radio" id="inactive" name="status" value="0">
									<label for="inactive">Tidak Aktif
									</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-delete-tahun-ajaran" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Hapus Tahun Ajaran</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<form id="form-delete-tahun-ajaran">
				<div class="modal-body">
					<input type="hidden" name="id" class="form-control">
					Apakah Anda yakin akan menghapus tahun ajaran ?
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-danger">Hapus</button>
				</div>
			</form>
		</div>
	</div>
</div>


<?php $this->load->view("footer"); ?>

<script>

	var datatable;

	$(document).ready(function () {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000
		});

		datatable = $('[data-plugin=datatable]').DataTable({
			language: {
				searchPlaceholder: 'Search...',
				sSearch: '',
				lengthMenu: '_MENU_ items/page',
				emptyTable: "No data available in table"
			},
			"ajax": "/TahunAjaran/getTahunAjaran",
			"columns": [{
				className: "text-center"
			},
			{
				"data": "tahun_ajaran"
			},
			{
				className: "text-center"
			},
			{
				className: ""
			}
			],
			"columnDefs": [{
				"targets": 0,
				"render": function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{
				"targets": 2,
				"render": function (data, type, row, meta) {
					if (row.status == 1) {
						return '<div class="badge badge-success">Aktif</div>';
					} else {
						return '<div class="badge badge-secondary">Tidak Aktif</div>';
					}
				}
			},
			{
				"targets": 3,
				"render": function (data, type, row, meta) {
					if (row.status == 1) {
						return '<button type="button" class="btn btn-primary btn-sm mr-1" onclick="update(' +
						row.id_tahun_ajaran + ')"><i class="fas fa-pencil-alt"></i></button>';
					} else{
						return '<button type="button" class="btn btn-primary btn-sm mr-1" onclick="update(' +
						row.id_tahun_ajaran + ')"><i class="fas fa-pencil-alt"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="softdelete(' +
						row.id_tahun_ajaran + ')"><i class="fas fa-trash-alt"></i></button>';
					};
				}
			}
			]

		});

		$('#form-tambah-tahun-ajaran').validate({
			rules: {
				tahun_ajaran : {
					minlength: 9,
					maxlength: 9
				}
			},
			messages: {
				tahun_ajaran: {
					required:"Kolom ini wajib diisi.",
					minlength:"Kolom ini wajib diisi sesuai format.",
					maxlength:"Kolom ini wajib diisi sesuai format."
				}
			},
			submitHandler: function (form) {
				var form = $('#form-tambah-tahun-ajaran');
				$.ajax({
					type: 'POST',
					url: `${base_url}/TahunAjaran/tambahTahunAjaran`,
					data: $('#form-tambah-tahun-ajaran').serialize(),
					dataType: 'json'
				})
				.done(function (res) {
					$('.set-null').val(null);
					$('#modal-tambah-tahun-ajaran').modal('hide');
					if (res.success == true) {
						Toast.fire({
							type: 'success',
							title: res.message,
						});
						datatable.ajax.reload(null, false);
					} else {
						Toast.fire({
							type: 'error',
							title: res.message,
						});
						datatable.ajax.reload(null, false);
					}
				});
			}
		});

		$('#form-update-tahun-ajaran').validate({
			rules: {
				tahun_ajaran : {
					minlength: 9,
					maxlength: 9
				}
			},
			messages: {
				tahun_ajaran: {
					required:"Kolom ini wajib diisi.",
					minlength:"Kolom ini wajib diisi sesuai format.",
					maxlength:"Kolom ini wajib diisi sesuai format."
				}
			},
			submitHandler: function (form) {
				var form = $('#form-update-tahun-ajaran');
				$.ajax({
					type: 'POST',
					url: `${base_url}/TahunAjaran/updateTahunAjaran/${$('#form-update-tahun-ajaran').attr('data-form-id')}`,
					data: $('#form-update-tahun-ajaran').serialize(),
					dataType: 'json'
				})
				.done(function (res) {
					$('#modal-update-tahun-ajaran').modal('hide');
					if (res.success) {
						Toast.fire({
							type: 'success',
							title: res.message,
						});
						datatable.ajax.reload(null, false);
					} else {
						Toast.fire({
							type: 'error',
							title: res.message,
						});
						datatable.ajax.reload(null, false);
					}
				});
			}
		});

		$('#form-delete-tahun-ajaran').submit(function (e) {
			e.preventDefault();

			$.ajax({
				type: 'POST',
				url: `${base_url}/TahunAjaran/deleteTahunAjaran/${$('#form-delete-tahun-ajaran').attr('data-form-id')}`,
				data: $('#form-delete-tahun-ajaran').serialize(),
				dataType: 'json'
			})
			.done(function (res) {
				$('#modal-delete-tahun-ajaran').modal('hide');
				if (res.success) {
					Toast.fire({
						type: 'success',
						title: res.message,
					});
					datatable.ajax.reload(null, false);
				} else {
					Toast.fire({
						type: 'error',
						title: res.message,
					});
					datatable.ajax.reload(null, false);
				}
			});
		});
	});

function update(id) {
	$.ajax({
		type: 'GET',
		url: `${base_url}/TahunAjaran/getTahunAjaran/${id}`,
		dataType: 'json'
	})
	.done(function (res) {
		if(res.success){
			var form = $('#form-update-tahun-ajaran');

			var data = res.data[0];
			form.attr('data-form-id', data.id_tahun_ajaran)
			form.find('[name=tahun_ajaran]').val(data.tahun_ajaran)

			form.find('[name=status][value='+data.status+']').prop("checked", true)
			$('#modal-update-tahun-ajaran').modal();
		}
	});
}

function softdelete(id) {
	$.ajax({
		type: 'GET',
		url: `${base_url}/TahunAjaran/getTahunAjaran/${id}`,
		dataType: 'json'
	})
	.done(function (res) {
		if(res.success){
			var form = $('#form-delete-tahun-ajaran');
			var data = res.data[0];
			form.attr('data-form-id', data.id_tahun_ajaran)
			$('#modal-delete-tahun-ajaran').modal();
		}
	});
}
</script>