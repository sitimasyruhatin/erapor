<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-info elevation-2">
  <!-- Brand Logo -->
  <a href="#" class="brand-link elevation-4">
    <img src="<?php echo base_url()."assets/dist/img/AdminLTELogo.png"?>"
    alt="logoSMABSS"
    class="brand-image img-circle elevation-3"
    style="opacity: .8">
    <span class="brand-text font-weight-light">E-Rapor</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex"> -->
        <!-- <div class="image">
          <img src="<?php echo base_url()."assets/dist/img/user2-160x160.jpg"?>" class="" alt="User Image">
        </div> -->
        <!-- <div class="info"> -->
          <!-- <span href="#" class="d-block" style="text-transform: uppercase; color:#17a2b8;"><b>Dashboard <?php echo $this->session->userdata('nama_role') ?></b></span> -->
          <!-- </div> -->
          <!-- </div> -->

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="<?php echo base_url()?>" class="nav-link <?php if ($page == 'dashboard') {echo 'active';} ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <?php 

          if($this->session->userdata('isTataUsaha')): 

            $this->load->view('menu/menu-tatausaha');

          endif;

          if($this->session->userdata('isGuruBK')): 

            $this->load->view('menu/menu-gurubk');

          endif;

          if($this->session->userdata('isGuruMapel')): 

            $this->load->view('menu/menu-gurumapel');

          endif;
          if($this->session->userdata('isKesiswaan')): 

            $this->load->view('menu/menu-kesiswaan');

          endif;
          if($this->session->userdata('isWaliKelas')): 

            $this->load->view('menu/menu-walikelas');

          endif;

          ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>