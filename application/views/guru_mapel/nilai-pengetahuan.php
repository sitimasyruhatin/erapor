 <?php $this->load->view("header", ["page" => $page]);?>
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Nilai Pengetahuan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Nilai Pengetahuan</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <form id="form-simpan-nilai-p">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="form-group col-3">
                    <label>Rombel</label>
                    <select name="data[rombel]" class="form-control select2 select2-rombel" id="select2-rombel">
                      <option value="" selected>Pilih rombel</option>
                    </select>
                  </div>
                  <div class="form-group col-3">
                    <label>Semester</label>
                    <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt" disabled>
                      <option value="" selected="selected" disabled>Pilih semester</option>
                      <option value="1">1 (Gasal)</option>
                      <option value="2">2 (Genap)</option>
                    </select>
                  </div>
                  <div class="form-group col-3">
                    <label>Mata Pelajaran</label>
                    <select name="data[mapel]" class="form-control select2 select2-mapel" id="select2-mapel" disabled>
                      <option value="" selected disabled>Pilih mapel</option>
                    </select>
                  </div>
                  <div class="form-group col-3">
                    <button type="submit" class="btn btn-flat btn-success float-right"><i class="fa fa-save "></i> Simpan</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12" id="show-nilai-pengetahuan" hidden>
            <div class="card">
              <div class="card-body">
                <div class="callout callout-info bg-light" id="deskripsi-nilai"> 
                  <div class="form-group" id="button-deskripsi">
                  </div>
                  <small>
                    <table class="table table-sm table-borderless">
                      <tbody id="table-deskripsi">

                      </tbody>
                    </table>
                  </small>
                </div>
                <!-- <span><b>Keterangan: </b> NILAI_1 = KUIS <b>|</b> NILAI_2 = ULANGAN HARIAN <b>|</b> NILAI_3 = TES LISAN <b>|</b> NILAI_4 = PENUGASAN</span> -->

                <table id="table-nilai" class="table table-bordered table-striped" data-plugin="datatable" style="table-layout: fixed; word-wrap:break-word;"> 
                  <thead>
                    <tr>
                      <th rowspan="2" style="vertical-align: middle; width: 30px;">No</th>
                      <th rowspan="2" style="vertical-align: middle; width: 150px;">Nama Siswa</th>
                      <th colspan="4" style="text-align: center; width: 240px;">KD 1</th>
                      <th colspan="4" style="text-align: center; width: 240px;">KD 2</th>
                      <th colspan="4" style="text-align: center; width: 240px;">KD 3</th> 
                      <th colspan="4" style="text-align: center; width: 240px;">KD 4</th>
                      <th colspan="4" style="text-align: center; width: 240px;">KD 5</th>
                      <th colspan="4" style="text-align: center; width: 240px;">KD 6</th>

                      <th rowspan="2" style="text-align: center; width: 60px;">PENTS</th>
                      <th rowspan="2" style="text-align: center; width: 60px;">PENAS</th>
                      <th rowspan="2" style="text-align: center; width: 60px;">NILAI AKHIR</th>
                    </tr>
                    <tr>
                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>
                      <th class="bg-info" style="width: 60px;">NILAI_4</th>

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>
                      <th class="bg-info" style="width: 60px;">NILAI_4</th>

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>
                      <th class="bg-info" style="width: 60px;">NILAI_4</th>

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>
                      <th class="bg-info" style="width: 60px;">NILAI_4</th>

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>
                      <th class="bg-info" style="width: 60px;">NILAI_4</th>

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>
                      <th class="bg-info" style="width: 60px;">NILAI_4</th>                      
                    </tr>
                  </thead>
                  <tbody id="table-nilai-pengetahuan">

                  </tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- /.content -->
  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>
</div>


<div class="modal fade" id="modal-update-deskripsi-p" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Deskripsi Nilai Pengetahuan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-simpan-deskripsi-p" class="form-horizontal">
        <div class="modal-body" id="deskripsi-p">

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>

<?php
$this->load->view("footer");
?>

<script>
  $(document).ready(function () {    

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    var rombel, smt, mapel = null;

    $(".select2-rombel").select2({
      ajax: {
        url: "/Rombel/getCariRombelNilai/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_rombel,
                id: item.id_rombel
              }
            })
          };
        }
      }
    });

    $('.select2-rombel').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var id_rombel = data.id;    
      $('#show-nilai-pengetahuan').attr('hidden',"hidden");
      var smt = $('#select2-smt').val("").trigger('change');

      $(".select2-mapel").select2({
        ajax: {
          url: "/Mapel/getCariMapelNilai/"+id_rombel,
          dataType: 'json',
          type: "GET",
          quietMillis: 50,
          processResults: function (res) {
            return {
              results: $.map(res.data, function (item) {
                return {
                  text: item.nama_mapel,
                  id: item.id_mapel
                }
              })
            };
          }
        }
      });

      var id_mapel = $('#select2-mapel').val("").trigger('change');

      $('#select2-smt').attr('disabled',false);        
    });

    $('.select2-smt').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var smt = data.id;
      var id_rombel = $('#select2-rombel').val();
      var id_mapel = $('#select2-mapel').val();
      if(id_rombel && id_mapel) set_nilai(id_rombel, smt, id_mapel);      
      $('#select2-mapel').attr('disabled',false);
    });

    $('.select2-mapel').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var id_mapel = data.id;

      var smt = $('#select2-smt').val();

      $(".select2-rombel").select2({
        ajax: {
          url: "/Rombel/getCariRombelNilai/"+ id_mapel,
          dataType: 'json',
          type: "GET",
          quietMillis: 50,
          processResults: function (res) {
            return {
              results: $.map(res.data, function (item) {
                return {
                  text: item.nama_rombel,
                  id: item.id_rombel
                }
              })
            };
          }
        }
      });
      var id_rombel = $('#select2-rombel').val();

      if(smt && id_rombel) set_nilai(id_rombel, smt, id_mapel);     
      $('#select2-mapel').attr('disabled', true);
      $('#select2-smt').attr('disabled',true);
      $('#show-nilai-pengetahuan').removeAttr('hidden');
    });

    $('#form-simpan-deskripsi-p').submit(function (e) {
      e.preventDefault();

      let form = $('#form-simpan-deskripsi-p').serializeObject();
      let deskripsi = [];

      form.data.predikat.map((item, index) => {
        deskripsi.push({
          id_pembelajaran: form.data.id_pembelajaran[index],
          semester: form.data.semester[index],
          kategori_nilai: form.data.kategori_nilai[index],
          id_format: form.data.id_format[index],
          predikat: form.data.predikat[index],
          deskripsi_nilai: form.data.deskripsi_nilai[index],
        });
      });
      let data = {
        deskripsi
      }

      $.ajax({
        type: 'POST',
        url: `${base_url}/NilaiPengetahuan/saveDeskripsiP/`,
        data: data,
        dataType: 'json'
      })
      .done(function (res) {

        $('#modal-update-deskripsi-p').modal('hide');

        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });

          set_deskripsi(data.deskripsi[0].id_pembelajaran,data.deskripsi[0].semester,data.deskripsi[0].kategori_nilai);
          // setTimeout(() => {
          //   location.reload();
          // }, 1000);

        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
            // setTimeout(() => {
            //   location.reload();
            // }, 1000);
          }
        });
    });

    $('#form-simpan-nilai-p').submit(function (e) {
      e.preventDefault();
      
      $('#select2-mapel').attr('disabled', false);
      $('#select2-smt').attr('disabled',false);

      let form = $('#form-simpan-nilai-p').serializeObject();            
      
      let penilaian = [];

      form.data.id_anggota_rombel.map((item, index) => {
        penilaian.push({
          id_nilai_p: form.data.id_nilai_p[index],
          id_anggota_rombel: form.data.id_anggota_rombel[index],
          id_pembelajaran: form.data.id_pembelajaran[index],
          kd1_kuis: form.data.kd1_kuis[index],
          kd1_uh: form.data.kd1_uh[index],
          kd1_lisan: form.data.kd1_lisan[index],
          kd1_tugas: form.data.kd1_tugas[index], 
          kd2_kuis: form.data.kd2_kuis[index],
          kd2_uh: form.data.kd2_uh[index],
          kd2_lisan: form.data.kd2_lisan[index],
          kd2_tugas: form.data.kd2_tugas[index],
          kd3_kuis: form.data.kd3_kuis[index],
          kd3_uh: form.data.kd3_uh[index],
          kd3_lisan: form.data.kd3_lisan[index],
          kd3_tugas: form.data.kd3_tugas[index],
          kd4_kuis: form.data.kd4_kuis[index],
          kd4_uh: form.data.kd4_uh[index],
          kd4_lisan: form.data.kd4_lisan[index],
          kd4_tugas: form.data.kd4_tugas[index],
          kd5_kuis: form.data.kd5_kuis[index],
          kd5_uh: form.data.kd5_uh[index],
          kd5_lisan: form.data.kd5_lisan[index],
          kd5_tugas: form.data.kd5_tugas[index],
          kd6_kuis: form.data.kd6_kuis[index],
          kd6_uh: form.data.kd6_uh[index],
          kd6_lisan: form.data.kd6_lisan[index],
          kd6_tugas: form.data.kd6_tugas[index],
          pts: form.data.pts[index],
          pas: form.data.pas[index],
          nilai_akhir: form.data.nilai_akhir[index]
        });
      });
      
      let data = {
        rombel: form.data.rombel,
        semester: form.data.semester,
        penilaian
      }

      $.ajax({
        type: 'POST',
        url: '/NilaiPengetahuan/saveNilaiP/',
        data: data,
        dataType: 'json'
      })
      .done(function (res) {                      

        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
                
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
     });

      $('#select2-mapel').attr('disabled', true);
      $('#select2-smt').attr('disabled',true);
    });

    function set_deskripsi(id_pembelajaran,smt,kategori){
      $('#button-deskripsi').html("");  
      $('#button-deskripsi').append('<button type="button" class="btn btn-sm btn-info text-right" onclick = "modalDeskripsi(' + id_pembelajaran + ',' + smt + ',' + 1 +')"> <i class="fas fa-pencil-alt"></i> Deskripsi</button>');  
      $.ajax({
        type: 'GET',
        url: '/NilaiPengetahuan/getDeskripsiP/'+ id_pembelajaran+"/"+smt+"/"+ kategori,
        dataType: 'json'
      })
      .done(function (res) {

        $('#table-deskripsi').html("");
        if(res.success){
          res.data.forEach(element => {

            let deskripsi_nilai = 'Belum ada deskripsi';

            if(element.deskripsi_nilai != null) {deskripsi_nilai = element.deskripsi_nilai;}

            $('#table-deskripsi').append('<tr><th width="10">'+element.predikat+'</th><td>'+deskripsi_nilai+'</td></tr>');
          });
        }
      });
    }


    function set_nilai(rombel, smt, mapel) {        
      $('#table-nilai').DataTable({       
        "autoWidth": false,
        "scrollX": true,        
        destroy: true,
        paging:true,
        searching:true,
        "initComplete": function(settings, json) {
          let data = json.data;
          data.forEach((element,index) => {
            nilai_a(index);
          });          
        },
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
          emptyTable: "No data available in table"
        },
        "ajax": {
          "url" : "/NilaiPengetahuan/getNilaiP/"+rombel+"/"+smt+"/"+mapel,
          "dataSrc": function ( json ) {                  
            set_deskripsi(json.data[0].id_pembelajaran, smt, 1);        
            json.data.forEach((element, index) => {
              let nilai = [];            
              if(element.kd1kuis && element.kd1kuis != 0) nilai.push(element.kd1kuis);
              if(element.kd1uh && element.kd1uh != 0) nilai.push(element.kd1uh);
              if(element.kd1lisan && element.kd1lisan != 0) nilai.push(element.kd1lisan);
              if(element.kd1tugas && element.kd1tugas != 0) nilai.push(element.kd1tugas);
              if(element.kd2kuis && element.kd2kuis != 0) nilai.push(element.kd2kuis);
              if(element.kd2uh && element.kd2uh != 0) nilai.push(element.kd2uh);
              if(element.kd2lisan && element.kd2lisan != 0) nilai.push(element.kd2lisan);
              if(element.kd2tugas && element.kd2tugas != 0) nilai.push(element.kd2tugas);
              if(element.kd3kuis && element.kd3kuis != 0) nilai.push(element.kd3kuis);
              if(element.kd3uh && element.kd3uh != 0) nilai.push(element.kd3uh);
              if(element.kd3lisan && element.kd3lisan != 0) nilai.push(element.kd3lisan);
              if(element.kd3tugas && element.kd3tugas != 0) nilai.push(element.kd3tugas);
              if(element.kd4kuis && element.kd4kuis != 0) nilai.push(element.kd4kuis);
              if(element.kd4uh && element.kd4uh != 0) nilai.push(element.kd4uh);
              if(element.kd4lisan && element.kd4lisan != 0) nilai.push(element.kd4lisan);
              if(element.kd4tugas && element.kd4tugas != 0) nilai.push(element.kd4tugas);
              if(element.kd5kuis && element.kd5kuis != 0) nilai.push(element.kd5kuis);
              if(element.kd5uh && element.kd5uh != 0) nilai.push(element.kd5uh);
              if(element.kd5lisan && element.kd5lisan != 0) nilai.push(element.kd5lisan);
              if(element.kd5tugas && element.kd5tugas != 0) nilai.push(element.kd5tugas);
              if(element.kd6kuis && element.kd6kuis != 0) nilai.push(element.kd6kuis);
              if(element.kd6uh && element.kd6uh != 0) nilai.push(element.kd6uh);
              if(element.kd6lisan && element.kd6lisan != 0) nilai.push(element.kd6lisan);
              if(element.kd6tugas && element.kd6tugas != 0) nilai.push(element.kd6tugas);

              if(element.pts && element.pts != 0) nilai.push(element.pts);
              if(element.pas && element.pas != 0) nilai.push(element.pas);

              const arrSum = arr => arr.reduce((a,b) => a + b, 0) / arr.length;
              element.nilai_akhir = arrSum(nilai);                       
            });            
            return json.data;
          } 
        },
        "columns": [{
          className: "text-center"
          }    
        ],    
        "columnDefs": [{
          "width": '30px',
          "targets": 0,
          "render": function (data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
          }
        },
        {      
          "width": '150px',
          "targets": 1,
          "render": function(data, type, row, meta){
            return '<input class="form-control" hidden type="text" name="data[id_nilai_p][]" value="' + row.id_nilai_p + '"><input class="form-control" hidden type="text" name="data[id_anggota_rombel][]" value="' + row.id_anggota_rombel + '"><input class="form-control" hidden type="text" name="data[id_pembelajaran][]" value="' + row.id_pembelajaran + '"><span>' + row.nama_siswa + '</span>';
          }
        },
        {
          "width": '60px',
          "targets": 2,
          "render": function(data, type, row, meta){
            let value = (row.kd1_kuis != 0)?row.kd1_kuis:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd1_kuis][]" id="kd1kuis' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 3,
          "render": function(data, type, row, meta){
            let value = (row.kd1_uh != 0)?row.kd1_uh:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd1_uh][]" id="kd1uh' + meta.row + '" value="' + value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 4,
          "render": function(data, type, row, meta){
            let value = (row.kd1_lisan != 0)?row.kd1_lisan:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd1_lisan][]" id="kd1lisan' + meta.row + '" value="' + value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 5,
          "render": function(data, type, row, meta){
            let value = (row.kd1_tugas != 0)?row.kd1_tugas:null;            
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd1_tugas][]" id="kd1tugas' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 6,
          "render": function(data, type, row, meta){
            let value = (row.kd2_kuis != 0)?row.kd2_kuis:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd2_kuis][]" id="kd2kuis' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 7,
          "render": function(data, type, row, meta){
            let value = (row.kd2_uh != 0)?row.kd2_uh:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd2_uh][]" id="kd2uh' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 8,
          "render": function(data, type, row, meta){
            let value = (row.kd2_lisan != 0)?row.kd2_lisan:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd2_lisan][]" id="kd2lisan' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 9,
          "render": function(data, type, row, meta){
            let value = (row.kd2_tugas != 0)?row.kd2_tugas:null;            
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd2_tugas][]" id="kd2tugas' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 10,
          "render": function(data, type, row, meta){
            let value = (row.kd3_kuis != 0)?row.kd3_kuis:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd3_kuis][]" id="kd3kuis' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 11,
          "render": function(data, type, row, meta){
            let value = (row.kd3_uh != 0)?row.kd3_uh:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd3_uh][]" id="kd3uh' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 12,
          "render": function(data, type, row, meta){
            let value = (row.kd3_lisan != 0)?row.kd3_lisan:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd3_lisan][]" id="kd3lisan' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 13,
          "render": function(data, type, row, meta){
            let value = (row.kd3_tugas != 0)?row.kd3_tugas:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd3_tugas][]" id="kd3tugas' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 14,
          "render": function(data, type, row, meta){
            let value = (row.kd4_kuis != 0)?row.kd4_kuis:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd4_kuis][]" id="kd4kuis' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 15,
          "render": function(data, type, row, meta){
            let value = (row.kd4_uh != 0)?row.kd4_uh:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd4_uh][]" id="kd4uh' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 16,
          "render": function(data, type, row, meta){
            let value = (row.kd4_lisan != 0)?row.kd4_lisan:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd4_lisan][]" id="kd4lisan' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 17,
          "render": function(data, type, row, meta){
            let value = (row.kd4_tugas != 0)?row.kd4_tugas:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd4_tugas][]" id="kd4tugas' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 18,
          "render": function(data, type, row, meta){
            let value = (row.kd5_kuis != 0)?row.kd5_kuis:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd5_kuis][]" id="kd5kuis' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 19,
          "render": function(data, type, row, meta){
            let value = (row.kd5_uh != 0)?row.kd5_uh:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd5_uh][]" id="kd5uh' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 20,
          "render": function(data, type, row, meta){
            let value = (row.kd5_lisan != 0)?row.kd5_lisan:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd5_lisan][]" id="kd5lisan' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 21,
          "render": function(data, type, row, meta){
            let value = (row.kd5_tugas != 0)?row.kd5_tugas:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd5_tugas][]" id="kd5tugas' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 22,
          "render": function(data, type, row, meta){
            let value = (row.kd6_kuis != 0)?row.kd6_kuis:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd6_kuis][]" id="kd6kuis' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 23,
          "render": function(data, type, row, meta){
            let value = (row.kd6_uh != 0)?row.kd6_uh:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd6_uh][]" id="kd6uh' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 24,
          "render": function(data, type, row, meta){
            let value = (row.kd6_lisan != 0)?row.kd6_lisan:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd6_lisan][]" id="kd6lisan' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 25,
          "render": function(data, type, row, meta){
            let value = (row.kd6_tugas != 0)?row.kd6_tugas:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[kd6_tugas][]" id="kd6tugas' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 26,
          "render": function(data, type, row, meta){
            let value = (row.pts != 0)?row.pts:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[pts][]" id="pts' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 27,
          "render": function(data, type, row, meta){
            let value = (row.pas != 0)?row.pas:null;
            return '<input class="form-control number-only" type="number" onkeyup="nilai_a(' + meta.row + ')" name="data[pas][]" id="pas' + meta.row + '" value="'+ value + '">';
          }
        },
        {      
          "width": '60px',
          "targets": 28,
          "render": function(data, type, row, meta){
            let value = (row.nilai_akhir != 0)?row.nilai_akhir:null;
            return '<input class="form-control number-only" type="number" name="data[nilai_akhir][]" id="nilaiakhir' + meta.row + '" value="' + value + '" readonly>';
          }
        }
        ]
      });
      
      $('.number-only').ForceNumericOnly();
      $('#show-nilai-pengetahuan').attr('hidden',false);     
    }

  });

  function modalDeskripsi(id_pembelajaran,smt, kategori) {

    $('#deskripsi-p').html("");
    $.ajax({
      type: 'GET',
      url: '/NilaiPengetahuan/getDeskripsiP/'+ id_pembelajaran+"/"+smt+"/"+ kategori,
      dataType: 'json'
    })
    .done(function (res) {
      if(res.success){

        var form = $('#form-simpan-deskripsi-p');

        res.data.forEach(element => {

          let deskripsi_nilai = '';

          if(element.deskripsi_nilai != null) {deskripsi_nilai = element.deskripsi_nilai;}

          $('#deskripsi-p').append('<div class="form-group row"><label class="col-1 control-label">'+element.predikat+'</label><div class="col-11"><input type="text" name="data[id_format][]" value="'+element.id_format+'" class="form-control" hidden required><input type="text" name="data[id_pembelajaran][]" value="'+element.id_pembelajaran+'"class="form-control" hidden required><input type="text" name="data[semester][]" value="'+element.smt+'" class="form-control" hidden required><input type="text" name="data[kategori_nilai][]" value="'+element.kategori_nilai+'" class="form-control" hidden required><input type="text" name="data[predikat][]" value="'+element.predikat+'" class="form-control" hidden required><textarea name="data[deskripsi_nilai][]" class="form-control" required="">'+deskripsi_nilai+'</textarea></div></div>');
        });
      }

      $('#modal-update-deskripsi-p').modal();

    });
  }

  function nilai_a(id){
    let nilai = [];            
    if($('#kd1kuis'+id).val() && $('#kd1kuis'+id).val() != 0) nilai.push(parseFloat($('#kd1kuis'+id).val()));
    if($('#kd1uh'+id).val() && $('#kd1uh'+id).val() != 0) nilai.push(parseFloat($('#kd1uh'+id).val()));
    if($('#kd1lisan'+id).val() && $('#kd1lisan'+id).val() != 0) nilai.push(parseFloat($('#kd1lisan'+id).val()));
    if($('#kd1tugas'+id).val() && $('#kd1tugas'+id).val() != 0) nilai.push(parseFloat($('#kd1tugas'+id).val()));
    if($('#kd2kuis'+id).val() && $('#kd2kuis'+id).val() != 0) nilai.push(parseFloat($('#kd2kuis'+id).val()));
    if($('#kd2uh'+id).val() && $('#kd2uh'+id).val() != 0) nilai.push(parseFloat($('#kd2uh'+id).val()));
    if($('#kd2lisan'+id).val() && $('#kd2lisan'+id).val() != 0) nilai.push(parseFloat($('#kd2lisan'+id).val()));
    if($('#kd2tugas'+id).val() && $('#kd2tugas'+id).val() != 0) nilai.push(parseFloat($('#kd2tugas'+id).val()));
    if($('#kd3kuis'+id).val() && $('#kd3kuis'+id).val() != 0) nilai.push(parseFloat($('#kd3kuis'+id).val()));
    if($('#kd3uh'+id).val() && $('#kd3uh'+id).val() != 0) nilai.push(parseFloat($('#kd3uh'+id).val()));
    if($('#kd3lisan'+id).val() && $('#kd3lisan'+id).val() != 0) nilai.push(parseFloat($('#kd3lisan'+id).val()));
    if($('#kd3tugas'+id).val() && $('#kd3tugas'+id).val() != 0) nilai.push(parseFloat($('#kd3tugas'+id).val()));
    if($('#kd4kuis'+id).val() && $('#kd4kuis'+id).val() != 0) nilai.push(parseFloat($('#kd4kuis'+id).val()));
    if($('#kd4uh'+id).val() && $('#kd4uh'+id).val() != 0) nilai.push(parseFloat($('#kd4uh'+id).val()));
    if($('#kd4lisan'+id).val() && $('#kd4lisan'+id).val() != 0) nilai.push(parseFloat($('#kd4lisan'+id).val()));
    if($('#kd4tugas'+id).val() && $('#kd4tugas'+id).val() != 0) nilai.push(parseFloat($('#kd4tugas'+id).val()));
    if($('#kd5kuis'+id).val() && $('#kd5kuis'+id).val() != 0) nilai.push(parseFloat($('#kd5kuis'+id).val()));
    if($('#kd5uh'+id).val() && $('#kd5uh'+id).val() != 0) nilai.push(parseFloat($('#kd5uh'+id).val()));
    if($('#kd5lisan'+id).val() && $('#kd5lisan'+id).val() != 0) nilai.push(parseFloat($('#kd5lisan'+id).val()));
    if($('#kd5tugas'+id).val() && $('#kd5tugas'+id).val() != 0) nilai.push(parseFloat($('#kd5tugas'+id).val()));
    if($('#kd6kuis'+id).val() && $('#kd6kuis'+id).val() != 0) nilai.push(parseFloat($('#kd6kuis'+id).val()));
    if($('#kd6uh'+id).val() && $('#kd6uh'+id).val() != 0) nilai.push(parseFloat($('#kd6uh'+id).val()));
    if($('#kd6lisan'+id).val() && $('#kd6lisan'+id).val() != 0) nilai.push(parseFloat($('#kd6lisan'+id).val()));
    if($('#kd6tugas'+id).val() && $('#kd6tugas'+id).val() != 0) nilai.push(parseFloat($('#kd6tugas'+id).val()));

    if($('#pts'+id).val() && $('#pts'+id).val() != 0) nilai.push(parseFloat($('#pts'+id).val()));
    if($('#pas'+id).val() && $('#pas'+id).val() != 0) nilai.push(parseFloat($('#pas'+id).val()));

    const arrSum = arr => (arr.reduce((a,b) => a + b, 0) / arr.length).toFixed(1);
    $('#nilaiakhir'+id).val(arrSum(nilai));    
  }
</script>