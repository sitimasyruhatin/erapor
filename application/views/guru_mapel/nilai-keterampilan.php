<?php $this->load->view("header", ["page" => $page]);?>
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Nilai Keterampilan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Nilai Keterampilan</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <form id="form-simpan-nilai-k">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="form-group col-3">
                    <label>Rombel</label>
                    <select name="data[rombel]" class="form-control select2 select2-rombel" id="select2-rombel">
                      <option value="" selected>Pilih rombel</option>
                    </select>
                  </div>
                  <div class="form-group col-3">
                    <label>Semester</label>
                    <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt" disabled>
                      <option value="" selected="selected" disabled>Pilih semester</option>
                      <option value="1">1 (Gasal)</option>
                      <option value="2">2 (Genap)</option>
                    </select>
                  </div>
                  <div class="form-group col-3">
                    <label>Mata Pelajaran</label>
                    <select name="data[mapel]" class="form-control select2 select2-mapel" id="select2-mapel" disabled>
                      <option value="" selected disabled>Pilih mapel</option>
                    </select>
                  </div>
                  <div class="form-group col-3">
                    <button type="submit" class="btn btn-flat btn-success float-right"><i class="fa fa-save "></i> Simpan</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12" id="show-nilai-keterampilan" hidden>
            <div class="card">
              <div class="card-body">
                <div class="callout callout-info bg-light" id="deskripsi-nilai"> 
                  <div class="form-group" id="button-deskripsi">
                  </div>
                  <small>
                    <table class="table table-sm table-borderless">
                      <tbody id="table-deskripsi">

                      </tbody>
                    </table>
                  </small>
                </div>
                <!-- <span><b>Keterangan: </b> NILAI_1 = KUIS <b>|</b> NILAI_2 = ULANGAN HARIAN <b>|</b> NILAI_3 = TES LISAN <b>|</b> NILAI_4 = PENUGASAN</span> -->

                <table id="table-nilai" class="table table-bordered table-striped" data-plugin="datatable" style="table-layout: fixed; word-wrap:break-word;"> 
                  <thead>
                    <tr>
                      <th rowspan="2" style="vertical-align: middle; width: 30px;">No</th>
                      <th rowspan="2" style="vertical-align: middle; width: 150px;">Nama Siswa</th>
                      <th colspan="3" style="text-align: center; width: 180px;">KD 1</th>
                      <th colspan="3" style="text-align: center; width: 180px;">KD 2</th>
                      <th colspan="3" style="text-align: center; width: 180px;">KD 3</th> 
                      <th colspan="3" style="text-align: center; width: 180px;">KD 4</th>
                      <th colspan="3" style="text-align: center; width: 180px;">KD 5</th>
                      <th colspan="3" style="text-align: center; width: 180px;">KD 6</th>
                      <th rowspan="2" style="text-align: center; width: 60px;">NILAI AKHIR</th>
                    </tr>
                    <tr>
                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>                      

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>                      

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>                      

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>                      

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>                      

                      <th class="bg-danger" style="width: 60px;">NILAI_1</th>
                      <th class="bg-warning" style="width: 60px;">NILAI_2</th>
                      <th class="bg-success" style="width: 60px;">NILAI_3</th>                                         
                    </tr>
                  </thead>
                  <tbody id="table-nilai-pengetahuan">

                  </tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- /.content -->
  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>
</div>


<div class="modal fade" id="modal-update-deskripsi-p" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Deskripsi Nilai Pengetahuan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-simpan-deskripsi-k" class="form-horizontal">
        <div class="modal-body" id="deskripsi-p">

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>

<?php
$this->load->view("footer");
?>

<script>
  $(document).ready(function () {    

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    console.log(Toast);
    

    var rombel, smt, mapel = null;

    $(".select2-rombel").select2({
      ajax: {
        url: "/Rombel/getCariRombelNilai/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_rombel,
                id: item.id_rombel
              }
            })
          };
        }
      }
    });

    $('.select2-rombel').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var id_rombel = data.id;    
      $('#show-nilai-keterampilan').attr('hidden',"hidden");
      var smt = $('#select2-smt').val("").trigger('change');

      $(".select2-mapel").select2({
        ajax: {
          url: "/Mapel/getCariMapelNilai/"+id_rombel,
          dataType: 'json',
          type: "GET",
          quietMillis: 50,
          processResults: function (res) {
            return {
              results: $.map(res.data, function (item) {
                return {
                  text: item.nama_mapel,
                  id: item.id_mapel
                }
              })
            };
          }
        }
      });

      var id_mapel = $('#select2-mapel').val("").trigger('change');

      $('#select2-smt').attr('disabled',false);        
    });

    $('.select2-smt').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var smt = data.id;
      var id_rombel = $('#select2-rombel').val();
      var id_mapel = $('#select2-mapel').val();
      if(id_rombel && id_mapel) set_nilai(id_rombel, smt, id_mapel);      
      $('#select2-mapel').attr('disabled',false);
    });

    $('.select2-mapel').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var id_mapel = data.id;

      var smt = $('#select2-smt').val();

      $(".select2-rombel").select2({
        ajax: {
          url: "/Rombel/getCariRombelNilai/"+ id_mapel,
          dataType: 'json',
          type: "GET",
          quietMillis: 50,
          processResults: function (res) {
            return {
              results: $.map(res.data, function (item) {
                return {
                  text: item.nama_rombel,
                  id: item.id_rombel
                }
              })
            };
          }
        }
      });
      var id_rombel = $('#select2-rombel').val();

      if(smt && id_rombel) set_nilai(id_rombel, smt, id_mapel);     
      $('#select2-mapel').attr('disabled', true);
      $('#select2-smt').attr('disabled',true);
      $('#show-nilai-keterampilan').removeAttr('hidden');
    });

    $('#form-simpan-deskripsi-k').submit(function (e) {
      e.preventDefault();

      let form = $('#form-simpan-deskripsi-k').serializeObject();
      let deskripsi = [];

      form.data.predikat.map((item, index) => {
        deskripsi.push({
          id_pembelajaran: form.data.id_pembelajaran[index],
          semester: form.data.semester[index],
          kategori_nilai: form.data.kategori_nilai[index],
          id_format: form.data.id_format[index],
          predikat: form.data.predikat[index],
          deskripsi_nilai: form.data.deskripsi_nilai[index],
        });
      });
      let data = {
        deskripsi
      }

      $.ajax({
        type: 'POST',
        url: `${base_url}/NilaiKeterampilan/saveDeskripsiK/`,
        data: data,
        dataType: 'json'
      })
      .done(function (res) {

        $('#modal-update-deskripsi-p').modal('hide');

        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });

          set_deskripsi(data.deskripsi[0].id_pembelajaran,data.deskripsi[0].semester,data.deskripsi[0].kategori_nilai);
          // setTimeout(() => {
          //   location.reload();
          // }, 1000);

        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
            // setTimeout(() => {
            //   location.reload();
            // }, 1000);
          }
        });
    });

    $('#form-simpan-nilai-k').submit(function (e) {
      e.preventDefault();
      
      
      $('#select2-mapel').attr('disabled', false);
      $('#select2-smt').attr('disabled',false);

      let form = $('#form-simpan-nilai-k').serializeObject();            
      
      let penilaian = [];

      form.data.id_anggota_rombel.map((item, index) => {
        penilaian.push({
          id_nilai_ket: form.data.id_nilai_ket[index],
          id_anggota_rombel: form.data.id_anggota_rombel[index],
          id_pembelajaran: form.data.id_pembelajaran[index],
          kd1_praktik: form.data.kd1_praktik[index],
          kd1_produk: form.data.kd1_produk[index],
          kd1_porto: form.data.kd1_porto[index],
          kd2_praktik: form.data.kd2_praktik[index],
          kd2_produk: form.data.kd2_produk[index],
          kd2_porto: form.data.kd2_porto[index],
          kd3_praktik: form.data.kd3_praktik[index],
          kd3_produk: form.data.kd3_produk[index],
          kd3_porto: form.data.kd3_porto[index],
          kd4_praktik: form.data.kd4_praktik[index],
          kd4_produk: form.data.kd4_produk[index],
          kd4_porto: form.data.kd4_porto[index],
          kd5_praktik: form.data.kd5_praktik[index],
          kd5_produk: form.data.kd5_produk[index],
          kd5_porto: form.data.kd5_porto[index],
          kd6_praktik: form.data.kd6_praktik[index],
          kd6_produk: form.data.kd6_produk[index],
          kd6_porto: form.data.kd6_porto[index],
          nilai_akhir: form.data.nilai_akhir[index]
        });
      });      
      let data = {
        rombel: form.data.rombel,
        semester: form.data.semester,
        penilaian
      }

      $.ajax({
        type: 'POST',
        url: '/NilaiKeterampilan/saveNilaiK/',
        data: data,
        dataType: 'json'
      })
      .done(function (res) {                      
        console.log(res);
        
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
                
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
     });

      $('#select2-mapel').attr('disabled', true);
      $('#select2-smt').attr('disabled',true);
    });

    function set_deskripsi(id_pembelajaran,smt,kategori){
      $('#button-deskripsi').html("");  
      $('#button-deskripsi').append('<button type="button" class="btn btn-sm btn-info text-right" onclick = "modalDeskripsi(' + id_pembelajaran + ',' + smt + ',' + 2 +')"> <i class="fas fa-pencil-alt"></i> Deskripsi</button>');  
      $.ajax({
        type: 'GET',
        url: '/NilaiKeterampilan/getDeskripsiK/'+ id_pembelajaran+"/"+smt+"/"+ kategori,
        dataType: 'json'
      })
      .done(function (res) {

        $('#table-deskripsi').html("");
        if(res.success){
          res.data.forEach(element => {

            let deskripsi_nilai = 'Belum ada deskripsi';

            if(element.deskripsi_nilai != null) {deskripsi_nilai = element.deskripsi_nilai;}

            $('#table-deskripsi').append('<tr><th width="10">'+element.predikat+'</th><td>'+deskripsi_nilai+'</td></tr>');
          });
        }
      });
    }


    function set_nilai(rombel, smt, mapel) {        
      $('#table-nilai').DataTable({       
        "autoWidth": false,
        "scrollX": true,        
        destroy: true,
        paging:true,
        searching:true,
        "initComplete": function(settings, json) {
          let data = json.data;
          data.forEach((element,index) => {
            nilai_a(index);
          });          
        },
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
          emptyTable: "No data available in table"
        },
        "ajax": {
          "url" : "/NilaiKeterampilan/getNilaiK/"+rombel+"/"+smt+"/"+mapel,
          "dataSrc": function ( json ) {                  
            set_deskripsi(json.data[0].id_pembelajaran, smt, 2);        
            json.data.forEach((element, index) => {
              let nilai = [];            
              if(element.kd1praktik && element.kd1praktik != 0) nilai.push(element.kd1praktik);
              if(element.kd1produk && element.kd1produk != 0) nilai.push(element.kd1produk);
              if(element.kd1porto && element.kd1porto != 0) nilai.push(element.kd1porto);
              if(element.kd2praktik && element.kd2praktik != 0) nilai.push(element.kd2praktik);
              if(element.kd2produk && element.kd2produk != 0) nilai.push(element.kd2produk);
              if(element.kd2porto && element.kd2porto != 0) nilai.push(element.kd2porto);
              if(element.kd3praktik && element.kd3praktik != 0) nilai.push(element.kd3praktik);
              if(element.kd3produk && element.kd3produk != 0) nilai.push(element.kd3produk);
              if(element.kd3porto && element.kd3porto != 0) nilai.push(element.kd3porto);
              if(element.kd4praktik && element.kd4praktik != 0) nilai.push(element.kd4praktik);
              if(element.kd4produk && element.kd4produk != 0) nilai.push(element.kd4produk);
              if(element.kd4porto && element.kd4porto != 0) nilai.push(element.kd4porto);
              if(element.kd5praktik && element.kd5praktik != 0) nilai.push(element.kd5praktik);
              if(element.kd5produk && element.kd5produk != 0) nilai.push(element.kd5produk);
              if(element.kd5porto && element.kd5porto != 0) nilai.push(element.kd5porto);
              if(element.kd6praktik && element.kd6praktik != 0) nilai.push(element.kd6praktik);
              if(element.kd6produk && element.kd6produk != 0) nilai.push(element.kd6produk);
              if(element.kd6porto && element.kd6porto != 0) nilai.push(element.kd6porto);

              const arrSum = arr => arr.reduce((a,b) => a + b, 0) / arr.length;
              element.nilai_akhir = arrSum(nilai);                       
            });            
            return json.data;
          } 
        },
        "columns": [{
          className: "text-center"
          }    
        ],    
        "columnDefs": [{
          "width": '30px',
          "targets": 0,
          "render": function (data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
          }
        },
        {      
          "width": '150px',
          "targets": 1,
          "render": function(data, type, row, meta){
            return '<input class="form-control" hidden type="text" name="data[id_nilai_ket][]" value="' + row.id_nilai_ket + '">'+'<input class="form-control" hidden type="text" name="data[id_anggota_rombel][]" value="' + row.id_anggota_rombel + '"><input class="form-control" hidden type="text" name="data[id_pembelajaran][]" value="' + row.id_pembelajaran + '"><span>' + row.nama_siswa + '</span>';
          }
        },
        {
          "width": '60px',
          "targets": 2,
          "render": function(data, type, row, meta){
            let value = (row.kd1_praktik != 0)?row.kd1_praktik:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd1_praktik][]" id="kd1praktik' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 3,
          "render": function(data, type, row, meta){
            let value = (row.kd1_produk != 0)?row.kd1_produk:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd1_produk][]" id="kd1produk' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 4,
          "render": function(data, type, row, meta){
            let value = (row.kd1_porto != 0)?row.kd1_porto:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd1_porto][]" id="kd1porto' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 5,
          "render": function(data, type, row, meta){
            let value = (row.kd2_praktik != 0)?row.kd2_praktik:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd2_praktik][]" id="kd2praktik' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 6,
          "render": function(data, type, row, meta){
            let value = (row.kd2_produk != 0)?row.kd2_produk:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd2_produk][]" id="kd2produk' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 7,
          "render": function(data, type, row, meta){
            let value = (row.kd2_porto != 0)?row.kd2_porto:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd2_porto][]" id="kd2porto' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 8,
          "render": function(data, type, row, meta){
            let value = (row.kd3_praktik != 0)?row.kd3_praktik:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd3_praktik][]" id="kd3praktik' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 9,
          "render": function(data, type, row, meta){
            let value = (row.kd3_produk != 0)?row.kd3_produk:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd3_produk][]" id="kd3produk' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 10,
          "render": function(data, type, row, meta){
            let value = (row.kd3_porto != 0)?row.kd3_porto:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd3_porto][]" id="kd3porto' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 11,
          "render": function(data, type, row, meta){
            let value = (row.kd4_praktik != 0)?row.kd4_praktik:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd4_praktik][]" id="kd4praktik' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 12,
          "render": function(data, type, row, meta){
            let value = (row.kd4_produk != 0)?row.kd4_produk:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd4_produk][]" id="kd4produk' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 13,
          "render": function(data, type, row, meta){
            let value = (row.kd4_porto != 0)?row.kd4_porto:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd4_porto][]" id="kd4porto' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 14,
          "render": function(data, type, row, meta){
            let value = (row.kd5_praktik != 0)?row.kd5_praktik:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd5_praktik][]" id="kd5praktik' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 15,
          "render": function(data, type, row, meta){
            let value = (row.kd5_produk != 0)?row.kd5_produk:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd5_produk][]" id="kd5produk' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 16,
          "render": function(data, type, row, meta){
            let value = (row.kd5_porto != 0)?row.kd5_porto:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd5_porto][]" id="kd5porto' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 17,
          "render": function(data, type, row, meta){
            let value = (row.kd6_praktik != 0)?row.kd6_praktik:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd6_praktik][]" id="kd6praktik' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 18,
          "render": function(data, type, row, meta){
            let value = (row.kd6_produk != 0)?row.kd6_produk:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd6_produk][]" id="kd6produk' + meta.row + '" value="'+ value + '">';
          }
        },
        {
          "width": '60px',
          "targets": 19,
          "render": function(data, type, row, meta){
            let value = (row.kd6_porto != 0)?row.kd6_porto:null;
            return '<input class="form-control number-only" type="number" min="0" max="100" onkeyup="nilai_a(' + meta.row +')" name="data[kd6_porto][]" id="kd6porto' + meta.row + '" value="'+ value + '">';
          }
        },
        {      
          "width": '60px',
          "targets": 20,
          "render": function(data, type, row, meta){
            let value = (row.nilai_akhir != 0)?row.nilai_akhir:null;
            return '<input class="form-control number-only" type="number" name="data[nilai_akhir][]" id="nilaiakhir' + meta.row + '" value="' + value + '" readonly>';
          }
        }
        ]
      });
      
      $('.number-only').ForceNumericOnly();
      $('#show-nilai-keterampilan').attr('hidden',false);     
    }

  });

  function modalDeskripsi(id_pembelajaran,smt, kategori) {

    $('#deskripsi-p').html("");
    $.ajax({
      type: 'GET',
      url: '/NilaiKeterampilan/getDeskripsiK/'+ id_pembelajaran+"/"+smt+"/"+ kategori,
      dataType: 'json'
    })
    .done(function (res) {
      if(res.success){

        var form = $('#form-simpan-deskripsi-k');

        res.data.forEach(element => {

          let deskripsi_nilai = '';

          if(element.deskripsi_nilai != null) {deskripsi_nilai = element.deskripsi_nilai;}

          $('#deskripsi-p').append('<div class="form-group row"><label class="col-1 control-label">'+element.predikat+'</label><div class="col-11"><input type="text" name="data[id_format][]" value="'+element.id_format+'" class="form-control" hidden required><input type="text" name="data[id_pembelajaran][]" value="'+element.id_pembelajaran+'"class="form-control" hidden required><input type="text" name="data[semester][]" value="'+element.smt+'" class="form-control" hidden required><input type="text" name="data[kategori_nilai][]" value="'+element.kategori_nilai+'" class="form-control" hidden required><input type="text" name="data[predikat][]" value="'+element.predikat+'" class="form-control" hidden required><textarea name="data[deskripsi_nilai][]" class="form-control" required="">'+deskripsi_nilai+'</textarea></div></div>');
        });
      }

      $('#modal-update-deskripsi-p').modal();

    });
  }

  function nilai_a(id){
    let nilai = [];            
    if($('#kd1praktik'+id).val() && $('#kd1praktik'+id).val() != 0) nilai.push(parseFloat($('#kd1praktik'+id).val()));
    if($('#kd1produk'+id).val() && $('#kd1produk'+id).val() != 0) nilai.push(parseFloat($('#kd1produk'+id).val()));
    if($('#kd1porto'+id).val() && $('#kd1porto'+id).val() != 0) nilai.push(parseFloat($('#kd1porto'+id).val()));
    if($('#kd2praktik'+id).val() && $('#kd2praktik'+id).val() != 0) nilai.push(parseFloat($('#kd2praktik'+id).val()));
    if($('#kd2produk'+id).val() && $('#kd2produk'+id).val() != 0) nilai.push(parseFloat($('#kd2produk'+id).val()));
    if($('#kd2porto'+id).val() && $('#kd2porto'+id).val() != 0) nilai.push(parseFloat($('#kd2porto'+id).val()));
    if($('#kd3praktik'+id).val() && $('#kd3praktik'+id).val() != 0) nilai.push(parseFloat($('#kd3praktik'+id).val()));
    if($('#kd3produk'+id).val() && $('#kd3produk'+id).val() != 0) nilai.push(parseFloat($('#kd3produk'+id).val()));
    if($('#kd3porto'+id).val() && $('#kd3porto'+id).val() != 0) nilai.push(parseFloat($('#kd3porto'+id).val()));
    if($('#kd4praktik'+id).val() && $('#kd4praktik'+id).val() != 0) nilai.push(parseFloat($('#kd4praktik'+id).val()));
    if($('#kd4produk'+id).val() && $('#kd4produk'+id).val() != 0) nilai.push(parseFloat($('#kd4produk'+id).val()));
    if($('#kd4porto'+id).val() && $('#kd4porto'+id).val() != 0) nilai.push(parseFloat($('#kd4porto'+id).val()));
    if($('#kd5praktik'+id).val() && $('#kd5praktik'+id).val() != 0) nilai.push(parseFloat($('#kd5praktik'+id).val()));
    if($('#kd5produk'+id).val() && $('#kd5produk'+id).val() != 0) nilai.push(parseFloat($('#kd5produk'+id).val()));
    if($('#kd5porto'+id).val() && $('#kd5porto'+id).val() != 0) nilai.push(parseFloat($('#kd5porto'+id).val()));
    if($('#kd6praktik'+id).val() && $('#kd6praktik'+id).val() != 0) nilai.push(parseFloat($('#kd6praktik'+id).val()));
    if($('#kd6produk'+id).val() && $('#kd6produk'+id).val() != 0) nilai.push(parseFloat($('#kd6produk'+id).val()));
    if($('#kd6porto'+id).val() && $('#kd6porto'+id).val() != 0) nilai.push(parseFloat($('#kd6porto'+id).val()));

    const arrSum = arr => (arr.reduce((a,b) => a + b, 0) / arr.length).toFixed(1);
    $('#nilaiakhir'+id).val(arrSum(nilai));    
  }
</script>