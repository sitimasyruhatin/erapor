 <?php $this->load->view("header", ["page" => $page]);?>
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Nilai Pengetahuan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Nilai Pengetahuan</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="form-group col-3">
                  <label>Rombel</label>
                  <select name="data[rombel]" class="form-control select2 select2-rombel" id="select2-rombel">
                    <option value="" selected>Pilih rombel</option>
                  </select>
                </div>
                <div class="form-group col-3">
                  <label>Semester</label>
                  <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt" disabled>
                    <option selected="selected" disabled>Pilih semester</option>
                    <option value = "1">1 (Gasal)</option>
                    <option value = "2">2 (Genap)</option>
                  </select>
                </div>
                <div class="form-group col-3">
                  <label>Mata Pelajaran</label>
                  <select name="data[mapel]" class="form-control select2 select2-mapel" id="select2-mapel" disabled>
                    <option value="" selected disabled>Pilih mapel</option>
                  </select>
                </div>
                <div class="form-group col-3">
                  <button type="button" class="btn btn-flat btn-success float-right"><i class="fa fa-save "></i> Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12" id="show-nilai-pengetahuan" hidden>
          <div class="card">
            <div class="card-body">
              <div class="callout callout-info bg-light">
                <div class="form-group" id="button-deskripsi">
                </div>
                <small>
                  <table class="table table-sm table-borderless">
                    <tbody id="table-deskripsi">
                     
                    </tbody>
                  </table>
                </small>
              </div>
              <!-- <span><b>Keterangan: </b> NILAI_1 = KUIS <b>|</b> NILAI_2 = ULANGAN HARIAN <b>|</b> NILAI_3 = TES LISAN <b>|</b> NILAI_4 = PENUGASAN</span> -->

              <table class="table table-bordered table-striped" data-plugin="datatable" style="width:100%"> 
                <thead>
                  <tr>
                    <th rowspan="2" style="vertical-align: middle;">No</th>
                    <th rowspan="2" style="vertical-align: middle;">Nama Siswa</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 1</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 2</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 3</th> 
                    <th colspan="4" width="40" style="text-align: center;">KD 4</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 5</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 6</th>

                    <th rowspan="2">PTS</th>
                    <th rowspan="2">PAS</th>
                    <th rowspan="2">NILAI AKHIR</th>
                  </tr>
                  <tr>
                    <th class="bg-danger">NILAI_1</th>
                    <th class="bg-warning">NILAI_2</th>
                    <th class="bg-success">NILAI_3</th>
                    <th class="bg-info">NILAI_4</th>

                    <th class="bg-danger">NILAI_1</th>
                    <th class="bg-warning">NILAI_2</th>
                    <th class="bg-success">NILAI_3</th>
                    <th class="bg-info">NILAI_4</th>

                    <th class="bg-danger">NILAI_1</th>
                    <th class="bg-warning">NILAI_2</th>
                    <th class="bg-success">NILAI_3</th>
                    <th class="bg-info">NILAI_4</th>

                    <th class="bg-danger">NILAI_1</th>
                    <th class="bg-warning">NILAI_2</th>
                    <th class="bg-success">NILAI_3</th>
                    <th class="bg-info">NILAI_4</th>

                    <th class="bg-danger">NILAI_1</th>
                    <th class="bg-warning">NILAI_2</th>
                    <th class="bg-success">NILAI_3</th>
                    <th class="bg-info">NILAI_4</th>

                    <th class="bg-danger">NILAI_1</th>
                    <th class="bg-warning">NILAI_2</th>
                    <th class="bg-success">NILAI_3</th>
                    <th class="bg-info">NILAI_4</th>
                    <!-- <th>NA</th> -->
                    <!-- <th>PRE</th> -->
                    <!-- <th>DESKRIPSI NILAI</th> -->
                  </tr>
                </thead>
                <tbody id="table-nilai-pengetahuan">

                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>
</div>

<?php
$this->load->view("footer");
?>

<script>
 $(document).ready(function () {

  var datatableNilaiSikap;

  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });


  $(".select2-rombel").select2({
    ajax: {
      url: "/Rombel/getCariRombelNilai/",
      dataType: 'json',
      type: "GET",
      quietMillis: 50,
      processResults: function (res) {
        return {
          results: $.map(res.data, function (item) {
            return {
              text: item.nama_rombel,
              id: item.id_rombel
            }
          })
        };
      }
    }
  });

  $('.select2-rombel').on('select2:selecting', function (e) {    
    var data = e.params.args.data;  
    var id_rombel = data.id;

    var smt = $('#select2-smt').val();

    $(".select2-mapel").select2({
      ajax: {
        url: "/Mapel/getCariMapelNilai/"+id_rombel,
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_mapel,
                id: item.id_mapel
              }
            })
          };
        }
      }
    });

    var id_mapel = $('#select2-mapel').val();

    $('#select2-smt').attr('disabled',false);
    $('#select2-mapel').attr('disabled',false);

    if(smt && id_mapel) set_nilai(id_rombel, smt, id_mapel);     
  });

  $('.select2-smt').on('select2:selecting', function (e) {    
    var data = e.params.args.data;  
    var smt = data.id;
    var id_rombel = $('#select2-rombel').val();
    var id_mapel = $('#select2-mapel').val();
    if(id_rombel && id_mapel) set_nilai(id_rombel, smt, id_mapel);      
  });

  $('.select2-mapel').on('select2:selecting', function (e) {    
    var data = e.params.args.data;  
    var id_mapel = data.id;

    var smt = $('#select2-smt').val();

    $(".select2-rombel").select2({
      ajax: {
        url: "/Rombel/getCariRombelNilai/"+ id_mapel,
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_rombel,
                id: item.id_rombel
              }
            })
          };
        }
      }
    });
    var id_rombel = $('#select2-rombel').val();

    if(smt && id_rombel) set_nilai(id_rombel, smt, id_mapel);     
  });




  $('#form-simpan-deskripsi-p').submit(function (e) {
    e.preventDefault();

    let form = $('#form-simpan-deskripsi-p').serializeObject();
    let deskripsi = [];

    form.data.predikat.map((item, index) => {
      deskripsi.push({
        // id_anggota_rombel: form.data.id_anggota_rombel[index],
        // id_pembelajaran: form.data.id_pembelajaran[index],
        // nilai_spiritual: form.data.nilai_spiritual[index],
        // desc_spiritual: form.data.desc_spiritual[index],

        id_format: form.data.id_format[index],
        id_pembelajaran: form.data.id_pembelajaran[index],
        semester: form.data.semester[index],
        kategori_nilai: form.data.kategori_nilai[index],
        predikat: form.data.predikat[index],
        deskripsi_nilai: form.data.deskripsi_nilai[index],
      });
    });
    let data = {
      rombel: form.data.rombel,
      semester: form.data.semester,
      mapel: form.data.mapel,
      deskripsi
    }

    console.log(form);

    $.ajax({
      type: 'POST',
      url: `${base_url}/NilaiPengetahuan/saveDeskripsiP/`,
      data: data,
      dataType: 'json'
    })
    .done(function (res) {

      $('#modal-update-deskripsi-p').modal('hide');
      if (res.success) {
        Toast.fire({
          type: 'success',
          title: res.message,
        });
        // setTimeout(() => {
        //   location.reload();
        // }, 1000);
          // $('.show-nilai-sikap').html('')
          // $('.select2-rombel').val(form.data.rombel).trigger("change");
          // $('.select2-smt').val(form.data.semester).trigger("change");

          // $('.select2-rombel').val(form.data.id_nilai_sikap[index]);
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
          // setTimeout(() => {
          //   location.reload();
          // }, 1000);
        }
      });
  });

});

 function set_deskripsi(rombel,smt, mapel) {
  $.ajax({
    type: 'GET',
    url: `${base_url}/NilaiPengetahuan/getDeskripsiPengetahuan/`+ rombel+"/"+smt+"/"+ mapel,
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){

      var form = $('#form-simpan-deskripsi-p');

      // console.log(res.data);
      res.data.forEach(element => {

        let deskripsi_nilai = '';

        if(element.deskripsi_nilai != null) {deskripsi_nilai = element.deskripsi_nilai;}

        // $('#deskripsi-p').append('<div class="form-group row"><label class="col-1 control-label">'+element.predikat+'</label><div class="col-11"><input type="text" name="data[id_format][]" value="'+element.id_format+'" class="form-control" hidden required><input type="text" name="data[id_pembelajaran][]" value="'+element.id_pembelajaran+'"class="form-control" hidden required><input type="text" name="data[semester][]" value="'+element.smt+'" class="form-control" hidden required><input type="text" name="data[kategori_nilai][]" value="'+element.kategori_nilai+'" class="form-control" hidden required><input type="text" name="data[predikat][]" value="'+element.predikat+'" class="form-control" hidden required><textarea name="data[deskripsi_nilai][]" class="form-control" required="">'+deskripsi_nilai+'</textarea></div></div>');

        $('#table-deskripsi').append(' <tr><th width="10">'+element.predikat+'<input type="text" name="data[id_format][]" value="'+element.id_format+'" class="form-control" hidden required><input type="text" name="data[id_pembelajaran][]" value="'+element.id_pembelajaran+'"class="form-control" hidden required><input type="text" name="data[semester][]" value="'+element.smt+'" class="form-control" hidden required><input type="text" name="data[kategori_nilai][]" value="'+element.kategori_nilai+'" class="form-control" hidden required><input type="text" name="data[predikat][]" value="'+element.predikat+'" class="form-control" hidden required></th><td><textarea name="data[deskripsi_nilai][]" class="form-control" required="">'+deskripsi_nilai+'</textarea></td></tr>');
      });
    }

   
    $('#modal-update-deskripsi-p').modal();
  });
}

function set_nilai(rombel, smt, mapel) {
  $.ajax({
    type: 'GET',
    url: "/NilaiPengetahuan/getNilaiPengetahuan/"+rombel+"/"+smt+"/"+mapel,
    dataType: 'json'
  })
  .done(function (res) {
    $('#table-nilai-pengetahuan').html("");

    $('#button-deskripsi').html("");

    $('#button-deskripsi').append('<button type="button" class="btn btn-sm btn-info text-right" onclick = "modalDeskripsi('+res.data[0].id_pembelajaran+','+res.data[0].smt+','+1+')"> <i class="fas fa-pencil-alt"></i> Deskripsi</button>');

    if (res.success) {

      let number = 1;

      res.data.forEach(element => {
        let kd1kuis = '';
        let kd1uh = '';
        let kd1lisan = '';
        let kd1tugas = '';
        let kd2kuis = '';
        let kd2uh = '';
        let kd2lisan = '';
        let kd2tugas = '';
        let kd3kuis = '';
        let kd3uh = '';
        let kd3lisan = '';
        let kd3tugas = '';
        let kd4kuis = '';
        let kd4uh = '';
        let kd4lisan = '';
        let kd4tugas = '';
        let kd5kuis = '';
        let kd5uh = '';
        let kd5lisan = '';
        let kd5tugas = '';
        let kd6kuis = '';
        let kd6uh = '';
        let kd6lisan = '';
        let kd6tugas = '';
        let pts='';
        let pas='';
        let nilaiakhir='';

        if(element.kd1kuis != null) {kd1kuis = element.kd1kuis;}
        if(element.kd1uh != null) {kd1uh = element.kd1uh;}
        if(element.kd1lisan != null){kd1lisan = element.kd1lisan;}
        if(element.kd1tugas != null){kd1tugas = element.kd1tugas;}
        if(element.kd2kuis != null) {kd2kuis = element.kd2kuis;}
        if(element.kd2uh != null) {kd2uh = element.kd2uh;}
        if(element.kd2lisan != null){kd2lisan = element.kd2lisan;}
        if(element.kd2tugas != null){kd2tugas = element.kd2tugas;}
        if(element.kd3kuis != null) {kd3kuis = element.kd3kuis;}
        if(element.kd3uh != null) {kd3uh = element.kd3uh;}
        if(element.kd3lisan != null){kd3lisan = element.kd3lisan;}
        if(element.kd3tugas != null){kd3tugas = element.kd3tugas;}
        if(element.kd4kuis != null) {kd4kuis = element.kd4kuis;}
        if(element.kd4uh != null) {kd4uh = element.kd4uh;}
        if(element.kd4lisan != null){kd4lisan = element.kd4lisan;}
        if(element.kd4tugas != null){kd4tugas = element.kd4tugas;}
        if(element.kd5kuis != null) {kd5kuis = element.kd5kuis;}
        if(element.kd5uh != null) {kd5uh = element.kd5uh;}
        if(element.kd5lisan != null){kd5lisan = element.kd5lisan;}
        if(element.kd5tugas != null){kd5tugas = element.kd5tugas;}
        if (element.kd6kuis != null) {kd6kuis = element.kd6kuis;}
        if (element.kd6uh != null) {kd6uh = element.kd6uh;}
        if(element.kd6lisan != null){kd6lisan = element.kd6lisan;}
        if(element.kd6tugas != null){kd6tugas = element.kd6tugas;}
        if(element.pts != null){pts = element.pts;}
        if(element.pas != null){pas = element.pas;}

        if(element.nilaiakhir != null){nilaiakhir = element.nilaiakhir;}

        $('#table-nilai-pengetahuan').append('<tr><td width="30"><input class="form-control" hidden type="text" name="data[id_nilai_sikap][]" value="'+element.id_nilai_sikap+'"><input class="form-control" hidden type="text" name="data[id_anggota_rombel][]" value="'+element.id_anggota_rombel+'"><input class="form-control" hidden type="text" name="data[id_pembelajaran][]" value="'+element.id_pembelajaran+'">' + number++ + '</td><td>' + element.nama_siswa + ' </td><td><input class="form-control" type="text" name="data[kd1kuis][]" value="'+ kd1kuis + '"></td><td><input class="form-control" type="text" name="data[kd1uh][]" value="'+ kd1uh + '"></td><td><input class="form-control" type="text" name="data[kd1lisan][]" value="'+ kd1lisan + '"></td><td><input class="form-control" type="text" name="data[kd1tugas][]" value="'+ kd1tugas + '"></td><td><input class="form-control" type="text" name="data[kd2kuis][]" value="'+ kd2kuis + '"></td><td><input class="form-control" type="text" name="data[kd2uh][]" value="'+ kd2uh + '"></td><td><input class="form-control" type="text" name="data[kd2lisan][]" value="'+ kd2lisan + '"></td><td><input class="form-control" type="text" name="data[kd2tugas][]" value="'+ kd2tugas + '"></td><td><input class="form-control" type="text" name="data[kd3kuis][]" value="'+ kd3kuis + '"></td><td><input class="form-control" type="text" name="data[kd3uh][]" value="'+ kd3uh + '"></td><td><input class="form-control" type="text" name="data[kd3lisan][]" value="'+ kd3lisan + '"></td><td><input class="form-control" type="text" name="data[kd3tugas][]" value="'+ kd3tugas + '"></td><td><input class="form-control" type="text" name="data[kd4kuis][]" value="'+ kd4kuis + '"></td><td><input class="form-control" type="text" name="data[kd4uh][]" value="'+ kd4uh + '"></td><td> <input class="form-control" type="text" name="data[kd4lisan][]"value="'+ kd4lisan + '"></td><td><input class="form-control" type="text" name="data[kd4tugas][]" value="'+ kd4tugas + '"></td><td><input class="form-control" type="text" name="data[kd5kuis][]" value="'+ kd5kuis + '"></td><td><input class="form-control" type="text" name="data[kd5uh][]" value="'+ kd5uh + '"></td><td><input class="form-control" type="text" name="data[kd5lisan][]" value="'+ kd5lisan + '"></td><td><input class="form-control" type="text" name="data[kd5tugas][]" value="'+ kd5tugas + '"></td><td><input class="form-control" type="text" name="data[kd6kuis][]" value="'+ kd6kuis + '"></td><td><input class="form-control" type="text" name="data[kd6uh][]" value="'+ kd6uh + '"></td><td><input class="form-control" type="text" name="data[kd6lisan][]" value="'+ kd6lisan + '"></td><td><input class="form-control" type="text" name="data[kd6tugas][]" value="'+ kd6tugas + '"></td><td><input class="form-control" type="text" name="data[pts][]" value="'+ pts + '"></td><td><input class="form-control" type="text" name="data[pas][]" value="'+ pas +'"></td><td><input class="form-control" type="text" name="data[nilaiakhir][]" value="'+ nilaiakhir + '" disabled=""></td></tr>');
      });
 
 $('#show-nilai-pengetahuan').attr('hidden',false);
} else {
  Toast.fire({
    type: 'error',
    title: res.message,
  });
  $('#table-nilai-pengetahuan').append(
    '<tr><td colspan="6" align="middle">No data available in table</td></tr>')
}
});
}


</script>