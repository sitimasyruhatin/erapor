  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Guru Mata Pelajaran</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <h4>NILAI PENGETAHUAN</h1>
      <div class="row">
        <?php  for ($i=1; $i < 6 ; $i++) { 
        ?>
        <a href="nilai_pengetahuan">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon" style="font-size: 30px;">5%</span>
            <div class="info-box-content">
              <span class="info-box-text">NILAI MATEMATIKA</span>
              <span class="info-box-number">X IPA <?php echo $i ?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
              <span class="progress-description">
                1/40 Nilai
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </a>
        <?php
        }
      ?>
    </div>
    <h4>NILAI KETERAMPILAN</h1>
      <div class="row">
        <?php  for ($i=1; $i < 6 ; $i++) { 
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon" style="font-size: 30px;">5%</span>
            <div class="info-box-content">
              <span class="info-box-text">NILAI BIOLOGI</span>
              <span class="info-box-number">X IPA <?php echo $i ?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
              <span class="progress-description">
                1/40 Nilai
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <?php
        }
      ?>
    </div>
   
  </section>
</div>
  <!-- /.content-wrapper -->