 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Nilai Keterampilan Fisika X IPA 3
      <button class="btn btn-default btn-flat">Nilai Pengetahuan Fisika X IPA 3 </button>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Nilai Keterampilan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
         <div class="box-header with-border">
          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-flat btn-info"> Simpan</button>
              <button type="button" class="btn btn-flat btn-danger" data-toggle="modal" data-target="#reset-nilai-pengetahuan">Reset</button>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">

              <div class="bg-gray callout" style="margin-top: 1%; padding-right: 15px">
               <button type="button" class="btn btn-xs btn-flat btn-warning pull-right" data-toggle="modal" data-target="#edit-deskripsi-k">Edit</button><h4>Deskripsi Keterampilan </h4>

               <p>
                A = Sangat berkompeten dalam menganalisis pelanggaran hak asasi manusia dalam perspektif pancasila dalam kehidupan berbangsa dan bernegara<br>
                B = Sangat berkompeten dalam menganalisis pelanggaran hak asasi manusia dalam perspektif pancasila dalam kehidupan berbangsa dan bernegara<br>
                C = Berkompeten dalam menganalisis pelanggaran HAM dalam perspektif pancasila, namun perlu ditingkatkan dalam mendeskripsikan sistem hukum dan peradilan di Indonesia sesuai dengan UUD NRI Tahun 1945 <br>
                D = Cukup berkompeten  dalam menganalisis pelanggaran hak asasi manusia dalam perspektif pancasila dalam kehidupan berbangsa dan bernegara, namun perlu ditingkatkan dalam mendeskripsikan sistem hukum dan peradilan di Indonesia sesuai dengan Undang-Undang Dasar Negara Republik Indonesia Tahun 1945<br>
              </p>
            </div>
            <span><b>Keterangan: </b> KS = KUIS <b>|</b> UH = ULANGAN HARIAN <b>|</b> TL = TES LISAN <b>|</b> PS = PENUGASAN</span>
          </div>
        </div>
      </div>


      <!-- /.box-header -->
      <div class="box-body table-responsive">

       <table id="data-keterampilan" class="table table-bordered table-striped" style="width: 100%;">
        <thead>
          <tr>
            <th rowspan="2" width="1%" >No</th>
            <th rowspan="2" >Nama Siswa</th>
            <th colspan="4" style="text-align: center;">KD 1</th>
            <th colspan="4" style="text-align: center;">KD 2</th>
            <th colspan="4" style="text-align: center;">KD 3</th>
            <th colspan="4" style="text-align: center;">KD 4</th>
            <th colspan="4" style="text-align: center;">KD 5</th>
            <th colspan="4" style="text-align: center;">KD 6</th>
            <th colspan="3">NILAI RAPOR</th>
          </tr>
          <tr>
            <th>KZ&nbsp</th>
            <th>UH</th>
            <th>TL&nbsp</th>
            <th>PS&nbsp</th>
            <th>KS&nbsp</th>
            <th>UH</th>
            <th>TL&nbsp</th>
            <th>PS&nbsp</th>
            <th>KS&nbsp</th>
            <th>UH</th>
            <th>TL&nbsp</th>
            <th>PS&nbsp</th>
            <th>KS&nbsp</th>
            <th>UH</th>
            <th>TL&nbsp</th>
            <th>PS&nbsp</th>
            <th>KS&nbsp</th>
            <th>UH</th>
            <th>TL&nbsp</th>
            <th>PS&nbsp</th>
            <th>KS&nbsp</th>
            <th>UH</th>
            <th>TL&nbsp</th>
            <th>PZ&nbsp</th>
            <th>Nilai</th>
            <th>Predikat</th>
            <th>Deskripsi</th>
          </tr>
        </thead>
        <tbody>
          <?php for ($i=0; $i < 35 ; $i++) { 
            ?>
            <tr>
              <td>1</td>
              <td>Muhammad Aditya Irwandana</td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>

              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>

              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>

              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>

              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>

              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name=""></td>

              <td><input style="width: 100%" class="form-control" type="text" name="" disabled=""></td>
              <td><input style="width: 100%" class="form-control" type="text" name="" disabled=""></td>
              <td><textarea class="form-control" disabled=""></textarea></td>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.modal -->
<div class="modal fade" id="import-nilai-pengetahuan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Import Nilai</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
           <form role="form">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Pilih File</label>
                  <input type="file" class="form-control">
                </div>
                <a href="#"><label>Download template file <i class="fa fa-download"> </i></label></a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-flat btn-success">Import</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="reset-nilai-pengetahuan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Reset Data</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <h5>Apakah Anda yakin akan menghapus seluruh catatan wali kelas X IPA 1?</h5>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-flat btn-danger">Hapus</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="edit-deskripsi-k">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Deskripsi</h4>
          </div>
          <div class="modal-body">
            <div class="box-body">
              <form role="form" class="form-horizontal">
                <div class="row">
                  <div class="col-md-12">
                   <div class="form-group">
                    <label for="inputEmail3" class="col-sm-1 control-label">A</label>
                    <div class="col-sm-11">
                      <textarea type="text" class="form-control" id="inputA" placeholder="Deskripsi predikat A"></textarea> 
                    </div>

                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-1 control-label">B</label>
                    <div class="col-sm-11">
                      <textarea type="text" class="form-control" id="inputB" placeholder="Deskripsi predikat B"></textarea> 
                    </div>

                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-1 control-label">C</label>
                    <div class="col-sm-11">
                      <textarea type="text" class="form-control" id="inputC" placeholder="Deskripsi predikat C"></textarea> 
                    </div>

                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-1 control-label">D</label>
                    <div class="col-sm-11">
                      <textarea type="text" class="form-control" id="inputD" placeholder="Deskripsi predikat D"></textarea> 
                    </div>

                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-flat btn-info">Simpan</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

