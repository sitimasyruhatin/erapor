 <?php $this->load->view("header", ["page" => $page]);?>
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Nilai Pengetahuan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Nilai Pengetahuan</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="form-group col-3">
                  <label>Kelas</label>
                  <select class="form-control select2" style="width: 100%;">
                    <option selected="selected" disabled>Pilih kelas</option>
                    <option>X-1</option>
                    <option>X-2</option>
                    <option>X-3</option>
                    <option>X-4</option>
                    <option>X-5</option>
                    <option>X-6</option>
                  </select>
                </div>
                <div class="form-group col-3">
                  <label>Semester</label>
                  <select class="form-control select2" style="width: 100%;">
                    <option selected="selected" disabled>Pilih semester</option>
                    <option>1 (Genap)</option>
                    <option>2 (Ganjil)</option>
                  </select>
                </div>
                <div class="form-group col-3">
                  <label>Mapel</label>
                  <select class="form-control select2" style="width: 100%;">
                    <option selected="selected" disabled>Pilih mapel</option>
                    <option>Bahasa Indonesia</option>
                    <option>Matematika</option>
                    <option>Seni Budaya</option>
                  </select>
                </div>
                <div class="form-group col-3">
                  <button type="button " class="btn btn-flat btn-success float-right " data-toggle="modal" data-target="#tambah-kelas"><i class="fa fa-save "></i> Simpan</button>
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="callout callout-info bg-light">
                <div class="form-group" id="button-deskripsi"><button type="button" class="btn btn-sm btn-info text-right" onclick="modalDeskripsi(5,1,1)"> <i class="fas fa-pencil-alt"></i> Deskripsi</button></div>
                <small>
                  <table class="table table-sm table-borderless">
                    <tbody id="table-deskripsi"><tr><th width="10">A</th><td>Berkompeten dalam menjelaskan nilai-nilai pancasila dalam kerangka praktik penyelenggaraan pemerintahan Negara, namun perlu ditingkatkan dalam menjelaskan faktor-faktor pembentuk integrasi nasional</td></tr><tr><th width="10">B</th><td>Berkompeten dalam menjelaskan nilai-nilai pancasila dalam kerangka praktik penyelenggaraan pemerintahan Negara, namun perlu ditingkatkan dalam menjelaskan faktor-faktor pembentuk integrasi nasional</td></tr><tr><th width="10">C</th><td>Berkompeten dalam menjelaskan nilai-nilai pancasila dalam kerangka praktik penyelenggaraan pemerintahan Negara, namun perlu ditingkatkan dalam menjelaskan faktor-faktor pembentuk integrasi nasional</td></tr><tr><th width="10">D</th><td>Berkompeten dalam menjelaskan nilai-nilai pancasila dalam kerangka praktik penyelenggaraan pemerintahan Negara, namun perlu ditingkatkan dalam menjelaskan faktor-faktor pembentuk integrasi nasional</td></tr></tbody>
                  </table>
                </small>
              </div>
              <table class="table table-bordered table-striped" data-plugin="datatable" style="width:100%"> 
                <thead>
                  <tr>
                    <th rowspan="2" style="vertical-align: middle;">No</th>
                    <th rowspan="2" style="vertical-align: middle;">Nama Siswa</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 1</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 2</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 3</th> 
                    <th colspan="4" width="40" style="text-align: center;">KD 4</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 5</th>
                    <th colspan="4" width="40" style="text-align: center;">KD 6</th>

                    <th rowspan="2">PTS</th>
                    <th rowspan="2">PAS</th>
                    <th rowspan="2">NILAI RAPOR</th>
                  </tr>
                  <tr>
                    <th class="bg-danger">KUIS</th>
                    <th class="bg-warning">UH</th>
                    <th class="bg-success">LISAN</th>
                    <th class="bg-success">TUGAS</th>

                    <th class="bg-danger">KUIS</th>
                    <th class="bg-warning">UH</th>
                    <th class="bg-success">LISAN</th>
                    <th class="bg-success">TUGAS</th>

                    <th class="bg-danger">KUIS</th>
                    <th class="bg-warning">UH</th>
                    <th class="bg-success">LISAN</th>
                    <th class="bg-success">TUGAS</th>

                    <th class="bg-danger">KUIS</th>
                    <th class="bg-warning">UH</th>
                    <th class="bg-success">LISAN</th>
                    <th class="bg-success">TUGAS</th>

                    <th class="bg-danger">KUIS</th>
                    <th class="bg-warning">UH</th>
                    <th class="bg-success">LISAN</th>
                    <th class="bg-success">TUGAS</th>
                    
                    <th class="bg-danger">KUIS</th>
                    <th class="bg-warning">UH</th>
                    <th class="bg-success">LISAN</th>
                    <th class="bg-success">TUGAS</th>
                    <!-- <th>TOTAL</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php for ($i=1; $i < 10 ; $i++) { 
                    ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td style="min-width: 100px;">Ahmad Faiz Hidayat</td>
                      <td><input style="min-width: 50px;" class="form-control" type="text" name=""></td>
                      <td><input style="min-width: 50px;" class="form-control" type="text" name=""></td>
                      <td><input style="min-width: 50px;" class="form-control" type="text" name=""></td>

                      <td><input  style="min-width: 50px;" class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>

                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>

                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>

                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>

                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>


                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>

                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;"class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;" class="form-control" type="text" name=""></td>
                      <td><input  style="min-width: 50px;" class="form-control" type="text" name=""></td>

                      <td><input  style="min-width: 50px;"class="form-control" type="text" name="" disabled=""></td>
                    <?php } ?>
                  </tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>
  </div>

  <div class="modal fade" id="modal-tambah-siswa" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Siswa</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form id="form-tambah-siswa">
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-6">
                <label>NIS</label>
                <span class="text-danger">*</span>
                <input type="text" name="nis" class="form-control" placeholder="Masukkan NIS" required>
              </div>
              <div class="form-group col-6">
                <label>NISN</label>
                <span class="text-danger">*</span>
                <input type="text" name="nisn" class="form-control" placeholder="Masukkan NISN" required>
              </div>
              <div class="form-group col-12">
                <label>Nama Siswa</label>
                <span class="text-danger">*</span>
                <input type="text" name="nama_siswa" class="form-control" placeholder="Masukkan Nama Siswa" required>
              </div>
              <div class="form-group col-6">
                <label>Tempat Lahir</label>
                <span class="text-danger">*</span>
                <input type="text" name="tempat_lahir" class="form-control" placeholder="Masukkan Tempat Lahir" required>
              </div>
              <div class="form-group col-6">
                <label>Tanggal Lahir</label>
                <span class="text-danger">*</span>
                <div class="input-group date" data-target-input="nearest">
                  <input type="text" name="tanggal_lahir" class="form-control datetimepicker-input" placeholder="Tanggal Lahir" required />
                  <div class="input-group-append">
                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                  </div>
                </div>
              </div>
              <div class="form-group clearfix col-12">
                <label>Jenis Kelamin</label>
                <span class="text-danger">*</span>
                <br>
                <div class="row">
                  <div class="col-5">
                    <div class="icheck-primary d-inline" id="gender" name="gender">
                      <input type="radio" id="addfemale" name="gender" value="0" required>
                      <label for="addfemale">Perempuan
                      </label>
                    </div>
                  </div>
                  <div class="col-5">
                    <div class="icheck-primary d-inline">
                      <input type="radio" id="addmale" name="gender" value="1">
                      <label for="addmale">Laki-Laki
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>


  <div class="modal fade" id="modal-update-siswa" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Update Siswa</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form id="form-update-siswa">
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-6">
                <label>NIS</label>
                <span class="text-danger">*</span>
                <input type="text" name="nis" class="form-control" placeholder="Masukkan NIS" required>
              </div>
              <div class="form-group col-6">
                <label>NISN</label>
                <span class="text-danger">*</span>
                <input type="text" name="nisn" class="form-control" placeholder="Masukkan NISN" required>
              </div>
              <div class="form-group col-12">
                <label>Nama Siswa</label>
                <span class="text-danger">*</span>
                <input type="text" name="nama_siswa" class="form-control" placeholder="Masukkan Nama Siswa" required>
              </div>
              <div class="form-group col-6">
                <label>Tempat Lahir</label>
                <span class="text-danger">*</span>
                <input type="text" name="tempat_lahir" class="form-control" placeholder="Masukkan Tempat Lahir" required>
              </div>
              <div class="form-group col-6">
                <label>Tanggal Lahir</label>
                <span class="text-danger">*</span>
                <div class="input-group date" data-target-input="nearest">
                  <input type="text" name="tanggal_lahir" class="form-control datetimepicker-input" placeholder="Tanggal Lahir" required />
                  <div class="input-group-append">
                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                  </div>
                </div>
              </div>
              <div class="form-group clearfix col-12">
                <label>Jenis Kelamin</label>
                <span class="text-danger">*</span>
                <br>
                <div class="row">
                  <div class="col-4">
                    <div class="icheck-primary d-inline" id="gender" name="gender">
                      <input type="radio" id="updatefemale" name="gender" value="0" required>
                      <label for="updatefemale">Perempuan
                      </label>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="icheck-primary d-inline">
                      <input type="radio" id="updatemale" name="gender" value="1">
                      <label for="updatemale">Laki-Laki
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group clearfix col-12">
                <label>Status</label>
                <span class="text-danger">*</span>
                <br>
                <div class="row">
                  <div class="col-4">
                    <div class="icheck-primary d-inline">
                      <input type="radio" id="active" name="status" value="1" required>
                      <label for="active">Aktif
                      </label>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="icheck-primary d-inline">
                      <input type="radio" id="inactive" name="status" value="0">
                      <label for="inactive">Tidak Aktif
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>

  <?php
  $this->load->view("footer");
  ?>

  <script>
    $(document).ready(function () {
  //   const Toast = Swal.mixin({
  //     toast: true,
  //     position: 'top-end',
  //     showConfirmButton: false,
  //     timer: 3000
  //   });

  datatable = $('[data-plugin=datatable]').DataTable({
    language: {
      searchPlaceholder: 'Search...',
      sSearch: '',
      lengthMenu: '_MENU_ items/page',
      emptyTable: "No data available in table",
    },
    scrollY: 400,
    scrollX: true,
    scrollCollapse: true,
    paging:false,
    ordering: false,
    fixedColumns:   {
      leftColumns: 2,
      rightColumns: 1
    },
  //     },
  //     "ajax": "/Siswa/getSiswa",
      // "columns": [{
        // width: "20%"
  //     },
  //     {
  //       "data": "nis"
  //     },
  //     {
  //       "data": "nisn"
  //     },
  //     {
  //       "data": "nama_siswa"
  //     },
  //     {
  //       className: "text-center"
  //     },
  //     {
  //       className: "text-center"
      // }
      // ],

      // "columnDefs": [{
      //   "targets": 0,
      //   "width": "1%"
      // },
      // { 
      //   "targets": 1,
      //   "width": "10%"
      // },
      // { 
      //   "targets": 2,
      //   "width": "12.5%"
      // },
      // { 
      //   "targets": 3,
      //   "width": "12.5%"
      // },
      // { 
      //   "targets": 4,
      //   "width": "12.5%"
      // },
      // { 
      //   "targets": 5,        
      //   "width": "12.5%"
      // },
      // { 
      //   "targets": 6,
      //   "width": "12.5%"
      // },
      // { 
      //   "targets": 7,
      //   "width": "12.5%"
      // },
      // { 
      //   "targets": 8,
      //   "width": "12.5%"
      // },
      // { 
      //   "targets": 9,
      //   "width": "15.5%"
      // },
      // { 
      //   "targets": 10,
      //   "width": "12.5%"
      // },
      // { 
      //   "targets": 11,
      //   "width": "12.5%"
      // },
      // { 
      //   "targets": 12,
      //   "width": "30%"
      // },
      // { 
      //   "targets": 13,
      //   "width": "20%"
      // },
      // { 
      //   "targets": 14,
      //   "width": "60%"
      // }
      // { 
      //   "targets": 15,        
      //   "width": "1%"
      // }
      // { 
      //   "targets": 16,
      //   "width": "1%"
      // }
      // { 
      //   "targets": 17,
      //   "width": "3"
      // },
      // { 
      //   "targets": 18,
      //   "width": "3"
      // },
      // { 
      //   "targets": 19,
      //   "width": "3"
      // },
      // { 
      //   "targets": 20,
      //   "width": "10"
      // },
      // { 
      //   "targets": 21,
      //   "width": "1%"
      // },
      // { 
      //   "targets": 22,
      //   "width": "30"
      // },
      // { 
      //   "targets": 23,
      //   "width": "1"
      // },
      // { 
      //   "targets": 24,
      //   "width": "1"
      // },
      // { 
      //   "targets": 25,
      //   "width": "1"
      // },
      // { 
      //   "targets": 26,
      //   "width": "1"
      // },
      // { 
      //   "targets": 27,
      //   "width": "20"
      // },
      // { 
      //   "targets": 28,
      //   "width": "20"
      // },
      // { 
      //   "targets": 29,
      //   "width": "20"
      // },
      // { 
      //   "targets": 30,
      //   "width": "50"
      // }

  //       "render": function (data, type, row, meta) {
  //         return meta.row + meta.settings._iDisplayStart + 1;
  //       }
      // },
  //     {
  //       "targets": 4,
  //       "render": function (data, type, row, meta) {
  //         if (row.status == 1) {
  //           return '<div class="badge badge-success">Aktif</div>';
  //         } else {
  //           return '<div class="badge badge-secondary">Tidak Aktif</div>';
  //         }
  //       }
  //     },
  //     {
  //       "targets": 5,
  //       "render": function (data, type, row, meta) {
  //         return '<button type="button" class="btn btn-primary btn-sm" onclick="detail(' +
  //         row.nis + ')"><i class="fas fa-pencil-alt"></i></button>';
  //       }
// }
// ]

});

  //   $('#form-tambah-siswa').submit(function (e) {
  //     e.preventDefault();

  //     $.ajax({
  //       type: 'POST',
  //       url: `${base_url}/Siswa/tambahSiswa`,
  //       data: $('#form-tambah-siswa').serialize(),
  //       dataType: 'json'
  //     })
  //     .done(function (res) {
  //       $('#modal-tambah-siswa').modal('hide');
  //       if (res.success) {
  //         Toast.fire({
  //           type: 'success',
  //           title: res.message,
  //         });
  //         // setTimeout(() => {
  //         //   location.reload();
  //         // }, 1000);
  //       } else {
  //         Toast.fire({
  //           type: 'error',
  //           title: res.message,
  //         });
  //         setTimeout(() => {
  //           location.reload();
  //         }, 1000);
  //       }
  //     });
  //   });
  //   $('#form-update-siswa').submit(function (e) {
  //     e.preventDefault();

  //     $.ajax({
  //       type: 'POST',
  //       url: `${base_url}/Siswa/updateSiswa/${$('#form-update-siswa').attr('data-form-id')}`,
  //       data: $('#form-update-siswa').serialize(),
  //       dataType: 'json'
  //     })
  //     .done(function (res) {
  //       $('#modal-update-siswa').modal('hide');
  //       if (res.success) {
  //         Toast.fire({
  //           type: 'success',
  //           title: res.message,
  //         });
  //         setTimeout(() => {
  //           location.reload();
  //         }, 1000);
  //       } else {
  //         Toast.fire({
  //           type: 'error',
  //           title: res.message,
  //         });
  //         setTimeout(() => {
  //           location.reload();
  //         }, 1000);
  //       }
  //     });
  //   });
  // });
  // function detail(id) {
  //   $.ajax({
  //     type: 'POST',
  //     url: `${base_url}/Siswa/detailSiswa/${id}`,
  //     data: $('#form-tambah-siswa').serialize(),
  //     dataType: 'json'
  //   })
  //   .done(function (res) {
  //     if(res.success){
  //       var form = $('#form-update-siswa');
  //       form.attr('data-form-id', res.data.nis)
  //       form.find('[name=nis]').val(res.data.nis)
  //       form.find('[name=nisn]').val(res.data.nisn)
  //       form.find('[name=nama_siswa]').val(res.data.nama_siswa)
  //       form.find('[name=tempat_lahir]').val(res.data.tempat_lahir)
  //       console.log(res.data.tanggal_lahir);
  //       form.find('[name=tanggal_lahir]').val(res.data.tanggal_lahir)
  //       form.find('[name=gender][value='+res.data.gender+']').prop("checked", true)
  //       form.find('[name=status][value='+res.data.status+']').prop("checked", true)
  //       $('#modal-update-siswa').modal();
  //     }
});

</script>