 <?php $this->load->view("header", ["page" => $page]);?>
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Nilai Ekstrakurikuler</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Nilai Ekstrakurikuler</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <form id="form-simpan-nilai-ekskul">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="form-group col-3">
                    <label>Rombel</label>
                    <select name="data[rombel]" class="form-control select2 select2-rombel" id="select2-rombel">
                      <option value="" selected>Pilih rombel</option>
                    </select>
                  </div>
                  <div class="form-group col-3">
                    <label>Semester</label>
                    <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt">
                      <option selected="selected" disabled>Pilih semester</option>
                      <option value = "1">1 (Gasal)</option>
                      <option value = "2">2 (Genap)</option>
                    </select>
                  </div>
                  <div class="form-group col-6">
                    <button type="submit" class="btn btn-flat btn-success float-right"><i class="fa fa-save "></i> Simpan</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12" id="show-nilai-ekskul" hidden>
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table id="data-ekskul" class="table table-bordered table-striped display responsive nowrap">
                    <thead>
                      <tr>
                        <th rowspan="2" colspan="1" style="width: 1%">No</th>
                        <th rowspan="2" colspan="1" style="width: 9%">Nama Siswa</th>
                        <th colspan="4" rowspan="1" style="text-align: center;">Ekstrakurikuler 1</th>
                        <th colspan="4" rowspan="1" style="text-align: center; width: 40%">Ekstrakurikuler 2</th>

                      </tr>
                      <tr>
                        <th style=" width: 10%">Ekstrakurikuler</th>
                        <th style=" width: 8%">Nilai</th>
                        <th style=" width: 25%">Deskripsi</th>
                        <th style=" width: 10%">Ekstrakurikuler</th>
                        <th style=" width: 8%">Nilai</th>
                        <th style=" width: 25%">Deskripsi</th>
                      </tr>
                    </thead>
                    <tbody id="table-nilai-ekskul">

                    </tbody>
                  </table>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-flat btn-success float-right"><i class="fa fa-save "></i> Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- /.content -->
  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>
</div>
<?php
$this->load->view("footer");
?>

<script>
  $(document).ready(function () {

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    }); 

    $(".select2-rombel").select2({
      ajax: {
        url: "/Rombel/getCariRombel/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_rombel,
                id: item.id_rombel
              }
            })
          };
        }
      }
    });


    $('.select2-rombel').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var id_rombel = data.id;
      var smt = $('#select2-smt').val();

      if(smt) set_nilai(id_rombel, smt);      
    });

    $('.select2-smt').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var smt = data.id;

      var id_rombel = $('#select2-rombel').val();
      if(id_rombel) set_nilai(id_rombel, smt);      
    });

    $('#form-simpan-nilai-ekskul').submit(function (e) {
      e.preventDefault();

      let form = $('#form-simpan-nilai-ekskul').serializeObject();
      let penilaian = [];

      form.data.id_anggota_rombel.map((item, index) => {
        penilaian.push({
          id_nilai_ekskul: form.data.id_nilai_ekskul[index],
          id_anggota_rombel: form.data.id_anggota_rombel[index],
          id_ekskul_wajib_fk: form.data.id_ekskul_wajib_fk[index],
          nilai_wajib: form.data.nilai_wajib[index],
          deskripsi_wajib: form.data.deskripsi_wajib[index],
          id_ekskul_pilihan_fk: form.data.id_ekskul_pilihan_fk[index],
          nilai_pilihan: form.data.nilai_pilihan[index],
          deskripsi_pilihan: form.data.deskripsi_pilihan[index]
        });
      });
      
      let data = {
        rombel: form.data.rombel,
        semester: form.data.semester,
        penilaian
      }    

      $.ajax({
        type: 'POST',
        url: '/NilaiEkskul/saveNilaiEkskul/',
        data: data,
        dataType: 'json'
      })
      .done(function (res) {
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
        }
        $('#show-absensi').attr('hidden',false);
      });
    });
  });

  function set_nilai(rombel, smt) {
    $.ajax({
      type: 'GET',
      url: "/NilaiEkskul/getNilaiEkskul/"+rombel+"/"+smt,
      dataType: 'json'
    })
    .done(function (res) {
      $('#table-nilai-ekskul').html("");

      if (res.success) {

        let number = 1;

        res.data.forEach(element => {
          let nilai_wajib = '';
          let predikat_wajib = '';
          let deskripsi_wajib = '';
          let nilai_pilihan = '';
          let predikat_pilihan = '';
          let deskripsi_pilihan = '';

          if (element.nilai_wajib != null && element.nilai_wajib !=0){nilai_wajib = element.nilai_wajib;}
          else{nilai_wajib = '';}
          if(element.deskripsi_wajib != null && element.deskripsi_wajib !=0){deskripsi_wajib = element.deskripsi_wajib;}
          else{deskripsi_wajib = '';}
          if(element.nilai_pilihan != null && element.nilai_pilihan !=0){nilai_pilihan = element.nilai_pilihan;}
          else{nilai_pilihan = '';}
          if(element.deskripsi_pilihan != null && element.deskripsi_pilihan !=0){deskripsi_pilihan = element.deskripsi_pilihan;}
          else{deskripsi_pilihan = '';}

          $('#table-nilai-ekskul').append('<tr><td><input class="form-control" hidden type="text" name="data[id_nilai_ekskul][]" value="'+element.id_nilai_ekskul+'"><input class="form-control" hidden type="text" name="data[id_anggota_rombel][]" value="'+element.id_anggota_rombel+'">' + number++ + '</td><td>' + element.nama_siswa + '</td><td><select name="data[id_ekskul_wajib_fk][]" class="form-control select2 select2-wajib" id="select2-wajib'+number+'" style="min-width: 100px;"><option value="" selected>Pilih ekskul</option> </select></td><td><input class="form-control" type="number" name="data[nilai_wajib][]" value="'+ nilai_wajib + '" style="min-width: 60px;"></td><td><textarea class="form-control" name="data[deskripsi_wajib][]" rows="2" value="'+ deskripsi_wajib +'" style="min-width: 150px;">'+deskripsi_wajib+'</textarea></td><td><select name="data[id_ekskul_pilihan_fk][]" class="form-control select2 select2-pilihan" id="select2-pilihan'+number+'" style="min-width: 100px;"><option value="" selected>Pilih ekskul</option> </select></td><td><input max="100" class="form-control" type="number" id="nilai-pilihan" name="data[nilai_pilihan][]" value="'+ nilai_pilihan + '" style="min-width: 60px;"></td><td><textarea class="form-control" name="data[deskripsi_pilihan][]" rows="2" value="'+ deskripsi_pilihan +'" style="min-width: 150px;">'+deskripsi_pilihan+'</textarea></td></tr>');

          if(element.id_ekskul_wajib_fk){
            var ekskul_wajib = new Option(element.nama_ekskul_wajib, element.id_ekskul_wajib_fk, true, true);
            $('#select2-wajib'+number).append(ekskul_wajib).trigger('change');
            $('#select2-wajib'+number).trigger({
              type: 'select2:select',
              params: {
                data: {
                  text: element.nama_ekskul_wajib,
                  id: element.id_ekskul_wajib_fk
                }
              }
            });
          }else{
            var ekskul_wajib = new Option("Pramuka", 1, true, true);
            $('#select2-wajib'+number).append(ekskul_wajib).trigger('change');
            $('#select2-wajib'+number).trigger({
              type: 'select2:select',
              params: {
                data: {
                  text: "Pramuka",
                  id: 1
                }
              }
            });
          }

          if(element.id_ekskul_pilihan_fk != 0 && element.id_ekskul_pilihan_fk != null ){
            var ekskul_pilihan = new Option(element.nama_ekskul_pilihan, element.id_ekskul_pilihan_fk, true, true);
            $('#select2-pilihan'+number).append(ekskul_pilihan).trigger('change');
            $('#select2-pilihan'+number).trigger({
              type: 'select2:select',
              params: {
                data: {
                  text: element.nama_ekskul_pilihan,
                  id: element.id_ekskul_pilihan_fk
                }
              }
            });
          }else{
            var ekskul_pilihan = new Option("Pilih ekskul", null, true, true);   
            $('#select2-pilihan'+number).append(ekskul_pilihan).trigger('change');
            $('#select2-wajib'+number).trigger({
              type: 'select2:select',
              params: {
                data: {
                  text: "Pilih salah satu",
                  id: null
                }
              }
            }); 
          }
        });

        $('#show-nilai-ekskul').attr('hidden',false);
      } else {
        Toast.fire({
          type: 'error',
          title: res.message,
        });
        $('#table-nilai-ekskul').append('<tr><td colspan="6" align="middle">No data available in table</td></tr>');
      }

      $(".select2-wajib").select2({
        ajax: {
          url: "/Ekskul/getCariEkskul/",
          dataType: 'json',
          type: "GET",
          quietMillis: 50,
          processResults: function (res) {
            return {
              results: $.map(res.data, function (item) {
                return {
                  text: item.nama_ekskul,
                  id: item.id_ekskul
                }
              })
            };
          }
        }
      });

      $(".select2-pilihan").select2({
        ajax: {
          url: "/Ekskul/getCariEkskul/",
          dataType: 'json',
          type: "GET",
          quietMillis: 50,
          processResults: function (res) {
            return {
              results: $.map(res.data, function (item) {
                return {
                  text: item.nama_ekskul,
                  id: item.id_ekskul
                }
              })
            };
          }
        }
      });
    });
  }
</script>