 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Ekstrakurikuler X IPA 1
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Ekstrakurikuler</a></li>
      <li class="active">Data Ekstrakurikuler</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
         <div class="box-header with-border">
          <button type="button" class="btn btn-flat btn-info"></i> Simpan</button>
          <button type="button" class="btn btn-flat btn-danger " data-toggle="modal" data-target="#reset-ekskul">Reset</button>
          

        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">

         <table id="data-ekskul" class="table table-bordered table-striped" style="width: 100%;">
          <thead>
            <tr>
              <th rowspan="2" colspan="1" style="width: 5%;">No</th>
              <th rowspan="2" colspan="1" style="width: 25%;" >Nama Siswa</th>
              <th colspan="4" rowspan="1" style="text-align: center;">Ekstrakurikuler 1</th>
              <th colspan="4" rowspan="1" style="text-align: center">Ekstrakurikuler 2</th>
              
            </tr>
            <tr>
              <th style="width: 10%;">Ekstrakurikuler</th>
              <th style="width: 5%;">Nilai</th>
              <th style="width: 5%;">Predikat </th>
              <th style="width: 15%;">Keterangan</th>
              <th style="width: 10%;">Ekstrakurikuler</th>
              <th style="width: 5%;">Nilai</th>
              <th style="width: 5%;">Predikat </th>
              <th style="width: 15%;">Keterangan</th>
            </tr>
          </thead>
          <tbody>
            <?php for ($i=0; $i < 35 ; $i++) { 
              ?>
              <tr>
                <td>1</td>
                <td>Alamsyah Yanuar Wiryan Jana Pribadi</td>
                <td><select class="form-control select2" style="width: 100%">
                  <option>Pilih</option>
                  <option>Olimpiade Matematika</option>
                  <option>Basket</option>
                 
                </select></td>
                <td><input class="form-control" type="text" name="" style="width: 100%"></td>
                <td><input class="form-control" type="text" name="" disabled style="width: 100%;"></td>
                <td><textarea class="form-control"></textarea></td>
                <td><select class="form-control select2" style="width: 100%;">
                  <option>Pilih</option>
                  <option>Menari</option>
                  <option>Basket</option>
                 
                </select></td>
                <td><input class="form-control" type="text" name="" style="width: 100%" ></td>
                <td><input class="form-control" type="text" name="" disabled style="width: 100%;"></td>
                <td><textarea class="form-control"></textarea></td>
              </tr>
              <?php } ?>
            </tbody>

          </table>

        </div>
      </div>
      <!-- /.box -->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <div class="modal fade" id="import-ekskul">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Import Ekstrakurikuler</h4>
          </div>
          <div class="modal-body">
            <div class="box-body">
             <form role="form">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Pilih File</label>
                    <input type="file" class="form-control">
                  </div>
                  <a href="#"><label>Download template file <i class="fa fa-download"> </i></label></a>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-flat btn-primary">Simpan</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


  <div class="modal fade" id="reset-ekskul">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Reset Esktrakurikuler X IPA 1</h4>
          </div>
          <div class="modal-body">
            <div class="box-body">
              <h5>Apakah Anda yakin akan menghapus seluruh ekstrakurikuler kelas X IPA 1 ?</h5>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
            <button type="button" class="btn btn-flat btn-danger">Hapus</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
