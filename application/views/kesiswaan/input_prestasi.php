 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Prestasi X IPA 1
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Prestasi</a></li>
      <li class="active">Data Prestasi</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
         <div class="box-header with-border">
          <button type="button" class="btn btn-flat btn-info"></i> Simpan</button>
          <button type="button" class="btn btn-flat btn-danger " data-toggle="modal" data-target="#reset-prestasi">Reset</button>
          
          
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">

         <table id="data-prestasi" class="table table-bordered table-striped" style="table-layout: fixed;">
          <thead>
            <tr>
              <th rowspan="2" colspan="1" style="width: 5%;" ">No</th>
              <th rowspan="2" colspan="1" style="width: 15%;">Nama Siswa</th>
              <th colspan="2" rowspan="1">Prestasi 1</th>
              <th colspan="2" rowspan="1">Prestasi 2</th>
              
            </tr>
            <tr>
             <th>Jenis Prestasi</th>
             <th>Keterangan</th>
             <th>Jenis Prestasi </th>
             <th>Keterangan</th>
           </tr>
         </thead>
         <tbody>
          <?php for ($i=0; $i < 35 ; $i++) { 
            ?>
            <tr>
              
              <td>1</td>
              <td>Muhammad Aditya Irwandana</td>
              <td><input class="form-control" type="text" name=""></td>
              <td><textarea class="form-control"></textarea></td>
              <td><input class="form-control" type="text" name=""></td>
              <td><textarea class="form-control"></textarea></td>
              
            </tr>
            <?php } ?>
          </tbody>

        </table>
        
      </div>
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="edit-ekstra">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Prestasi</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
           <form role="form">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Prestasi 1</label>
                  <input type="text" class="form-control" id="inputnis" placeholder="Masukkan nama ekstrakurikuler" value="Pramuka" autofocus>
                </div>
                <div class="form-group">
                  <label>Prestasi 2</label>
                  <input type="text" class="form-control" id="inputnis" placeholder="Masukkan nama ekstrakurikuler" value="Pramuka" autofocus>
                </div>
                <button type="button" class="btn btn-flat btn-success" data-dismiss="modal">Tambah</button>
              </div>

            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-flat btn-primary">Simpan</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="import-prestasi">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Import Prestasi</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
           <form role="form">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Pilih File</label>
                  <input type="file" class="form-control">
                </div>
                <a href="#"><label>Download template file <i class="fa fa-download"> </i></label></a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-flat btn-primary">Simpan</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="reset-prestasi">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Reset Prestasi X IPA 1</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <h5>Apakah Anda yakin akan menghapus seluruh prestasi kelas X IPA 1 ?</h5>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-flat btn-danger">Hapus</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
