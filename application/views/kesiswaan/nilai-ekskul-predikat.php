 <?php $this->load->view("header", ["page" => $page]);?>
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Nilai Ekstrakurikuler</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Nilai Ekstrakurikuler</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <form id="form-simpan-nilai-ekskul">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="form-group col-3">
                    <label>Rombel</label>
                    <select name="data[rombel]" class="form-control select2 select2-rombel" id="select2-rombel">
                      <option value="" selected>Pilih rombel</option>
                    </select>
                  </div>
                  <div class="form-group col-3">
                    <label>Semester</label>
                    <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt">
                      <option selected="selected" disabled>Pilih semester</option>
                      <option value = "1">1 (Gasal)</option>
                      <option value = "2">2 (Genap)</option>
                    </select>
                  </div>
                  <div class="form-group col-6">
                    <button type="submit" class="btn btn-flat btn-success float-right"><i class="fa fa-save "></i> Simpan</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12" id="show-nilai-ekskul" hidden>
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table id="data-ekskul" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th rowspan="2" colspan="1" style="width: 1%">No</th>
                        <th rowspan="2" colspan="1" style="width: 9%">Nama Siswa</th>
                        <th colspan="4" rowspan="1" style="text-align: center;">Ekstrakurikuler 1</th>

                        <th colspan="4" rowspan="1" style="text-align: center; width: 40%">Ekstrakurikuler 2</th>

                      </tr>
                      <tr>
                        <th style=" width: 10%">Ekstrakurikuler</th>
                        <th style=" width: 8%">Nilai</th>
                        <th style=" width: 1%">Huruf</th>
                        <th style=" width: 19%">Deskripsi</th>
                        <th style=" width: 10%">Ekstrakurikuler</th>
                        <th style=" width: 8%">Nilai</th>
                        <th style=" width: 1%">Huruf</th>
                        <th style=" width: 19%">Deskripsi</th>
                      </tr>
                    </thead>
                    <tbody id="table-nilai-ekskul">

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- /.content -->
  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
    <i class="fas fa-chevron-up"></i>
  </a>
</div>
<?php
$this->load->view("footer");
?>

<script>
  $(document).ready(function () {

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000

    }); 

    $(".select2-rombel").select2({
      ajax: {
        url: "/Rombel/getCariRombel/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_rombel,
                id: item.id_rombel
              }
            })
          };
        }
      }

    });

    $(".select2-ekskul").select2({
      ajax: {
        url: "/Ekskul/getCariEkskul/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_ekskul,
                id: item.id_ekskul
              }
            })
          };
        }
      }

    });

    $('.select2-rombel').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var id_rombel = data.id;
      var smt = $('#select2-smt').val();

      if(smt) set_nilai(id_rombel, smt);      
    });

    $('.select2-smt').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var smt = data.id;

      var id_rombel = $('#select2-rombel').val();
      if(id_rombel) set_nilai(id_rombel, smt);      
    });

  });

  function set_nilai(rombel, smt) {
   $.ajax({
    type: 'GET',
    url: "/NilaiEkskul/getNilaiEkskul/"+rombel+"/"+smt,
    dataType: 'json'
  })
   .done(function (res) {
    $('#table-nilai-ekskul').html("");

    if (res.success) {

      let number = 1;

      res.data.forEach(element => {
        let nilai_wajib = '';
        let predikat_wajib = '';
        let deskripsi_wajib = '';
        let nilai_pilihan = '';
        let predikat_pilihan = '';
        let deskripsi_pilihan = '';

        if (element.nilai_wajib != null) {
          nilai_wajib = element.nilai_wajib;
        }
        if (element.predikat_wajib != null) {
          predikat_wajib = element.predikat_wajib;
        }
        if(element.deskripsi_wajib != null){
          deskripsi_wajib = element.deskripsi_wajib;
        }
        if(element.nilai_pilihan != null){
          nilai_pilihan = element.nilai_pilihan;
        }
        if(element.predikat_pilihan != null){
          predikat_pilihan = element.predikat_pilihan;
        }
        if(element.deskripsi_pilihan != null){
          deskripsi_pilihan = element.deskripsi_pilihan;
        }

        $('#table-nilai-ekskul').append('<tr><td><input class="form-control" hidden type="text" name="data[id_nilai_ekskul][]" value="'+element.id_nilai_ekskul+'"><input class="form-control" hidden type="text" name="data[id_anggota_rombel][]" value="'+element.id_anggota_rombel+'">' + number++ + '</td><td>' + element.nama_siswa + '</td><td><input class="form-control" hidden type="text" name="data[id_ekskul_wajib_fk][]" value="'+element.id_ekskul_wajib_fk+'"><select name="data[ekskul]" class="form-control select2 select2-ekskul" id="select2-ekskul-wajib"><option value="" selected>Pilih ekskul</option></select></td><td><input class="form-control" type="text" id="nilai-wajib'+element.id_anggota_rombel+'" name="data[nilai_wajib][]" onkeyup="predikat('+element.id_anggota_rombel+')" value="'+ nilai_wajib + '"></td><td><input class="form-control" type="text" id="predikat-wajib'+element.id_anggota_rombel+'" value="'+ predikat_wajib + '" name="data[predikat_wajib][]" disabled></td><td><textarea class="form-control" name="data[deskripsi_wajib][]" rows="2"  value="'+ deskripsi_wajib +'">'+deskripsi_wajib+'</textarea></td><td><select name="data[ekskul]" class="form-control select2 select2-ekskul" id="select2-ekskul-pilihan"><option value="" selected>Pilih ekskul</option></select></td><td><input class="form-control" hidden type="text" name="data[id_ekskul_pilihan_fk][]" value="'+element.id_ekskul_pilihan_fk+'"><input class="form-control" type="text" id="nilai-pilihan" name="data[nilai_pilihan][]" onkeyup="predikat('+element.id_anggota_rombel+')" value="'+ nilai_pilihan + '"></td><td><input class="form-control" type="text" id="predikat-pilihan" name="data[predikat_pilihan][]" value="'+ predikat_pilihan + '" disables></td><td><textarea class="form-control" name="data[deskripsi_pilihan][]" rows="2"  value="'+ deskripsi_pilihan +'">'+deskripsi_pilihan+'</textarea></td></tr>');
      });

      $('#show-nilai-ekskul').attr('hidden',false);
    } else {
      Toast.fire({
        type: 'error',
        title: res.message,
      });
      $('#table-nilai-ekskul').append(
        '<tr><td colspan="6" align="middle">No data available in table</td></tr>')
    }
  });
 }

 function predikat(id_anggota_rombel){
   var nilai_wajib = $('#nilai-wajib'+id_anggota_rombel).val();
   var nilai_pilihan = $('#nilai-pilihan'+id_anggota_rombel).val();
   
   $(nilai_wajib).off('keyup');

   if(nilai_wajib < 70){
     var result = "D";
      $('#predikat-wajib'+id_anggota_rombel).val(result);
   }
   else if(nilai_wajib >= 70 && nilai_wajib <= 74){
    var result = "C";
    $('#predikat-wajib'+id_anggota_rombel).val(result);
  }
  else if(nilai_wajib >= 75 && nilai_wajib <= 85){
    var result = "B";
    $('#predikat-wajib'+id_anggota_rombel).val(result);
  }
  else if(nilai_wajib >= 86 && nilai_wajib <= 100){
    var result = "A";
    $('#predikat-wajib'+id_anggota_rombel).val(result);
  }
  else if(nilai_wajib == null){
    $('#predikat-wajib'+id_anggota_rombel).val('');
  }
// if(nilai_pilihan < 70){
//  var result = "D";
//  if (!isNaN(result)) {
//   $('#predikat-pilihan').val(result);
// }
// }
// else if(nilai_pilihan >= 70 && nilai_pilihan <= 74){
//   var result = "C";
//   if (!isNaN(result)) {
//    $('#predikat-pilihan').val(result);
//  }
// }
// else if(nilai_pilihan >= 75 && nilai_pilihan < 85){
//   var result = "B";
//   if (!isNaN(result)) {
//     $('#predikat-pilihan').val(result);
//   }
// }
// else if(nilai_pilihan >= 86 && nilai_pilihan < 100){
//   var result = "A";
//   if (!isNaN(result)) {
//    $('#predikat-pilihan').val(result);
//  }
// }
}

</script>