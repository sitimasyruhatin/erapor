 <?php $this->load->view("header", ["page" => $page]);?>

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Prestasi <span id="nama-rombel"></span></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active">Prestasi</li>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <form id="form-simpan-prestasi">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="form-group col-3">
                  <label>Pilih Rombel</label>
                  <span class="text-danger">*</span>
                  <select name="data[rombel]" id="id-rombel" class="form-control select2 select2-rombel" style="width: 100%;" required="">
                    <option value="" selected>Pilih item</option>
                  </select>
                </div>
                <div class="form-group col-3">
                  <label>Pilih Semester</label>
                  <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt" style="width: 100%;">
                    <option selected="selected" disabled>Pilih semester</option>
                    <option value = "1">1 (Gasal)</option>
                    <option value = "2">2 (Genap)</option>
                  </select>
                </div>
                <div class="form-group col-6">
                  <button type="submit " class="btn btn-flat btn-success float-right " data-toggle="modal" data-target="#tambah-kelas"><i class="fa fa-save "></i> Simpan</button>
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.card-header -->
          </div>
        </div>

        <div class="col-12" id="show-prestasi" hidden >
          <div class="card">
            <div class="card-body">

             <table class="table table-bordered table-striped" data-plugin="prestasi" style="width:100%"> 
              <thead>
                <tr>
                  <th width="1%">No</th>
                  <th>Nama Siswa</th>
                  <th width="50%">Prestasi</th>
                </tr>
                
              </thead>
              <tbody  id="table-prestasi">

              </tbody>

            </table>
          </div>
        </div>
      </div>
    </form>
    <!-- /.card -->
  </div>
</section>
<!-- /.content -->
<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
  <i class="fas fa-chevron-up"></i>
</a>
</div>

<?php
$this->load->view("footer");
?>

<script>

  $(document).ready(function () {

    var datatableNilaiSikap;

    var dataRombel;

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $(".select2-rombel").select2({
      ajax: {
        url: "/Rombel/getCariRombel/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_rombel,
                id: item.id_rombel
              }
            })
          };
        }
      }
    });

    $('.select2-rombel').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var id_rombel = data.id;

      var smt = $('#select2-smt').val();
      if(smt) set_nilai(id_rombel, smt);  
    });

    $('.select2-smt').on('select2:selecting', function (e) {    
      var data = e.params.args.data;  
      var smt = data.id;

      var id_rombel = $('#id-rombel').val();
      if(id_rombel) set_nilai(id_rombel, smt);      
    });

    $('#form-simpan-prestasi').submit(function (e) {
      e.preventDefault();

      let form = $('#form-simpan-prestasi').serializeObject();
      let penilaian = [];

      form.data.id_anggota_rombel.map((item, index) => {
        penilaian.push({
          id_prestasi: form.data.id_prestasi[index],
          id_anggota_rombel: form.data.id_anggota_rombel[index],
          jenis_kegiatan: form.data.jenis_kegiatan[index],
          keterangan: form.data.keterangan[index]
        });
      });
      let data = {
        rombel: form.data.rombel,
        semester: form.data.semester,
        penilaian
      }

      $.ajax({
        type: 'POST',
        url: `${base_url}/Prestasi/savePrestasi/`,
        data: data,
        dataType: 'json'
      })
      .done(function (res) {
        if (res.success) {
          Toast.fire({
            type: 'success',
            title: res.message,
          });
          // setTimeout(() => {
          //   location.reload();
          // }, 1000);

          // $('#show-prestasi').html('');
          // $('.select2-smt').val(data.semester).trigger("change");
        } else {
          Toast.fire({
            type: 'error',
            title: res.message,
          });
          // setTimeout(() => {
          //   location.reload();
          // }, 1000);
        }
        $('#show-prestasi').attr('hidden',false);
      });
    });

  }); 
  function set_nilai(rombel, smt) {
    $.ajax({
      type: 'GET',
      url: "/Prestasi/getPrestasi/"+rombel+"/"+smt,
      dataType: 'json'
    })
    .done(function (res) {
      $('#table-prestasi').html("");
      if (res.success) {

        let number = 1;

        res.data.forEach(element => {
            // let sakit = '';
            // let ijin = '';
            // let alpa = '';

            // if (element.sakit != 0) {
            //   sakit = element.sakit;
            // }
            // if (element.ijin != 0) {
            //   ijin = element.ijin;
            // }
            // if (element.alpa != 0) {
            //   alpa = element.alpa;
            // }
            $('#table-prestasi').append(
              '<tr><td width="30"><input class="form-control" hidden type="text" name="data[id_prestasi][]" value="'+element.id_prestasi+'"><input class="form-control" hidden type="text" name="data[id_anggota_rombel][]" value="'+element.id_anggota_rombel+'">' + number++ + '</td><td>' + element.nama_siswa + '</td><td id="baris'+element.id_anggota_rombel+'"><div class="row" id="prestasi-'+element.id_anggota_rombel+'" class="align-middle"><div class="col-3"><div class="form-group"><label>Jenis Prestasi</label><textarea  name="data[jenis_kegiatan][]"class="form-control" ></textarea></div></div><div class="col-8"><div class="form-group"><label>Keterangan</label><textarea class="form-control"  name="data[keterangan][]" ></textarea></div></div><div "class="col-1"><button type="button" class="btn btn-primary btn-sm mr-1" onclick="addprestasi('+element.id_anggota_rombel+')" ><i class="fas fa-plus"></i></button></div></div></td></tr>')

                $('#show-prestasi').attr('hidden',false);
              }); 
      } else {
        Toast.fire({
          type: 'error',
          title: res.message,
        });
      }
    }); 
  }

  function addprestasi(id_anggota_rombel){
    $('#baris'+id_anggota_rombel).prepend('<div class="row remove-attr" id="prestasi-'+id_anggota_rombel+'"><div class="col-3"><div class="form-group"><label>Jenis Prestasi</label><textarea name="data[jenis_kegiatan][]" class="form-control" ></textarea></div></div><div class="col-8"><div class="form-group"><label>Keterangan</label><textarea name="data[keterangan][]" class="form-control" ></textarea></div></div><div class="col-1"><button type="button" class="btn btn-primary btn-sm mr-1 align-middle" id="removeprestasi" ><i class="fas fa-minus"></i></button></div></div>')
    // $('#kegiatan-'+id_anggota_rombel).append('<div class="form-group"><label>Jenis Prestasi</label><textarea class="form-control" ></textarea></div>');
    // $('#keterangan-'+id_anggota_rombel).append('<div class="form-group"><label>Keterangan</label><textarea class="form-control" ></textarea></div>');
    // $('#button-'+id_anggota_rombel).append('<button type="button" class="btn btn-primary btn-sm mr-1" id="add-form-prestasi" onclick="addprestasi()"><i class="fas fa-minus"></i></button>') 
  };

  $("#removeprestasi").on('click',function(){
    $(this).closest('.remove-attr').remove();
  });

  // function removeprestasi(){
  //   $(this).closest('.remove-attr').remove();
  //   // console.log($(this).closest('div').closest('div'));
  // }

// $(document).ready(function() {
//   var max_fields      = 10; //maximum input boxes allowed
//   var wrapper       = $(".input_fields_wrap"); //Fields wrapper
//   var add_button      = $(".add_field_button"); //Add button ID

//   var x = 1; //initlal text box count
//   $(add_button).click(function(e){ //on add input button click
//     e.preventDefault();
//     if(x < max_fields){ //max input box allowed
//       x++; //text box increment
//       $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
//     }
//   });

//   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
//     e.preventDefault(); $(this).parent('div').remove(); x--;
//   })
// });


    // $('#form-update-prestasi').submit(function (e) {
    //   e.preventDefault();

    //   $.ajax({
    //     type: 'POST',
    //     url: `${base_url}/Prestasi/updatePrestasi/${$('#form-update-prestasi').attr('data-form-id')}`,
    //     data: $('#form-update-prestasi').serialize(),
    //     dataType: 'json'
    //   })
    //   .done(function (res) {
    //     $('#modal-update-prestasi').modal('hide');
    //     if (res.success) {
    //       Toast.fire({
    //         type: 'success',
    //         title: res.message,
    //       });
    //       datatable.ajax.reload(null, false);
    //     } else {
    //       Toast.fire({
    //         type: 'error',
    //         title: res.message,
    //       });        
    //     }
    //   });
    // });

    // function update(id) {
    //   $.ajax({
    //     type: 'POST',
    //     url: `${base_url}/Prestasi/getDetailPrestasi/${id}`,
    //     data: $('#form-tambah-prestasi').serialize(),
    //     dataType: 'json'
    //   })
    //   .done(function (res) {
    //     if(res.success){
    //       var form = $('#form-update-prestasi');
    //       form.attr('data-form-id', res.data.id_ekskul)
    //       form.find('[name=jurusan]').val(res.data.nama_ekskul)
    //       $('#modal-update-ekskul').modal();
    //     }
    //   });
    // }



  </script>