 <?php $this->load->view("header", ["page" => $page]);?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Prestasi <span id="nama-rombel"></span></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active">Prestasi</li>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <form id="form-simpan-prestasi">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="form-group col-3">
                  <label>Pilih Rombel</label>
                  <span class="text-danger">*</span>
                  <select name="data[rombel]" id="id-rombel" class="form-control select2 select2-rombel" style="width: 100%;" required="">
                    <option value="" selected>Pilih item</option>
                  </select>
                </div>
                <div class="form-group col-3">
                  <label>Pilih Semester</label>
                  <select name="data[semester]" class="form-control select2 select2-smt" id="select2-smt" style="width: 100%;">
                    <option selected="selected" disabled>Pilih semester</option>
                    <option value = "1">1 (Gasal)</option>
                    <option value = "2">2 (Genap)</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12" id="show-prestasi" hidden >
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-tambah-prestasi"><i class="fas fa-plus"></i> Tambah</button>
            </div>
            <div class="card-body">

             <table class="table table-bordered table-striped" data-plugin="prestasi" style="width:100%"> 
              <thead>
                <tr>
                  <th width="1%">No</th>
                  <th width="20%">Nama Siswa</th>
                  <th width="20%">Jenis Kegiatan</th>

                  <th width="50%">Keterangan</th>

                  <th width="15%">Aksi</th>
                </tr>
                
              </thead>
              <tbody  id="table-prestasi">

              </tbody>

            </table>
          </div>
        </div>
      </div>
    </form>
    <!-- /.card -->
  </div>
</section>
<!-- /.content -->
<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
  <i class="fas fa-chevron-up"></i>
</a>
</div>

<div class="modal fade" id="modal-tambah-prestasi" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Prestasi <span id="judul-tambah-prestasi"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-tambah-prestasi">
        <div class="modal-body">
          <!-- text input -->
          <div class="form-group">
            <label>Nama Siswa</label>
            <span class="text-danger">*</span>
            <select name="anggota_rombel" class="form-control select2 select2-anggota-rombel set-null" style="width: 100%;" required="">
              <option value="" selected="selected">Pilih item</option>
            </select>
          </div>
          <div class="form-group">
            <label>Jenis Kegiatan</label>
            <span class="text-danger">*</span>
            <input type="text" name="jenis_kegiatan" class="form-control ekskul set-null" placeholder="Masukkan jenis kegiatan" required>
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <span class="text-danger">*</span>
            <textarea  name="keterangan"class="form-control set-null" rows="3" placeholder="Masukkan keterangan prestasi" required></textarea>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-update-prestasi" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Prestasi <span id="judul-update-prestasi"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-update-prestasi">
        <div class="modal-body">
          <!-- text input -->
          <div class="form-group">
            <label>Nama Siswa</label>
            <span class="text-danger">*</span>
            <select name="anggota_rombel" class="form-control select2 select2-anggota-rombel" id="select2-anggota-rombel" style="width: 100%;" required="">
              <option value="" selected="selected">Pilih item</option>
            </select>
          </div>
          <div class="form-group">
            <label>Jenis Kegiatan</label>
            <span class="text-danger">*</span>
            <input type="text" name="jenis_kegiatan" class="form-control" placeholder="Masukkan nama ekskul" required>
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <span class="text-danger">*</span>
            <textarea name="keterangan"class="form-control" rows="3" required></textarea>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-delete-prestasi" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Prestasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form id="form-delete-prestasi">
        <div class="modal-body">
          <input type="hidden" name="id" class="form-control">
          Apakah Anda yakin akan menghapus prestasi?
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content -->
</div>

<?php
$this->load->view("footer");
?>

<script>
  $(document).ready(function () {

    var datatable;

    var datatableNilaiSikap;

    var dataRombel;

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $(".select2-rombel").select2({
      ajax: {
        url: "/Rombel/getCariRombel/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text: item.nama_rombel,
                id: item.id_rombel
              }
            })
          };
        }
      }
    });

    $('.select2-rombel').on('select2:selecting', function (e) { 

     $('[data-plugin=prestasi]').dataTable().fnDestroy();

     var data = e.params.args.data;  
     var id_rombel = data.id;

     var smt = $('#select2-smt').val();
     if(smt) set_nilai(id_rombel, smt);

     $('#judul-tambah-prestasi').text(data.text); 
   });

    $('.select2-smt').on('select2:selecting', function (e) {  

     $('[data-plugin=prestasi]').dataTable().fnDestroy();

     var data = e.params.args.data;  
     var smt = data.id;

     var id_rombel = $('#id-rombel').val();

     if(id_rombel) set_nilai(id_rombel, smt); 

     // $('#judul-tambah-prestasi').text(data.text);
   });




    function set_nilai(rombel, smt) {

      $(".select2-anggota-rombel").select2({
        ajax: {
          url: "/AnggotaRombel/getCariAnggotaRombel/"+rombel,
          dataType: 'json',
          type: "GET",
          quietMillis: 50,
          processResults: function (res) {
            return {
              results: $.map(res.data, function (item) {
                return {
                  text: item.nama_siswa,
                  id: item.id_anggota_rombel
                }
              })
            };
          }
        }
      });

      datatable = $('[data-plugin=prestasi]').DataTable({

        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
          emptyTable: "No data available in table"
        },

        "ajax": "/Prestasi/getPrestasi/"+rombel+"/"+smt,
        "columns": [{
          className: "text-center"
        },
        {
          "data": "nama_siswa"
        },
        {
          "data": "jenis_kegiatan"
        },
        {
          "data": "keterangan"
        },
        {
          className: "text-center"
        }
        ],
        "columnDefs": [{
          "targets": 0,
          "render": function (data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
          }
        },
        {
          "targets": 4,
          "render": function (data, type, row, meta) {
            return '<button type="button" class="btn btn-primary btn-sm mr-1" onclick="update(' +
            row.id_prestasi + ')"><i class="fas fa-pencil-alt"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="softdelete(' + row.id_prestasi +')"><i class="fas fa-trash-alt"></i></button>';
          }
        }
        ]
      });
      $('#show-prestasi').attr('hidden',false);

      $('#form-tambah-prestasi').submit(function (e) {
        e.preventDefault();
        $.ajax({
          type: 'POST',
          url: `${base_url}/Prestasi/TambahPrestasi/`+rombel+"/"+smt,
          data: $('#form-tambah-prestasi').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('.set-null').val('').trigger('change');
          $('.set-null').val(null);
          $('#modal-tambah-prestasi').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });
          }
        });
      });

      $('#form-update-prestasi').submit(function (e) {
        e.preventDefault();

        $.ajax({
          type: 'POST',
          url: `${base_url}/Prestasi/UpdatePrestasi/${$('#form-update-prestasi').attr('data-form-id')}`+"/"+smt,
          data: $('#form-update-prestasi').serialize(),
          dataType: 'json'
        })
        .done(function (res) {
          $('#modal-update-prestasi').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });
          }
        });
      });

      $('#form-delete-prestasi').submit(function (e) {
        e.preventDefault();

        $.ajax({
          type: 'POST',
          url: `${base_url}/Prestasi/deletePrestasi/${$('#form-delete-prestasi').attr('data-form-id')}`,
          dataType: 'json'
        })
        .done(function (res) {
          $('#modal-delete-prestasi').modal('hide');
          if (res.success) {
            Toast.fire({
              type: 'success',
              title: res.message,
            });
            datatable.ajax.reload(null, false);
          } else {
            Toast.fire({
              type: 'error',
              title: res.message,
            });
          }
        });
      });
    }
  });

 function update(id,smt) {
  $.ajax({
    type: 'POST',
    url: `${base_url}/Prestasi/getPrestasi/${id}`,
    data: $('#form-tambah-prestasi').serialize(),
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-update-prestasi');

      var data = res.data[0];
      form.attr('data-form-id', data.id_prestasi)

      var anggota_rombel = new Option(data.nama_siswa, data.id_anggota_rombel_fk, true, true);
      $('#select2-anggota-rombel').append(anggota_rombel).trigger('change');
      $('#select2-anggota-rombel').trigger({
        type: 'select2:select',
        params: {
          data: {
            text: data.nama_siswa,
            id: data.id_anggota_rombel_fk
          }
        }
      });
      form.find('[name=jenis_kegiatan]').val(data.jenis_kegiatan)
      form.find('[name=keterangan]').val(data.keterangan)

      $('#modal-update-prestasi').modal();
    }
  });
}

function softdelete(id) {
  $.ajax({
    type: 'GET',
    url: `${base_url}/Prestasi/getPrestasi/${id}`,
    dataType: 'json'
  })
  .done(function (res) {
    if(res.success){
      var form = $('#form-delete-prestasi');

      var data = res.data[0];

      form.attr('data-form-id', data.id_prestasi)
      $('#modal-delete-prestasi').modal();
    }
  });
}


</script>