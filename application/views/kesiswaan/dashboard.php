  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Kesiswaan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <?php  for ($i=1; $i < 18 ; $i++) { 
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon" style="font-size: 30px;">5%</span>
            <div class="info-box-content">
              <span class="info-box-text">EKSTRAKURIKULER</span>
              <span class="info-box-number">X IPA <?php echo $i ?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
              <span class="progress-description">
                1/40 Nilai
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <?php
        }
      ?>
    </div>
      <div class="row">
        <?php  for ($i=1; $i <= 18 ; $i++) { 
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon" style="font-size: 30px;">5%</span>
            <div class="info-box-content">
              <span class="info-box-text">PRESTASI</span>
              <span class="info-box-number">X IPA <?php echo $i ?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
              <span class="progress-description">
                1/40 Nilai
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div
        <?php
        }
      ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->