<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>E-Rapor</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/fontawesome-free/css/all.min.css"?>">
  <!-- IonIcons -->
  <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css"?>">

  <!-- DataTables -->
  <link rel="stylesheet" type="text/css"  href="<?php echo base_url()."assets/plugins/datatables-fixedcolumns/css/fixedColumns.bootstrap4.min.css"?>">

  <!-- Tempusdominus Bbootstrap 4 -->
  <!-- <link rel="stylesheet" href="<?php echo base_url()."assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"?>"> -->
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css"?>">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css"?>">
  <!-- Select2 -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/select2/css/select2.min.css"?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"?>">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css"?>">
  <!-- Tempusdominus Bbootstrap 4 -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css"> -->

  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"?>">


  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/plugins/jquery-datatables-checkboxes/css/dataTables.checkboxes.css"?>">

  <!-- Theme style -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/dist/css/adminlte.min.css"?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <style type="text/css">
  body, .btn, .form-control, .dropdown-menu, .input-group-text, .custom-select, .popover-header, .dropdown-item-title, .form-group.has-icon .form-icon, .small-box p {
    font-size: 14px;
  }
  h1, .h1 {
    font-size: 2rem;
  }
  table.dataTable thead th, table.dataTable tbody td {
    padding: 8px 7px;
    vertical-align: middle;
  }

  .table-grey{
    background-color: rgba(0,0,0,.05);
  }

  .navbar-light .navbar-nav .nav-link:focus, .navbar-light .navbar-nav .nav-link:hover {
    color: rgba(0,0,0,.7);
    font-size: 1rem;
  }
  .navbar-light .navbar-nav .nav-link #tahun-ajaran-header {
    color: #17a2b8;
    font-size: 1rem;
  }

  .navbar-light .navbar-nav .nav-link {
    font-size: 1rem;
  }

  .error{
    color: red;
  }

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
  }

  input[type=number] {
    -moz-appearance:textfield; /* Firefox */
  }



</style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<!-- <link rel="stylesheet" href="<?php echo base_url()."assets/dist/css/custom.css"?>"> -->
<body class="hold-transition sidebar-mini layout-navbar-fixed">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item">
          <form id="tahun-ajaran-header">
            <b><select name="tahun-ajaran" id="select2-tahun-ajaran-header" class="form-control select2 tahun-ajaran" style="width: 200px;" required="">
              <option value="" selected="selected">Pilih Tahun Ajaran</option>
            </select></b>
          </form>
<!--          <span class="nav-link">

          <span id="tahun-ajaran" class="d-block"><b>TAHUN AJARAN <?php echo $this->session->userdata('tahun_ajaran')->tahun_ajaran ?></b></span>
        </span> -->
      </li>
    </ul>
    <!-- Right navbar links -->


    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
          <span href="#" class="d-block"><b><?php echo $this->session->userdata('nama_pegawai') ?> <i class="fas fa-caret-down"></i></b></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="<?php echo base_url()."Auth/logout" ?>" class="dropdown-item">
            LOGOUT  <span class="float-right text-sm text-danger"><i class="fas fa-sign-out-alt"></i></span>
          </a>
          <div class="dropdown-divider"></div>
        </div>
      </li>

    </ul>
  </nav>
  <!-- /.navbar -->
  <?php
  $this->load->view("sidebar", ["page" => $page]);

  ?>