  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0-rc.1
    </div>
  </footer>
  
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js"?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.bundle.min.js"?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()."assets/dist/js/adminlte.min.js"?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url()."assets/plugins/datatables/jquery.dataTables.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"?>"></script>

<script src="<?php echo base_url()."assets/plugins/datatables-fixedcolumns/js/dataTables.fixedColumns.min.js"?>"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url()."assets/plugins/sweetalert2/sweetalert2.min.js"?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url()."assets/plugins/select2/js/select2.full.min.js"?>"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?php echo base_url()."assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/bootstrap-show-password-master/bootstrap-show-password.min.js"?>"></script>


<script src="<?php echo base_url()."assets/plugins/moment/moment.min.js"?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url()."assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"?>"></script>


<script src="<?php echo base_url()."assets/plugins/jquery-validation/dist/jquery.validate.min.js"?>"></script>

<script src="//cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script>


<script src="<?php echo base_url()."assets/plugins/jquery.serialize-object.min.js"?>"></script>

<script src="<?php echo base_url()."assets/plugins/jquery-datatables-checkboxes/js/dataTables.checkboxes.min.js"?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url()."assets/dist/js/demo.js"?>"></script>
<!-- Custom JS -->
<script src="<?php echo base_url()."assets/dist/js/custom.js"?>"></script>


<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script> -->
<script>

  var base_url = 'http://dev.erapor.id';
  
  jQuery.fn.ForceNumericOnly = function () {
    return this.each(function ()
    {
      $(this).keydown(function (e) {
        var key = e.charCode || e.keyCode || 0;				          
          if(key == 16){
            e.preventDefault();
          }else {
            return (
              key == 8 ||
              key == 9 ||
              key == 13 ||
              key == 46 ||
              key == 110 ||
              (key >= 35 && key <= 40) ||
              (key >= 48 && key <= 57) ||
              (key >= 96 && key <= 105)) ||
            ((key == 65 || key == 86 || key == 67) && (e.ctrlKey === true || e.metaKey === true));
          }
              });
    });
  }
  
  $(document).ready(function(){

    $('.date input').each(function(idx) {
      var id = $(this).attr('id');
      if (!id) {
        id = 'datetimepicker_' + idx;
        $(this)
          .attr('id', id)
          .attr('data-toggle', 'datetimepicker') // strangely, data() doesn't work here
          .data('target', '#' + id);
      }
      $(this).datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: false
      });
    });
    
    //Initialize Select2 Elements
    $('.select2').select2({
      theme: 'bootstrap4'
    });

    $('.number-only').ForceNumericOnly();
  

    $('[data-number-only=true]').ForceNumericOnly();



    $("#select2-tahun-ajaran-header").select2({
      ajax: {
        url: "/TahunAjaran/getCariTahunAjaran/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (res) {
          return {
            results: $.map(res.data, function (item) {
              return {
                text:  "TAHUN AJARAN " + item.tahun_ajaran,
                id: item.id_tahun_ajaran
              }
            })
          };
        }
      }
    });

    $.ajax({
      type: 'POST',
      url: "/TahunAjaran/getActiveTahunAjaran/",
      dataType: 'json'
    })
    .done(function (res) {
      if(res.success){
        let data = res.data;
        TA = new Option("TAHUN AJARAN " + data.tahun_ajaran, data.id_tahun_ajaran, true, true);
        $('#select2-tahun-ajaran-header').append(TA).trigger('change');   
      }
    });


    $('#select2-tahun-ajaran-header').on('select2:selecting', function (e) {
      var data = e.params.args.data;  
      $.ajax({
        type: 'POST',
        url: "/TahunAjaran/getCariTahunAjaranChange/"+ data.id,
        dataType: 'json'
      })    
      .done(function (res) {
        if(res.success){
          setTimeout(() => {
            location.reload();
          }, 1000);
        }
      });
    });

    $('form').on('focus', 'input[type=number]', function (e) {
      $(this).on('wheel.disableScroll', function (e) {
        e.preventDefault();
      });
    });

    $('form').on('blur', 'input[type=number]', function (e) {
      $(this).off('wheel.disableScroll');
    });
  });

</script>
</body>
</html>
