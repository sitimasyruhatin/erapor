// setup number only
jQuery.fn.ForceNumericOnly = function () {
    return this.each(function () {
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            return (
                    key == 47 ||
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105)) ||
                ((key == 65 || key == 86 || key == 67) && (e.ctrlKey === true || e.metaKey === true));
        });
    });
};

$(".number").ForceNumericOnly();

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 47 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

//Initialize Select2 Elements
    $('.select2').select2({
      theme: 'bootstrap4'
    })